package br.com.pat2math.domainBase;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.pat2math.studentModel.Student;

@Entity
@Table(name="emotion")
public class Emotion {
	@Id @GeneratedValue
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="id_content", referencedColumnName="id")
	private Exercise exercise;
	
	private Long numerros;
	
	private Long numhints;
	
	private Long numcontentabandon;
	
	private String previousabandon;
	
	private Long valemotion;
	
	private Long id_student;
	
	public Long getStudent() {
		return id_student;
	}

	public void setStudent(Long student) {
		this.id_student = student;
	}

	public Long getNumerros() {
		return numerros;
	}

	public void setNumerros(Long numerros) {
		this.numerros = numerros;
	}
	
	public Long getValEmotion() {
		return valemotion;
	}

	public void setValEmotion(Long valemotion) {
		this.valemotion = valemotion;
	}

	public Long getNumhints() {
		return numhints;
	}

	public void setNumhints(Long numhints) {
		this.numhints = numhints;
	}

	public Long getNumcontentabandon() {
		return numcontentabandon;
	}

	public void setNumcontentabandon(Long numcontentabandon) {
		this.numcontentabandon = numcontentabandon;
	}

	public String getPreviousabandon() {
		return previousabandon;
	}

	public void setPreviousabandon(String previousabandon) {
		this.previousabandon = previousabandon;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Exercise getExercise() {
		return exercise;
	}

	public void setExercise(Exercise exercise) {
		this.exercise = exercise;
	}
	
	
	
	
}
