package br.com.pat2math.domainBase;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.pat2math.studentModel.User;

@Entity
@Table(name="personality_answer")
public class PersonalityAnswer {
	@Id @GeneratedValue
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="id_personality", referencedColumnName="id")
	private Personality personality;
	
	@ManyToOne
	@JoinColumn(name="id_user", referencedColumnName="id")
	private User usr;
	
	
	
	public PersonalityAnswer(Long id, Personality personality, User usr,
			Integer answer) {
		super();
		this.id = id;
		this.personality = personality;
		this.usr = usr;
		this.answer = answer;
	}
	
	public PersonalityAnswer() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Personality getPersonality() {
		return personality;
	}

	public void setPersonality(Personality personality) {
		this.personality = personality;
	}

	public User getUsr() {
		return usr;
	}

	public void setUsr(User usr) {
		this.usr = usr;
	}

	private Integer answer;

	public Integer getAnswer() {
		return answer;
	}

	public void setAnswer(Integer answer) {
		this.answer = answer;
	}
	
	
}
