package br.com.pat2math.domainBase;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
/**
 * Salva a equação e trabalho atual, com Serializable
 * @author SAVANNAD
 *
 */
@Entity
@DiscriminatorValue("equation")
public class Exercise extends Content implements Serializable {
	
	private static final long serialVersionUID = -7055969336661087202L;
	
	
	public Exercise() {
		
	}
	
	public Exercise(Long id, String name, String equation) {
		this.id = id;
		this.name = name;
		this.equation = equation;
	}
	
	public Exercise(Long id, String name, String equation, String nivelDificuldade) {
		this.id = id;
		this.name = name;
		this.equation = equation;
		this.nivelDificuldade = nivelDificuldade;
		     
	}
	
	public Exercise(Long id, String name, String equation, String nivelDificuldade, String acao1, byte[] acao2, String url) {
		this.id = id;
		this.name = name;
		this.equation = equation;
		this.nivelDificuldade = nivelDificuldade;
		this.acao1 = acao1;
		this.acao2 = acao2;
		this.url = url;

	}
	
	@NotNull
	@Size(min=3, max=100)
	private String equation;
	
	private String acao1;
	
	private byte[] acao2;
	
	private String url;
	
	@Transient
	private List<String> steps = new ArrayList<String>();

	public String getEquation() {
		return equation;
	}

	public void setEquation(String equation) {
		this.equation = equation;
	}
	
	//texto
	public String getAcao1() {
		return acao1;
	}

	public void setacao1(String acao1) {
		this.acao1 = acao1;
	}
	
	//imagem
	public byte[] getacao2() {
		return acao2;
	}

	public void setacao2(byte[] acao2) {
		this.acao2 = acao2;
	}
	
	//url video
	public String geturl() {
		return url;
	}

	public void seturl(String url) {
		this.url = url;
	}

	public List<String> getSteps() {
		return steps;
	}

	public void setSteps(List<String> steps) {
		this.steps = steps;
	}
}