package br.com.pat2math.repository;

import org.springframework.stereotype.Service;

import java.util.List;
import br.com.pat2math.domainBase.PersonalityAnswer;

@Service
public interface PersonalityAnswerRepository extends Repository<PersonalityAnswer>{
	
	PersonalityAnswer getAnswerByUserAndQuestion(Long idUser, Long question);
	
	List<PersonalityAnswer> getAnswerByUser(Long idUser);
}
