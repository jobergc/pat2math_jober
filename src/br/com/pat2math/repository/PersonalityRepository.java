package br.com.pat2math.repository;

import java.util.List;

import org.springframework.stereotype.Service;

import br.com.pat2math.domainBase.Personality;

@Service
public interface PersonalityRepository extends Repository<Personality>{
	
	List<Personality> getQuestions();
	
	Personality getByCodQuestion(Long cod);
}
