package br.com.pat2math.repository;

import br.com.pat2math.domainBase.Content;

public interface ContentRepository extends Repository<Content> {

	/**
	 * Deleta um content do banco
	 * @param id - id do content
	 * @return {int} número de linhas alteradas no banco
	 */
	int deleteContent(Long id);

}