package br.com.pat2math.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import br.com.pat2math.domainBase.Personality;
import br.com.pat2math.domainBase.PersonalityAnswer;
import br.com.pat2math.repository.PersonalityAnswerRepository;


@Repository
@Service
public class PersonalityAnswerDao extends GenericDao<PersonalityAnswer> implements PersonalityAnswerRepository{

	@Override
	public PersonalityAnswer getAnswerByUserAndQuestion(Long idUser, Long question) {
		
		String queryStr =
			    "select p " +
			    "from PersonalityAnswer p left join fetch p.personality " +
			    "left join fetch p.usr where p.usr.id = :id_user and p.personality.id = :question";
		
		List<PersonalityAnswer> ret = em
				.createQuery(queryStr, PersonalityAnswer.class)
				.setParameter("id_user", idUser)
				.setParameter("question", question)
				.getResultList();
		
		if(ret.isEmpty()){
			return null;
		}
		
		return ret.get(0);
	}

	@Override
	public List<PersonalityAnswer> getAnswerByUser(Long idUser) {

		String queryStr =
			    "select p " +
			    "from PersonalityAnswer p left join fetch p.personality " +
			    "left join fetch p.usr where p.usr.id = :id_user";
		
		return em
				.createQuery(queryStr, PersonalityAnswer.class)
				.setParameter("id_user", idUser)
				.getResultList();
				
	}	

}
