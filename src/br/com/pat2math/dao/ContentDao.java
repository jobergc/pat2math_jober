package br.com.pat2math.dao;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import br.com.pat2math.domainBase.Content;
import br.com.pat2math.repository.ContentRepository;

@Repository
public class ContentDao extends GenericDao<Content> implements ContentRepository {

	@Override
	public int deleteContent(Long id){
		String jpql = "delete from Content where id = :id";
		Query q = em.createQuery(jpql);
		q.setParameter("id", id);
		return q.executeUpdate();
	}
	
}