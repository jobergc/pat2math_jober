package br.com.pat2math.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import br.com.pat2math.domainBase.Personality;
import br.com.pat2math.repository.PersonalityRepository;


@Repository
@Service
public class PersonalityDao extends GenericDao<Personality> implements PersonalityRepository{

	@Override
	public List<Personality> getQuestions() {
		String queryStr =
			    "select NEW br.com.pat2math.domainBase.Personality(e.id, e.description) " +
			    "from Personality e";
		return em
				.createQuery(queryStr, Personality.class)
				.getResultList();
	}

	@Override
	public Personality getByCodQuestion(Long cod) {
		String queryStr =
			    "select NEW br.com.pat2math.domainBase.Personality(e.id, e.description) " +
			    "from Personality e where e.id = :cod";
		List<Personality> ret = em
				.createQuery(queryStr, Personality.class)
				.setParameter("cod", cod)
				.getResultList();
		
		if(ret.isEmpty()){
			return null;
		}
		
		return ret.get(0);
		
	}

	

}
