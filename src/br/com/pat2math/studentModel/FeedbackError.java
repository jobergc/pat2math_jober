package br.com.pat2math.studentModel;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="feedback_error")

public class FeedbackError {
	@Id @GeneratedValue
	private Long id;
	
	private int level;
	private String previousStep, wrongStep;
	
	@ManyToOne
	@JoinColumn(name="id_student", referencedColumnName="id", nullable=true)
	private Student student;
	
	public FeedbackError(int level, String previousStep, String wrongStep, Student student) {
		this.level = level;
		this.previousStep = previousStep;
		this.wrongStep = wrongStep;
		this.student = student;
	}
	
	public FeedbackError() {
		this.level = 1;
		this.previousStep = null;
		this.wrongStep = null;
		this.student = null;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getPreviousStep() {
		return previousStep;
	}

	public void setPreviousStep(String previousStep) {
		this.previousStep = previousStep;
	}

	public String getWrongStep() {
		return wrongStep;
	}

	public void setWrongStep(String wrongStep) {
		this.wrongStep = wrongStep;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}
	

}
