package br.com.pat2math.studentModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="resolution_step")
public class ResolutionStep {
	
	@Id @GeneratedValue
	private Long id;
	
	private boolean correct;
	
	private boolean operationCorrect;
	
	private boolean isDeleted; //Verificador especial do PAT2Exam

	private String answer;
	
	private String feedback;
	
	private String message;
	
	private Integer qtd_hints_step;
	
	private Integer qtd_confusion_click;
	
	private Integer qtd_abandon_pat2math;
	
	private Integer intervencao;
	
	private Long tempo_intervencao;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date timestamp;
	
	@ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(name="step_tip", joinColumns = {@JoinColumn(name = "id_step")}, inverseJoinColumns={@JoinColumn(name="id_tip")})											 
	private List<Tip> tips = new ArrayList<Tip>();
	
	@ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(name="step_operation", joinColumns = {@JoinColumn(name = "id_step")}, inverseJoinColumns={@JoinColumn(name="id_operation")})												 
	private List<Operation> operations = new ArrayList<Operation>();
	
	@ManyToOne
	@JoinColumn(name="id_taskPerformed", referencedColumnName="id", nullable=true)
	private TaskPerformed taskPerformed;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public List<Tip> getTips() {
		return tips;
	}

	public void setTips(List<Tip> tips) {
		this.tips = tips;
	}

	public List<Operation> getOperations() {
		return operations;
	}

	public void setOperations(List<Operation> operations) {
		this.operations = operations;
	}

	public TaskPerformed getTaskPerformed() {
		return taskPerformed;
	}

	public void setTaskPerformed(TaskPerformed taskPerformed) {
		this.taskPerformed = taskPerformed;
	}


	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public boolean isCorrect() {
		return correct;
	}

	public void setCorrect(boolean correct) {
		this.correct = correct;
	}

	public boolean isOperationCorrect() {
		return operationCorrect;
	}

	public void setOperationCorrect(boolean operationCorrect) {
		this.operationCorrect = operationCorrect;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getFeedback() {
		return feedback;
	}

	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getQtd_hints_step() {
		return qtd_hints_step;
	}

	public void setQtd_hints_step(Integer qtd_hints_step) {
		this.qtd_hints_step = qtd_hints_step;
	}

	public Integer getQtd_confusion_click() {
		return qtd_confusion_click;
	}

	public void setQtd_confusion_click(Integer qtd_confusion_click) {
		this.qtd_confusion_click = qtd_confusion_click;
	}

	public Integer getQtd_abandon_pat2math() {
		return qtd_abandon_pat2math;
	}

	public void setQtd_abandon_pat2math(Integer qtd_abandon_pat2math) {
		this.qtd_abandon_pat2math = qtd_abandon_pat2math;
	}

	public Integer getIntervencao() {
		return intervencao;
	}

	public void setIntervencao(Integer intervencao) {
		this.intervencao = intervencao;
	}

	public Long getTempo_intervencao() {
		return tempo_intervencao;
	}

	public void setTempo_intervencao(Long tempo_intervencao) {
		this.tempo_intervencao = tempo_intervencao;
	}
	
}