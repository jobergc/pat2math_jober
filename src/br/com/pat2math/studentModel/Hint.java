package br.com.pat2math.studentModel;
import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="hint")

public class Hint {
	@Id @GeneratedValue
	private Long id;
	
	private int level;
	private String step;
	
	@ManyToOne
	@JoinColumn(name="id_student", referencedColumnName="id", nullable=true)
	private Student student;
	
	public Hint(int level, String step, Student student) {
		this.level = level;
		this.step = step;
		this.student = student;
	}
	
	public Hint() {
		this.level = 1;
		this.step = null;
		this.student = null;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getStep() {
		return step;
	}

	public void setStep(String step) {
		this.step = step;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}
	
	
	
	
}
