package br.com.pat2math.action;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.pat2math.service.PersonalityService;

@Controller
@Transactional
@RequestMapping("/personality")
public class PersonalityController {
	
	@Autowired private PersonalityService personalityService;
	
	@RequestMapping(method = RequestMethod.GET, value = "listpersonalityquestions")
	public String listPersonality(Model model) {
		model.addAttribute("contentsPersonality", personalityService.getQuestions());
		return "personality.listpersonality";
	}
	
	
	@RequestMapping(value="setanswer/{iduser}/{codQuestion}/{answer}", method = RequestMethod.GET, produces="text/plain; charset=UTF-8")
	public @ResponseBody String setAnswer(@PathVariable Long iduser, @PathVariable Long codQuestion, @PathVariable Integer answer, HttpSession session, HttpServletRequest request, HttpServletResponse response ){
		personalityService.setAnswer(iduser, codQuestion, answer);
		return "OK";
	}	
	
	@RequestMapping(value="verifyAnswer/{iduser}", method = RequestMethod.GET, produces="text/plain; charset=UTF-8")
	public @ResponseBody String verifyAnswer(@PathVariable Long iduser, HttpSession session, HttpServletRequest request, HttpServletResponse response ){
		return personalityService.verifyAnswer(iduser);
	}
	
	@RequestMapping(value="getTimeIntervention/{iduser}", method = RequestMethod.GET, produces="text/plain; charset=UTF-8")
	public @ResponseBody String getTimeIntervention(@PathVariable Long iduser, HttpSession session, HttpServletRequest request, HttpServletResponse response ){
		return personalityService.getTimeIntervencao(iduser) + "";
	}
	
	
}