package br.com.pat2math.service;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.apache.commons.math3.distribution.GammaDistribution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;






import br.com.pat2math.dao.UserDao;
import br.com.pat2math.domainBase.Personality;
import br.com.pat2math.domainBase.PersonalityAnswer;
import br.com.pat2math.repository.AllStudents;
import br.com.pat2math.repository.PersonalityAnswerRepository;
import br.com.pat2math.repository.PersonalityRepository;
import br.com.pat2math.studentModel.Student;

@Service
public class PersonalityService {
	
	@Autowired private PersonalityRepository personalityRepo;
	@Autowired private PersonalityAnswerRepository personalityAnswerRepo;
	@Autowired private AllStudents studentRepo;
	
	@PreAuthorize("hasRole('ROLE_STUDENT')")
	public List<Personality> getQuestions(){
		return personalityRepo.getQuestions();
	}
	
	@PreAuthorize("hasRole('ROLE_STUDENT')")
	public String setAnswer(Long iduser, Long codigoQuestao, Integer answer){
		
		PersonalityAnswer p = personalityAnswerRepo.getAnswerByUserAndQuestion(iduser, codigoQuestao);
		
		if(p != null){
			p.setAnswer(answer);
			personalityAnswerRepo.alter(p);
			return "OK";
		}else{
		
			p = new PersonalityAnswer();
			p.setAnswer(answer);
			p.setUsr(studentRepo.get(iduser));
			p.setPersonality(personalityRepo.getByCodQuestion(codigoQuestao));
			personalityAnswerRepo.add(p);
			return "OK"; 
		}
	}
	
	public String verifyAnswer(Long user){
		
		List<Personality> questions = personalityRepo.getQuestions();
		
		List<PersonalityAnswer> answers = personalityAnswerRepo.getAnswerByUser(user);
		
		if(questions.size() == answers.size()){
			this.atualizaIndicesUsuario(user);
			return "OK";
		}else{
			return "INCOMPLETO";
		}
	}
	
	public Integer getExtroversao(Long idUser){
		
		List<PersonalityAnswer> answer = personalityAnswerRepo.getAnswerByUser(idUser);
		Integer result = 0;
		for (PersonalityAnswer personalityAnswer : answer) {
			if(personalityAnswer.getPersonality().getId()==1 || personalityAnswer.getPersonality().getId()==11 ||
					personalityAnswer.getPersonality().getId()==12 || personalityAnswer.getPersonality().getId()==17)
				result += personalityAnswer.getAnswer();
		}
		return result;
	}
	
	public Integer getNeuroticismo(Long idUser){
		
		List<PersonalityAnswer> answer = personalityAnswerRepo.getAnswerByUser(idUser);
		Integer result = 0;
		for (int i = 0; i < answer.size(); i++) {
			if(answer.get(i).getPersonality().getId()==6 || answer.get(i).getPersonality().getId()==15 ||
					answer.get(i).getPersonality().getId()==16 || answer.get(i).getPersonality().getId()==20)
				result += answer.get(i).getAnswer();
		}
		return result;
	}
	
	public Integer getAbertura(Long idUser){
		
		List<PersonalityAnswer> answer = personalityAnswerRepo.getAnswerByUser(idUser);
		Integer result = 0;
		for (int i = 0; i < answer.size(); i++) {
			if(answer.get(i).getPersonality().getId()==5 || answer.get(i).getPersonality().getId()==7 ||
					answer.get(i).getPersonality().getId()==14 || answer.get(i).getPersonality().getId()==18)
				result += answer.get(i).getAnswer();
		}
		return result;
	}
	
	public Integer getAmabilidade(Long idUser){
		
		List<PersonalityAnswer> answer = personalityAnswerRepo.getAnswerByUser(idUser);
		Integer result = 0;
		for (int i = 0; i < answer.size(); i++) {
			if(answer.get(i).getPersonality().getId()==4 || answer.get(i).getPersonality().getId()==8 ||
					answer.get(i).getPersonality().getId()==9 || answer.get(i).getPersonality().getId()==19)
				result += answer.get(i).getAnswer();
		}
		return result;
	}
	
	public Integer getRealizacao(Long idUser){
		
		List<PersonalityAnswer> answer = personalityAnswerRepo.getAnswerByUser(idUser);
		Integer result = 0;
		for (int i = 0; i < answer.size(); i++) {
			if(answer.get(i).getPersonality().getId()==2 || answer.get(i).getPersonality().getId()==3 ||
					answer.get(i).getPersonality().getId()==10 || answer.get(i).getPersonality().getId()==13)
				result += answer.get(i).getAnswer();
		}
		return result;
	}
	
	private Student atualizaIndicesUsuario(Long idUser){
		Student student = studentRepo.get(idUser);
		
		student.setIndice_abertura(this.getAbertura(idUser));
		student.setIndice_amabilidade(this.getAmabilidade(idUser));
		student.setIndice_extroversao(this.getExtroversao(idUser));
		student.setIndice_neuroticismo(this.getNeuroticismo(idUser));
		student.setIndice_realizacao(this.getRealizacao(idUser));
		
		studentRepo.alter(student);
		
		return student;
	}
	
	public double getTimeIntervencao(Long idUser){
		
		Student student = this.atualizaIndicesUsuario(idUser);
		
		return this.calculoR(student.getNotaAlgebra(), student.getIndice_neuroticismo(), student.getIndice_extroversao());
		
	}
	
	public double calculoR(double algebraTest, double neuroticism, double extroversion){
		
		//Scores (from 0 to 1)
		double x1 = (algebraTest/100); //# algebra test
		double x2 =  (neuroticism/100); //# neuroticism index
		double x3 = (extroversion/100); //# extroversion index

		//# Estimated parameters
		double beta1 = -1.970;
		double beta2 = 0.721;
		double beta3 = -0.828;
		double alpha = 1.258;
		double lambda = 0.010;
		double eta = 2.747;
		
		// create random object
		Random randomno = new Random();

		// setting seed
		randomno.setSeed(123);
		
		GammaDistribution g = new GammaDistribution(eta, 1/eta, 100000);
		
		double w[] = g.sample(100000);
		double val[];
		val = new double[w.length];
		for (int i = 0; i < w.length; i++) {
			val[i] = Math.pow(( (Math.log(0.5)*-1)/(lambda*Math.exp(beta1*x1+beta2*x2+beta3*x3)*w[i])) , (1/alpha));
		}
		
		Arrays.sort(val);
		
		double median;
		
		if (val.length % 2 == 0)
		    median = ((double)val[val.length/2] + (double)val[val.length/2 - 1])/2;
		else
		    median = (double) val[val.length/2];
		
		return median;
		
	}
	
}
