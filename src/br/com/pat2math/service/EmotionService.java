package br.com.pat2math.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;

import br.com.pat2math.domainBase.Emotion;
import br.com.pat2math.domainBase.Exercise;
import br.com.pat2math.repository.ContentRepository;
import br.com.pat2math.repository.EmotionRepository;

@Service
public class EmotionService {
	
	@Autowired private EmotionRepository emotionrepo;
	@Autowired private ContentService contentservice;
	
	public Emotion createEmotion(Emotion emotion, Exercise exercise, Errors errors) {
		emotion.setExercise(exercise);
		return errors.hasErrors() ? null : emotionrepo.add(emotion);
	}
	
	public Emotion createEmotion(Exercise exercise) {
		Emotion emotion = new Emotion();
		emotion.setExercise(exercise);
		return emotionrepo.add(emotion);
	}
	
	public String incrementError(Long idEmotion) {
		Emotion emotion = emotionrepo.get(idEmotion);
		long numerros = 0;
		if( emotion.getNumerros() == null){
			emotion.setNumerros(numerros);
		}
		emotion.setNumerros(emotion.getNumerros() + 1);
		emotionrepo.alter(emotion);
		return "1";
	}
	
	public String incrementHints(Long idEmotion) {
		Emotion emotion = emotionrepo.get(idEmotion);
		long num = 0;
		if( emotion.getNumhints() == null){
			emotion.setNumhints(num);
		}
		emotion.setNumhints(emotion.getNumhints() + 1);
		emotionrepo.alter(emotion);
		return "1";
	}
	
	public String incrementAbandon(Long idEmotion) {
		Emotion emotion = emotionrepo.get(idEmotion);
		if(emotion==null){
			return "0";
		}
		long num = 0;
		if( emotion.getNumcontentabandon() == null){
			emotion.setNumcontentabandon(num);
		}
		emotion.setNumcontentabandon(emotion.getNumcontentabandon() + 1);
		emotionrepo.alter(emotion);
		return "1";
	}
	
	public String setpreviousAbandon(Long idEmotion) {
		Emotion emotion = emotionrepo.get(idEmotion);
		emotion.setPreviousabandon("T");
		emotionrepo.alter(emotion);
		return "1";
	}
	
	public String setExerciseId(Long idStudent, Long idexercise) {
		Emotion emotion = emotionrepo.getEmotionByExercise(idexercise, idStudent);
		
		Exercise e = contentservice.getExercisebyId(idexercise);
		
		if(emotion == null){
			emotion = createEmotion(e);
		}
		
		emotion.setExercise(e);
		emotion.setStudent(idStudent);
		
		emotionrepo.alter(emotion);
		
		return "" + emotion.getId();
	}
	
	public String setValEmotion(Long idEmotion, Long valemotion) {
		Emotion emotion = emotionrepo.get(idEmotion);
		emotion.setValEmotion(valemotion);
		emotionrepo.alter(emotion);
		return "1";
	}
	
	public Long getValEmotion(Long idEmotion) {
		Emotion emotion = emotionrepo.get(idEmotion);
		return emotion.getValEmotion();
	}
	
	public String calculaIntervencao(Long idEmotion) {
		long intervencao;
		Emotion emotion = emotionrepo.get(idEmotion);
		long x1 = 1;
		Exercise e = (Exercise)contentservice.getExercisebyId(emotion.getExercise().getId());
		long x2;
		
		try{
			x2 = Long.parseLong(e.getNivelDificuldade());
		}catch(NumberFormatException exception){
			x2 = 1;
		}
		long x3 = 0;
		if(emotion.getNumerros() != null)
			x3 = emotion.getNumerros();
		
		long x4 = 0;
		if (emotion.getNumhints() != null){
			x4 = emotion.getNumhints();
		}
		long x5 = 0;
		
		if(emotion.getPreviousabandon()=="T"){
			x5 = 1;
		}
		
		long x6 = 0; 
		if (emotion.getNumcontentabandon() != null){
			x6 = emotion.getNumcontentabandon();
		}
		
		//se não incremeu o contador de abandono, então não teve abandono no ultimo exercicio
		if(x6 == 0 ){
			x5 = 0;
		}
		
		intervencao =  x1 * 10 + x2 * 7 + x3 * 6 + x4 * 5 + x5 * 20 + x6 * 2;
		return "" + intervencao;
	}
	
}
