var appContext = "pat2math/translator/";
var lineChars = 80;
var nextLine = "";
var problemId = null;
var aux_text = null;
var allowSelection = true;
var allowHint = false;
var stepError = false;
var numOfData = 0;
var numOfVariables = 0;
var numOfDefinedData = 0;
var numOfRelations = 0;
var numOfDefinedRelations = 0;
var numOfIrrelevantData = 0;
var score;
var problemsSolved;
var totalProblems;
var nextTopic;
var dataIds;
var variableIds;
var relationIds;
var equationIds;
var dataHints;
var variableHints;
var relationHints;
var equationHints;
var stepNumber = 1;
var hintHeight = 0;

var selectionErrors = 0;
var wrongValueErrors = 0;

var dataSelectionHintLevel = 1;
var dataWrongHintLevel = 1;
var variableSelectionHintLevel = 1;
var variableWrongHintLevel = 1;
var relationSelectionHintLevel = 1;
var relationWrongHintLevel = 1;
var equationSelectionHintLevel = 1;
var equationWrongHintLevel = 1;
var hintText;

function calculateLength (equation) {
	var length = equation.length;
	//Posição atual do símbolo de fração
	var posFrac = equation.indexOf ("/");
	//Array com todas as posições do símbolo de fração
	var posFracArray = new Array ( );
	
	//posFrac é diferente de -1 se houverem uma ou mais frações na equação
	while (posFrac !== -1) {
		posFracArray[posFracArray.length] = posFrac;
		posFrac = equation.indexOf ("/", posFrac + 1);
	}
	
	//Se o array de posições tiver pelo menos um registro, existe uma ou mais frações na equação
	if (posFracArray.length > 0) {
		//Converte a String em um array de caracteres
		var equationArray = equation.split ("");
		//Na primeira execução, first é o início da equação e last é a posição do primeiro símbolo de fração
		var first = 0, last = posFracArray[0];
		//posArray recebe 1 pois a posição 0 já foi utilizada acima
		var posArray = 1;
		//O próximo laço while deverá ser executado enquanto o último símbolo de fração da equação não foi verificado
		var continua = last !== undefined;
		
		while (continua) {
			//Controlam o balanceamento de parênteses abertos e fechados
			var cont1 = 0, cont2 = 0;
			//length1 é o tamanho do numerador e length2 do denominador da fração
			var length1, length2;
			//O primeiro for começará de trás para frente, a partir do primeiro caracter que não seja o símbolo de fração nem o parênteses que fecha o numerador
			var i = last - 2;
			//Para os parênteses estarem balanceados, cont2 deve ser o número sucessor a cont1
			for (; cont1 !== cont2 + 1; i--) {
				if (equationArray[i] === "(")
					cont1++;
				//Se este parêntese for encontrado, é que haviam parênteses internos no numerador da fração, como por exemplo 5*(x+2)
				else if (equationArray[i] === ")")
					cont2++;
			}
			
			length1 = last - i + 1; //seria -1+2, onde 2 são os parênteses que abrem e fecham a fração, os quais não deverão ser contados no tamanho final
			//O -1 é porque, antes de sair do laço for, ou seja, quando a condição for falsa, i é decrementado mais uma vez
			
			first = last + 1;
			last = posFracArray[posArray];
			
			if (last !== undefined)
				posArray++;
			//Se last for undefined, é que chegou ao último símbolo de fração, que está na última posição do array de caracteres
			else {
				last = equation.length - 1;
				continua = false;
			}
			//No segundo for ocorre o processo inverso em relação ao primeiro
			cont1 = 0; cont2 = 0; i = first + 1;
			
			for (; cont1 !== cont2 + 1; i++) {
				if (equationArray[i] === ")")
					cont1++;
				//Se este parêntese for encontrado, é que haviam parênteses internos no denominador da fração, como por exemplo 5*(x+2)
				else if (equationArray[i] === "(")
					cont2++;
			}
			
			length2 = i - first + 2; //2 são os parênteses que abrem e fecham a fração
			
			//Compara qual dos tamanhos é o menor, que deverá ser subtraído do tamanho total da equação (o que conta é o termo com mais caracteres)
			if (length2 < length1)
				length -= length2;
			
			else
				length -= length1;
		}
		
	}
	
	length -= posFracArray.length; //Referentes aos símbolos de divisão "/"
	pontuacaoAtual = length;
}

function calculateUsedLines(){
	usedLines = 0;
	
	if (selectedEquation.equation.indexOf ("/") !== -1) {
		usedLines++;
	}
	
	var steps = selectedEquation.steps;
	
	for (var i = 0; i < steps.length; i++) {
		if (steps[i].step.indexOf ("/") === -1) {
			usedLines++;
		}
		
		else {
			usedLines += 2;
		}
	}
}

function enableContent(id) {
	$.post(
		appContext + "content/enable",
		{"idContent": id},
		function(data) {
			$("#content-" + id).html("Ok");
		}
	);
}

function openClose(id, sequence, unlockedTopic){
	var open = $("#problems"+id).css("display");
	if($("#problems"+id).css("display") == "none"){
		if(unlockedTopic >= sequence){
			$('#problems'+id).show();
		}
	}else{
		$('#problems'+id).hide();
	}
}

function getNumEquationsPlan() {
	if (planoAtual < 19 && planoAtual !== 6 && planoAtual !== 11 && planoAtual !== 16)
		return 5;
	
	else if (planoAtual === 6 || planoAtual === 16 || (planoAtual > 20 && planoAtual < 35 && planoAtual !== 25 && planoAtual !== 28 && planoAtual!== 32))
		return 10;
	
	else if (planoAtual === 11)
		return 16;
	
	else if (planoAtual === 25 || planoAtual === 32)
		return 20;
	
	else if (planoAtual === 28 || planoAtual === 35)
		return 15;
	
	else 
		return 56;
	
}

function getPontuacaoPlano() {
	if (planoAtual !== 11 && planoAtual !== 16 && planoAtual !== 21 && planoAtual !== 25 && planoAtual !== 32 && planoAtual < 35) {
		if (planoAtual <= 6)
			pontuacaoPlano = 20;
		
		else if (planoAtual >= 7 && planoAtual <= 10)
			pontuacaoPlano = 25;
		
		else if (planoAtual === 12 || planoAtual === 13)
			pontuacaoPlano = 30;
		
		else if (planoAtual === 14 || planoAtual === 15)
			pontuacaoPlano = 35;
		
		else if (planoAtual === 17 || planoAtual === 18)
			pontuacaoPlano = 40;
		
		else if (planoAtual === 19 || planoAtual === 20)
			pontuacaoPlano = 50;
		
		else if (planoAtual >= 26 && planoAtual <= 28)
			pontuacaoPlano = 100;
		
		else if (planoAtual === 22)
			pontuacaoPlano = 50;
		
		else if (planoAtual === 23)
			pontuacaoPlano = 60;
		
		else if (planoAtual === 24)
			pontuacaoPlano = 80;
		
		else if (planoAtual === 29)
			pontuacaoPlano = 120;
		
		else if (planoAtual === 30)
			pontuacaoPlano = 140;
		
		else if (planoAtual === 31)
			pontuacaoPlano = 160;
		
		else if (planoAtual === 33)
			pontuacaoPlano = 200;
		
		else
			pontuacaoPlano = 300;	
	}
	
	else
		pontuacaoPlano = null;
}

function getPontuacaoEquacoes() {
	pontuacaoEquacoes = new Array();
	pontuacaoEquacoes[0] = 20;//..
	
	//----- PLANO DE AULA 11 -----//
	pontuacaoEquacoes[1100] = 20;
	pontuacaoEquacoes[1101] = 20;
	pontuacaoEquacoes[1102] = 20;
	pontuacaoEquacoes[1103] = 20;
	pontuacaoEquacoes[1104] = 20;
	pontuacaoEquacoes[1105] = 20;
	pontuacaoEquacoes[1106] = 20;
	pontuacaoEquacoes[1107] = 25;
	pontuacaoEquacoes[1108] = 25;
	pontuacaoEquacoes[1109] = 20;
	pontuacaoEquacoes[1110] = 25;
	pontuacaoEquacoes[1111] = 25;
	pontuacaoEquacoes[1112] = 25;
	pontuacaoEquacoes[1113] = 25;
	pontuacaoEquacoes[1114] = 25;
	pontuacaoEquacoes[1115] = 25;
	
	//----- PLANO DE AULA 16 -----//
	pontuacaoEquacoes[1600] = 30;
	pontuacaoEquacoes[1601] = 35;
	pontuacaoEquacoes[1602] = 35;
	pontuacaoEquacoes[1603] = 30;
	pontuacaoEquacoes[1604] = 35;
	pontuacaoEquacoes[1605] = 35;
	pontuacaoEquacoes[1606] = 30;
	pontuacaoEquacoes[1607] = 35;
	pontuacaoEquacoes[1608] = 30;
	pontuacaoEquacoes[1609] = 35;
}

function padlockClick ( ) {
	showHint("Para desbloquear este tópico, você deve resolver todos os exercícios do tópico anterior.");
}

function loadExerciseWE(eq, points) {
	isWorkedExample = true;
	blockMenu = true;
	
	$("#topics").fadeOut();
    $("#topicsAux").show();
    
	var equation = new Equation(eq, points);
	newEquations[0] = equation;
	reloadPaper(1);
}

/* carrega a dica e exibe a mesma na tela */
function defineHint(text){
	
	if(!allowHint){
		text = "Você deve primeiro selecionar um problema para resolver!";
		showHint(text);
	}else{
	
		if(!text){
			
			// se ainda existem dados a serem selecionados exibe dica sobre dados
			if(numOfData > numOfDefinedData + numOfVariables){
				console.log("pattranslation.defineHint(text): Generic Data Hint");
				if(dataHints && dataHints[0] && dataHints[0] != ""){
					showHint(dataHints[0]);
					dataHints.splice(0, 1);
				} else {
					console.log("pattranslation.defineHint(text): Specific Data Hint");
					
					if(allowSelection)
						getHint(dataIds[0], dataSelectionHintLevel++, "selection", "d", showHint);
					else
						getHint(dataIds[0], dataWrongHintLevel++, "wrongValue", "d", showHint);
				}
			} else if(variableIds.length > 0){
				console.log("pattranslation.defineHint(text): Generic Variable Hint");
				if(variableHints && variableHints[0] && variableHints[0] != ""){
					showHint(variableHints[0]);
					variableHints.splice(0, 1);
				} else {
					console.log("pattranslation.defineHint(text): Specific Variable Hint");
					getHint(variableIds[0], variableSelectionHintLevel++, "selection", "d", showHint);
				}
			} else if(relationIds.length > 0){
				console.log("pattranslation.defineHint(text): Generic Relation Hint");
				if(relationHints && relationHints[0] && relationHints[0] != ""){
					showHint(relationHints[0]);
					relationHints.splice(0, 1);
				} else {
					console.log("pattranslation.defineHint(text): Specific Relation Hint");
					getHint(relationIds[0], relationWrongHintLevel++, "wrongValue", "r", showHint);
				}
			} else if(equationIds.length > 0){
				console.log("pattranslation.defineHint(text): Generic Equation Hint");
				if(equationHints && equationHints[0] && equationHints[0] != ""){
					showHint(equationHints[0]);
					equationHints.splice(0, 1);
				} else {
					console.log("pattranslation.defineHint(text): Specific Equation Hint");
					getHint(equationIds[0], equationWrongHintLevel++, "wrongValue", "r", showHint);
				}
			} else {
				showHint("Você já concluiu o problema!");
			}
		} else {
			showHint(text);
		}
	}
}

function showHint(text) {	
	console.log("pattranslation.showHint(text) Hint text is: "+text)
	moveHint();
	
	$("#hintText").html(text);
	$('#hintText').fadeIn(500);
	
	hintHeight = document.getElementById('hintText').offsetHeight;
	moveCalculator();
}

function hideHint(){
	$('#hintText').fadeOut(700);
	hintHeight = 0;
	moveCalculator();
}

function getHint(id, level, type, dataType, callback){
	var data = {
		"id" : id,
  		"level" : level,
  		"type" : type,
  		"dataType" : dataType,
	}
	
	$.ajax({
		type: "POST",
		contentType : 'application/json; charset=utf-8',
	    dataType : 'json',
		url: "/pat2math/translator/getHint",
		data: JSON.stringify(data),
		async: false,
		success: function(data) {
			
	        // Se os dados são válidos
	        if(data.status){
	        	console.log("pattranslation.getHint(): the hint returned from controller is "+data.hintText);
	        	callback(data.hintText);
	        } else {
	        	callback("Callback erro ao acessar o servidor");
	        }
		},                
		error : function(req, status, error) {
			console.log("pattranslation.getHint() STATUS:"+status);
	        console.log("pattranslation.getHint() REQ:"+req);
	        console.log("pattranslation.getHint() ERROR:"+error);
	        callback("pattranslation.getHint() CALLBACK_ERROR");
		}
	});
}

/* posiciona a dica na tela */
function moveHint() {
	// Seta a altura da dica em pixels		
	if(nextLine)
		var rect = document.getElementById('line'+nextLine).getBoundingClientRect();
	else
		rect = 0;
    var docEl = document.documentElement;
    var top = rect.top + (window.pageYOffset || docEl.scrollTop || 0);
    var left = rect.left + (window.pageXOffset || docEl.scrollLeft || 0);
    
	top = top + 10;
	left = left + 150;
	document.getElementById("hintText").style.top = top+"px";
	document.getElementById("hintText").style.left = left+"px";
}

/* Carrega um problema na folha */
function loadProblem(id, statement) {		
	hideHint();
	loadingShow();
	problemId = id;
	
	// limpa todas as linhas da folha
	clearPaper();
	
	// Limpa a calculadora e remove da tela
	document.getElementById('calculator').style.display = 'none';
	removeButtons();
	
	// Busca dados na controller
	getDataRelEqu(id, statement, continueLoading, loadFromHistory);
	
	// pega a primeira linha da folha
	var div = document.getElementById('line2');
	
	// limpa ela e adiciona o texto do problema
	div.innerHTML = div.innerHTML + '<p onmouseup="mouseUpHandler()">'+ statement +'</p>';
	
	// Calculando quantos caracteres/linhas o problema ocupa na folha
	var textLength = statement.length;
	var linesUsed = Math.ceil(textLength/lineChars);
	nextLine = linesUsed + 3;
	
	// worked example barra tutorial
	if (id == 8 || id == 10 || id == 9) {
		allowSelection = false;
		allowHint = false;
	}
	
	if(id == 9){
		weTutorialPlano1();
	}
	
	if(id == 8){
		weTutorialPlano2();
	}
	
	if(id == 10){
		weTutorialPlano03();
	}
}

/* Função que trata o evento mouseUp no texto do enunciado */
function mouseUpHandler() {
    var text = (document.all) ? document.selection.createRange().text : document.getSelection();
    text = trim(''+text);
    selectText(''+text);
}

/* Continua o Loading dos problemas ainda nao completos */
function continueLoading(id, statement, history){
	// faz as linhas abaixo do problema não serem selecionáveis
	makeUnselectable();
	stepNumber = 1;
	
	var thereIsStillData = true;
	if(history.length > 0){
		for (i = 0; i < history.length; i++) {
			console.log(history[i].substr(1));
			
			div = document.getElementById('line'+nextLine);
			div.innerHTML = "";
			nextLine = nextLine + 1;
			
			if(history[i].charAt(0) == "d"){
				var text = history[i].substring(1, history[i].indexOf('=')-1);
				var value = history[i].substring(history[i].indexOf('=')+2, history[i].length);
				markerColor = "yellow";
				highlightText(text);
				addButton('d', text, value);
				numOfDefinedData = numOfDefinedData + 1;
				stepNumber = stepNumber + 1;
				div.innerHTML = div.innerHTML + '<p><img src="/pat2math/patequation/img/patt_btn_data.png" class="img_data"></img> '+ history[i].substr(1) +' <img src="/pat2math/patequation/img/cool.png" class="img_like"></img></p>';
			} else if(history[i].charAt(0) == "i"){
				markerColor = "green";
				highlightText(history[i].substr(1));
				numOfVariables = numOfVariables - 1;
				stepNumber = stepNumber + 1;
				div.innerHTML = div.innerHTML + '<p><img src="/pat2math/patequation/img/patt_btn_variable.png" class="img_data"></img> '+ history[i].substr(1) +' <img src="/pat2math/patequation/img/cool.png" class="img_like"></img></p>';
			} else if(history[i].charAt(0) == "r"){
				var text = history[i].substring(1, history[i].indexOf('=')-1);
				var value = history[i].substring(history[i].indexOf('=')+2, history[i].length);
				addButton('r', text, value);
				numOfDefinedRelations = numOfDefinedRelations + 1;
				stepNumber = stepNumber + 1;
				var thereIsStillData = false;
				div.innerHTML = div.innerHTML + '<p><img src="/pat2math/patequation/img/patt_btn_relation.png" class="img_data"></img> '+ history[i].substr(1) +' <img src="/pat2math/patequation/img/cool.png" class="img_like"></img></p>';
			} else if(history[i].charAt(0) == "e"){
				stepNumber = stepNumber + 1;
				var thereIsStillData = false;
				div.innerHTML = div.innerHTML + '<p><img src="/pat2math/patequation/img/patt_btn_equation.png" class="img_data"></img> '+ history[i].substr(1) +' <img src="/pat2math/patequation/img/cool.png" class="img_like"></img></p>';
				break;
			}
		}
		numOfDefinedData = numOfDefinedData - numOfIrrelevantData + numOfVariables + 1;
		
		if(thereIsStillData){
			checkNumData();
		} else {
			checkNumRelations();
		}
	}
	
	resetMarker();
	allowSelection = true;
	allowHint = true;
	selectionErrors = 0;
	wrongValueErrors = 0;
	loadingHide();
}

/* Carrega os problemas ja completos, mostra o historico */
function loadFromHistory(history){
	console.log("pattranslation.selectText() The history has "+history.length+" steps.");
	
	for (i = 0; i < history.length; i++) {
		console.log(history[i].substr(1));
		
		div = document.getElementById('line'+nextLine);
		div.innerHTML = "";
		nextLine = nextLine + 1;
		
		if(history[i].charAt(0) == "d"){
			div.innerHTML = div.innerHTML + '<p><img src="/pat2math/patequation/img/patt_btn_data.png" class="img_data"></img> '+ history[i].substr(1) +' <img src="/pat2math/patequation/img/cool.png" class="img_like"></img></p>';
		} else if(history[i].charAt(0) == "i"){
			div.innerHTML = div.innerHTML + '<p><img src="/pat2math/patequation/img/patt_btn_variable.png" class="img_data"></img> '+ history[i].substr(1) +' <img src="/pat2math/patequation/img/cool.png" class="img_like"></img></p>';
		} else if(history[i].charAt(0) == "r"){
			div.innerHTML = div.innerHTML + '<p><img src="/pat2math/patequation/img/patt_btn_relation.png" class="img_data"></img> '+ history[i].substr(1) +' <img src="/pat2math/patequation/img/cool.png" class="img_like"></img></p>';
		} else if(history[i].charAt(0) == "e"){
			div.innerHTML = div.innerHTML + '<p><img src="/pat2math/patequation/img/patt_btn_equation.png" class="img_data"></img> '+ history[i].substr(1) +' <img src="/pat2math/patequation/img/cool.png" class="img_like"></img></p>';
			break;
		}
	}
	
	div = document.getElementById('line'+nextLine);
	div.innerHTML = div.innerHTML + '<p> <button id="btnSendToPatEquation" onclick="sendToPatEquation()" type="button">Resolver no PATequation</button></p>';
	nextLine = nextLine + 2;
	
	loadingHide();
}

/* Trata o texto selecionado */
function selectText(text){
	if(allowSelection){
		aux_text = text;
		if(text){
			
			// Dados para enviar ao controller para verificação
			dataType = getDataType();
			console.log("pattranslation.selectText() Text: "+text)
			console.log("pattranslation.selectText() dataType: "+dataType)
			var data = {
		      "statementPart" : text,
		      "id" : problemId,
		      "dataType" : dataType
		   }
			
			$.ajax({
				type: "POST",
				contentType : 'application/json; charset=utf-8',
			    dataType : 'json',
				url: "/pat2math/translator/verifySelection",
				data: JSON.stringify(data),
				success: function(data) {
					
			        // Se os dados são válidos
			        if(data.status){
			        	console.log("pattranslation.selectText(): status is true");
			        	console.log("pattranslation.selectText(): data type is "+dataType);
			        	
			        	div = document.getElementById('line'+nextLine);
			    		div.innerHTML = "";
			    		
			    		if(dataType == "d")
			    			div.innerHTML = div.innerHTML + '<p><img src="/pat2math/patequation/img/patt_btn_data.png" class="img_data"></img> <input type="text" class="student_input_description" id="student_input_description" value="" placeholder="descrição" onclick="setMouseFocus(this)"/><input type="text" class="student_input_data" id="student_input_data" onkeypress="keyPressed(event)" onclick="setMouseFocus(this)" value="" placeholder="valor"/></p>';
		    			if(dataType == "i"){
			    			div.innerHTML = div.innerHTML + '<p><img src="/pat2math/patequation/img/patt_btn_variable.png" class="img_data"></img> '+ text +' = x';
			    			
			    			var e = $.Event('keypressed');
			    		    e.which = 13; // Character 'A'
			    			keyPressed(e);
		    			}
		    			
		    			if (document.getElementById("student_input_description")){
			    			document.getElementById("student_input_description").focus();
			    			document.getElementById("student_input_description").value = text;
			    			document.getElementById("student_input_description").select();
		    			} else if (document.getElementById("student_input_data")) {
		    				 document.getElementById("student_input_data").focus();
		    			}
		    			allowSelection = false;
		    			console.log("pattranslation.selectText(): focus should be set by now");
			    		
			    		// incrementa o nextLine
			    		// IMPORTANTE: Evitar definir textos muito grandes. Manter o máximo em torno de 50 caracteres
			    		nextLine = nextLine +1;
			    		
			    		// esconde a hint se houver
			    		hideHint();
			    		
			    		// deixa o texto marcado
			    		highlightText(text);

			        } else {
			        	// Exibe uma dica de seleção e reseta o contador de erros
			        	if(data.hintText){
			    			showHint(data.hintText);
			    			selectionErrors = selectionErrors - 1;
			    		}
			        	
			        	selectionErrors = selectionErrors + 1;
			        	if(selectionErrors > 4){
			        		if(numOfData > numOfDefinedData + numOfVariables){
			        			getHint(dataIds[0], dataSelectionHintLevel++, "selection", "d", showHint);
			        			selectionErrors = 0;
			        		} else if (variableIds.length > 0){
			        			getHint(variableIds[0], variableSelectionHintLevel++, "selection", "d", showHint);
			        			selectionErrors = 0;
			        		}
			        	}
			        }
				},                
				error : function(req, status, error) {
					console.log("pattranslation.selectText() STATUS:"+status);
			        console.log("pattranslation.selectText() REQ:"+req);
			        console.log("pattranslation.selectText() ERROR:"+error);
				}
			});
		}
	}
	// limpa a seleção do texto
	clearSelection();
	
	// Retorna o foco pro campo de texto
	var div = document.getElementById("student_input");
	if(div != null)
		div.focus();
}

/* Trata as teclas pressionadas */
function keyPressed(event){
	var key = event.which;
	console.log("pattranslation.keyPressed() key pressed");
	
	if (key === 13) {
		console.log("pattranslation.keyPressed() Enter pressed. Checking Answer...");
		
		if (document.getElementById("student_input_data")){
			var value = document.getElementById('student_input_data').value;
			// Troca a virgula por ponto
			value = value.replace(/,/g, '.');
			// Remove os espaços em branco
			value = value.replace(/\s/g, '');
		}
			
		if (document.getElementById("student_input_description"))
			var description = document.getElementById("student_input_description").value;
		
		console.log("pattranslation.keyPressed(): The step number on the JS is "+stepNumber);
		
		if(dataType == "d")
			var data = {
				"statementPart" : aux_text,
				"description" : description,
				"data" : value,
				"letter" : null,
			    "id" : problemId,
			    "dataType" : dataType,
			    "statementPart" : aux_text,
			    "stepNumber" : stepNumber
			    
			}
		if(dataType == "i")
			var data = {
				"statementPart" : aux_text,
				"data" : null,
				"letter" : value,
			    "id" : problemId,
			    "dataType" : dataType,
			    "statementPart" : aux_text,
			    "stepNumber" : stepNumber
			}
		
		$.ajax({
			type: "POST",
			contentType : 'application/json; charset=utf-8',
		    dataType : 'json',
			url: "/pat2math/translator/verifyData",
			data: JSON.stringify(data),
			success: function(data) {
				
				if(data.status){
					console.log("pattranslation.keyPressed(): value matches the one on the database");
					hideHint();
					
					// Limpa a div
					nextLine = nextLine - 1;
					div = document.getElementById('line'+nextLine);
					div.innerHTML = "";
					nextLine = nextLine + 1;
					stepNumber = stepNumber +1;
					
					// Confirma os dados inseridos
					if(dataType == "d"){
						div.innerHTML = div.innerHTML + '<p><img src="/pat2math/patequation/img/patt_btn_data.png" class="img_data"></img> '+ data.description + " = " + data.value +' <img src="/pat2math/patequation/img/cool.png" class="img_like"></img></p>';
						
						// remove o dado do array de dados.
						console.log("pattranslation.keyPressed(): the data with the id "+data.identifier+" was found.");
						var index = dataIds.indexOf(data.identifier);
						if (index > -1) {
							dataIds.splice(index, 1);
						}
						dataSelectionHintLevel = 1;
						dataWrongHintLevel = 1;
					}
					if(dataType == "i"){
						div.innerHTML = div.innerHTML + '<p><img src="/pat2math/patequation/img/patt_btn_variable.png" class="img_data"></img> '+ aux_text + ' = x <img src="/pat2math/patequation/img/cool.png" class="img_like"></img></p>';
						
						var index = variableIds.indexOf(data.identifier);
						if (index > -1) {
							variableIds.splice(index, 1);
						}
						numOfVariables = numOfVariables - 1;
						variableSelectionHintLevel = 1;
						variableWrongHintLevel = 1;
						console.log("pattranslation.keyPressed(): the no of variables still to be found is now "+numOfVariables);
					}
					
					allowSelection = true;
					stepError = false;
					
					// se o dado for relevante para a solução incrementa o número de dados definidos
					if(data.irrelevantData){
						console.log("pattranslation.keyPressed(): the data is irrelevant for the solution > "+data.irrelevantData);
					} else {
						if(dataType == "d")
							addButton(dataType, data.description, data.value);
						
						numOfDefinedData = numOfDefinedData + 1;
						checkNumData();
					}
					
				} else {
					if(stepError == false){
						nextLine = nextLine - 1;
						div = document.getElementById('line'+nextLine);
						var description = document.getElementById("student_input_description").value;
						div.innerHTML = div.innerHTML.substring(0, div.innerHTML.length - 4) + ' <img src="/pat2math/patequation/img/patt_btn_error.png" class="img_data"></img>';
						document.getElementById("student_input_description").value = description;
						nextLine = nextLine + 1;
						stepError = true;
					}
					
					// Exibe uma dica de seleção e reseta o contador de erros
		        	if(data.hintText){
		    			showHint(data.hintText);
		    			wrongValueErrors = wrongValueErrors - 1;
		    		}
		        	
		        	wrongValueErrors = wrongValueErrors + 1;
		        	if(wrongValueErrors > 2){
		        		if(numOfData > numOfDefinedData + numOfVariables){
		        			getHint(data.idData, dataWrongHintLevel++, "wrongValue", "d", showHint);
		        			wrongValueErrors = 0;
		        		}
		        	}
					
					// limpa o textfield e seta o foco de volta nele
					document.getElementById("student_input_data").value = "";
					document.getElementById("student_input_data").focus();
				}
			},
			error : function(req, status, error) {
				console.log("pattranslation.keyPressed() STATUS:"+status);
		        console.log("pattranslation.keyPressed() REQ:"+req);
		        console.log("pattranslation.keyPressed() ERROR:"+error);
			}
		});

	} else if (key === 27) {
		try{e.preventDefault();}//Non-IE
        catch(x){e.returnValue=false;}//IE
		console.log("pattranslation.keyPressed(): Esc pressed. Cleaning up...");
		div = document.getElementById('line'+nextLine);
		div.innerHTML = "";
	}
}

/* Trata as teclas pressionadas */
function keyPressedRelation(event){
	var key = event.which;
	console.log("pattranslation.keyPressedRelation(): key pressed");
	
	if (key === 13) {
		console.log("pattranslation.keyPressedRelation(): Enter pressed. Checking Answer...");
		
		if (document.getElementById("student_input_relation")){
			var equation = document.getElementById('student_input_relation').value;
			// Troca a virgulo por ponto
			equation = equation.replace(/,/g, '.');
			// Troca o X maiusculo por x minusculo
			equation = equation.replace('X', 'x');
			// Remove os espaços em branco
			equation = equation.replace(/\s/g, '');
		}
		
		if(document.getElementById('student_input_description')){
			var description = document.getElementById('student_input_description').value;
			if(!description.trim()){
				console.log("pattranslation.keyPressedRelation(): description is empty");
				var description = 'Relação';
			}
		} 
		
		//var equation = document.getElementById('student_input_relation').value;
		console.log('pattranslation.keyPressedRelation(): description is '+description);
		console.log('pattranslation.keyPressedRelation(): relation is '+equation);
		
		var data = {
			"description" : description,
			"equation" : equation,
		    "id" : problemId,
		    "stepNumber" : stepNumber
		}
			
		$.ajax({
			type: "POST",
			contentType : 'application/json; charset=utf-8',
		    dataType : 'json',
			url: "/pat2math/translator/verifyRelation",
			data: JSON.stringify(data),
			success: function(data) {
				
				if(data.status){
					console.log("pattranslation.keyPressedRelation(): equation matches the one on the database");
					hideHint();
					
					// Limpa a div
					nextLine = nextLine - 1;
					console.log("getting line number "+nextLine);
					div = document.getElementById('line'+nextLine);
					div.innerHTML = "";
					nextLine = nextLine + 1;
					stepNumber = stepNumber +1;
					
					// Confirma os dados inseridos
					if(data.dataType == "r"){
						div.innerHTML = div.innerHTML + '<p><img src="/pat2math/patequation/img/patt_btn_relation.png" class="img_data"></img> '+ data.description + " = " + equation +' <img src="/pat2math/patequation/img/cool.png" class="img_like"></img></p>';
						
						var index = relationIds.indexOf(data.identifier);
						if (index > -1) {
							relationIds.splice(index, 1);
						}
						relationSelectionHintLevel = 1;
						relationWrongHintLevel = 1;
						
					}if(data.dataType == "e"){
						div.innerHTML = div.innerHTML + '<p><img src="/pat2math/patequation/img/patt_btn_equation.png" class="img_data"></img> '+ equation +' <img src="/pat2math/patequation/img/cool.png" class="img_like"></img></p>';
					
						var index = equationIds.indexOf(data.identifier);
						if (index > -1) {
							equationIds.splice(index, 1);
						}
						equationSelectionHintLevel = 1;
						equationWrongHintLevel = 1;
						
					}
						
//					allowSelection = true;
					stepError = false;
					
					console.log("pattranslation.keyPressed(): the relation with the id "+data.identifier+" was found.");
					var index = relationIds.indexOf(data.identifier);
					if (index > -1) {
						relationIds.splice(index, 1);
					}
					console.log("pattranslation.keyPressed(): the following data are still to be found");
					for(index = 0; index<relationIds.length; ++index){
						console.log(relationIds[index]);
					}
					
					if(data.dataType == "r")
						addButton(data.dataType, data.description, equation);
					
					numOfDefinedRelations = numOfDefinedRelations + 1;
					checkNumRelations();
				} else {
					if(stepError == false){
						nextLine = nextLine - 1;
						div = document.getElementById('line'+nextLine);
						div.innerHTML = div.innerHTML.substring(0, div.innerHTML.length - 4) + ' <img src="/pat2math/patequation/img/patt_btn_error.png" class="img_data"></img>';
						nextLine = nextLine + 1;
						stepError = true;
					}
					
					// Exibe uma dica de seleção e reseta o contador de erros
		        	if(data.hintText){
		    			showHint(data.hintText);
		    			wrongValueErrors = wrongValueErrors - 1;
		    		}
		        	
		        	wrongValueErrors = wrongValueErrors + 1;
		        	if(wrongValueErrors > 2){
		        		if(wrongValueErrors > 2){
			        		if(relationIds.length > 0){
			        			getHint(relationIds[0], relationWrongHintLevel++, "wrongValue", "r", showHint);
			        			wrongValueErrors = 0;
			        		} else if(equationIds.length > 0){
			        			getHint(equationIds[0], equationWrongHintLevel++, "wrongValue", "r", showHint);
			        			wrongValueErrors = 0;
			        		}
			        	}
		        	}
					
					// limpa o textfield e seta o foco de volta nele
		        	if(document.getElementById("student_input_description"))
		        		document.getElementById("student_input_description").value = data.description;
					document.getElementById("student_input_relation").value = "";
					document.getElementById("student_input_relation").focus();
				}
			},
			error : function(req, status, error) {
				console.log("pattranslation.keyPressedRelation(): STATUS:"+status);
		        console.log("pattranslation.keyPressedRelation(): REQ:"+req);
		        console.log("pattranslation.keyPressedRelation(): ERROR:"+error);
			}
		});

	} else if (key === 27) {
		try{e.preventDefault();}//Non-IE
        catch(x){e.returnValue=false;}//IE
		console.log("pattranslation.keyPressedRelation(): Esc pressed. Cleaning up...");
		div = document.getElementById('line'+nextLine);
		div.innerHTML = "";
	}
}

function checkNumData(){
	if(numOfData == numOfDefinedData){
		console.log("pattranslation.checkNumData(): All data is defined. Student can now start working on the relations");
		div = document.getElementById('line'+nextLine);
		div.innerHTML = "";
		
		if(numOfRelations === 0){
			console.log('pattranslation.checkNumData(): theres is no relations in this problem. Just need the equation now.');
			div.innerHTML = div.innerHTML + '<p><img src="/pat2math/patequation/img/patt_btn_equation.png" class="img_data"></img><input type="text" class="student_input_relation" id="student_input_relation" onkeypress="keyPressedRelation(event)" onclick="setMouseFocus(this)" value="" placeholder="equação"/></p>';

			// seta o foco e desabilita a seleção no texto do problema
			document.getElementById("student_input_relation").focus();
			allowSelection = false;
		} else {
			div.innerHTML = div.innerHTML + '<p><img src="/pat2math/patequation/img/patt_btn_relation.png" class="img_data"></img> <input type="text" class="student_input_description" id="student_input_description" value="" onkeypress="keyPressedRelation(event)" placeholder="descrição" onclick="setMouseFocus(this)"/><input type="text" class="student_input_relation" id="student_input_relation" onkeypress="keyPressedRelation(event)" onclick="setMouseFocus(this)" value="" placeholder="Relação"/></p>';
			
			// seta o foco e desabilita a seleção no texto do problema
			document.getElementById("student_input_description").focus();
			allowSelection = false;
		}
		
		// Exibe a calculadora
		document.getElementById('calculator').style.display = 'block';
		moveCalculator();

		// incrementa o numero da linha
		nextLine = nextLine + 1;
	}
}

function checkNumRelations(){
	if(numOfRelations > numOfDefinedRelations){
		
		console.log("pattranslation.checkNumRelations(): There's still relations to be defined."+numOfRelations+'/'+numOfDefinedRelations);
		div = document.getElementById('line'+nextLine);
		div.innerHTML = "";
		div.innerHTML = div.innerHTML + '<p><img src="/pat2math/patequation/img/patt_btn_relation.png" class="img_data"></img> <input type="text" class="student_input_description" id="student_input_description" value="" placeholder="descrição" onclick="setMouseFocus(this)"/><input type="text" class="student_input_relation" id="student_input_relation" onkeypress="keyPressedRelation(event)" onclick="setMouseFocus(this)" value="" placeholder="Relação"/></p>';

		moveCalculator();
		
		// seta o foco e desabilita a selação no texto
		document.getElementById("student_input_description").focus();
		allowSelection = false;
		
		// incrementa o numero da linha
		nextLine = nextLine + 1;
	} else if (numOfRelations == numOfDefinedRelations){
		console.log("pattranslation.checkNumRelations(): Just need the equation now");
		div = document.getElementById('line'+nextLine);
		div.innerHTML = "";
		div.innerHTML = div.innerHTML + '<p><img src="/pat2math/patequation/img/patt_btn_equation.png" class="img_data"></img><input type="text" class="student_input_relation" id="student_input_relation" onkeypress="keyPressedRelation(event)" onclick="setMouseFocus(this)" value="" placeholder="equação"/></p>';

		moveCalculator();
		
		// seta o foco e desabilita a selação no texto
		document.getElementById("student_input_relation").focus();
		allowSelection = false;
		
		// incrementa o numero da linha
		nextLine = nextLine + 1;
	} else {
		// Tira a calculadora da tela
		document.getElementById('calculator').style.display = 'none';

		// Marca o exercício como concluído
		var data = {
		    "id" : problemId
		}
		
		$.ajax({
			type: "POST",
			contentType : 'application/json; charset=utf-8',
		    dataType : 'json',
			url: "/pat2math/translator/markAsCompleted",
			data: JSON.stringify(data),
			success: function(data) {
				
				if(data.status){
					console.log("pattranslation.checkNumRelations(): problem "+problemId+" marked as completed succesfully");
					
					//getting the data from JSON
					score = data.score;
					problemsSolved = data.problemsSolved;
					totalProblems = data.totalProblems;
					nextTopic = data.nextTopic;
					
					updatePoints(score, problemsSolved, totalProblems);
					markProblemCompleted();
				}
			},
			error : function(req, status, error) {
				console.log("pattranslation.checkNumRelations(): STATUS:"+status);
		        console.log("pattranslation.checkNumRelations(): REQ:"+req);
		        console.log("pattranslation.checkNumRelations(): ERROR:"+error);
			}
		});
	}
}

function markProblemCompleted(){
	if (document.getElementById("icon-ok-"+problemId)){
		document.getElementById("icon-ok-"+problemId).className = "icon-ok icon-white";
		document.getElementById("icon-pencil-"+problemId).style.marginRight = "6px";
	}
	
	console.log('pattranslation.markProblemCompleted(): ProblemsSolved='+problemsSolved+" totalProblems="+totalProblems);
	if(problemsSolved === totalProblems){
		console.log('pattranslation.markProblemCompleted(): all problems solved. Unlocking next topic');
		unlockNextTopic();
	} else {
		console.log('pattranslation.markProblemCompleted(): still problems to solve before unlocking next topic');
	}
	
	// Chama o botao para resolver no patequation (se não for os worked examples)
	if(problemId !== 8 || problemId !== 9 || problemId !== 10){
		div = document.getElementById('line'+nextLine);
		div.innerHTML = div.innerHTML + '<p> <button id="btnSendToPatEquation" onclick="sendToPatEquation()" type="button">Resolver no PATequation</button></p>';
		nextLine = nextLine + 2;
	}
}

/**
 * Busca o ID da equação do patequation relacionada com o problema do pattranslator no banco
 * E redireciona para o patequation para o aluno resolver a equação
 */
function sendToPatEquation(){
	console.log("pattranslation.getEquationId(): the problem id here is "+ problemId);
	var data = {
		"id" : problemId
	}
	$.ajax({
		type: "POST",
		contentType : 'application/json; charset=utf-8',
	    dataType : 'json',
		url: "/pat2math/translator/getEquationId",
		data: JSON.stringify(data),
		success: function(data) {
			console.log("pattranslation.getEquationId(): The equation id received was "+ data.patequationId);
			setCookieDays("patt_eqFromTranslator", data.patequationId, 1);
			setCookieDays("patt_translator_control", "", 0);
			location.href="/pat2math/student/home";
			
		}, error: function(req, status, error){
			console.log("pattranslation.getEquationId(): STATUS: "+status);
	        console.log("pattranslation.getEquationId(): REQ: "+req);
	        console.log("pattranslation.getEquationId(): ERROR: "+error);
		}
	});
}

function getPatEquationId_old(problemId){
	if(problemId == 14)
		return 100000;
	else if(problemId == 15){
		return 100001;
	} else if(problemId == 16){
		return 100002;
	} else if(problemId == 17){
		return 100003;
	} else if(problemId == 18){
		return 100004;
	} else if(problemId == 19){
		return 100005;
	} else if(problemId == 20){
		return 100006;
	} else if(problemId == 22){
		return 100007;
	} else if(problemId == 23){
		return 100008;
	} else if(problemId == 21){
		return 200000;
	} else if(problemId == 24){
		return 200001;
	} else if(problemId == 25){
		return 200002;
	} else if(problemId == 26){
		return 200003;
	} else if(problemId == 27){
		return 200004;
	} else if(problemId == 29){
		return 200005;
	} else if(problemId == 31){
		return 200006;
	} else if(problemId == 42){
		return 200007;
	} else if(problemId == 33){
		return 300000;
	} else if(problemId == 34){
		return 300001;
	} else if(problemId == 35){
		return 300002;
	} else if(problemId == 36){
		return 300003;
	} else if(problemId == 37){
		return 300004;
	} else if(problemId == 38){
		return 300005;
	} else if(problemId == 39){
		return 300006;
	} else if(problemId == 40){
		return 300007;
	} else if(problemId == 41){
		return 300008;
	} else if(problemId == 43){
		return 400000;
	} else if(problemId == 44){
		return 400001;
	} else if(problemId == 45){
		return 400002;
	} else if(problemId == 46){
		return 400003;
	} else if(problemId == 47){
		return 400004;
	} else if(problemId == 48){
		return 400005;
	} else if(problemId == 49){
		return 400006;
	} else if(problemId == 50){
		return 400007;
	} else if(problemId == 51){
		return 400008;
	} else if(problemId == 52){
		return 400009;
	} else if(problemId == 53){
		return 500000;
	} else if(problemId == 54){
		return 500001;
	} else if(problemId == 55){
		return 500002;
	} else if(problemId == 56){
		return 500003;
	} else if(problemId == 57){
		return 500004;
	} else if(problemId == 58){
		return 500005;
	} else if(problemId == 59){
		return 500006;
	} else if(problemId == 60){
		return 500007;
	} else if(problemId == 61){
		return 500008;
	} else if(problemId == 62){
		return 500009;
	} else if(problemId == 63){
		return 600000;
	} else if(problemId == 64){
		return 600001;
	} else if(problemId == 65){
		return 600002;
	} else if(problemId == 66){
		return 600003;
	} else if(problemId == 67){
		return 600004;
	} else if(problemId == 68){
		return 600005;
	} else if(problemId == 69){
		return 600006;
	} else if(problemId == 70){
		return 600007;
	} else if(problemId == 71){
		return 600008;
	} else if(problemId == 72){
		return 600009;
	} else if(problemId == 73){
		return 600010;
	} else if(problemId == 74){
		return 600011;
	} else if(problemId == 75){
		return 600012;
	} else if(problemId == 77){
		return 600013;
	}	
}

function unlockNextTopic(){
	console.log('pattranslation.unlockNextTopic(): Unlocking topic '+nextTopic);
	
	if (document.getElementById("problems"+nextTopic)) {
		document.getElementById("problems"+nextTopic).className = "topic";
	}
}

/* Move a calculadora para baixo */
function moveCalculator(){
	// Seta a altura da calculadora em pixels
	if(nextLine)
		var rect = document.getElementById('line'+nextLine).getBoundingClientRect();
	else
		rect = 0;
    var docEl = document.documentElement;
    var top = rect.top + (window.pageYOffset || docEl.scrollTop || 0);
    var left = rect.left + (window.pageXOffset || docEl.scrollLeft || 0);
    
	top = top - 30;
	left = left + 150;
	
	top = top + (hintHeight/2);
	
	document.getElementById("calculator").style.top = top+"px";
	document.getElementById("calculator").style.left = left+"px";
	$("#calculator").fadeIn();
}

function updatePoints(score, problemsSolved, totalProblems){
	$("#amountPoins").text(score + " pontos");
	
	$("#progressBar .label").text(problemsSolved + " / " + totalProblems);
	var widthBar = (problemsSolved / totalProblems) * 100;
	widthBar = Math.trunc(widthBar);
	$("#progressBar div").css("width", widthBar + "%");
	
	console.log('pattranslation.updatePoints(): score and progress bar should be updated by now [2]');
}

/* Adiciona botoes na calculadora, dinamicamente. Para os dados e relações definidos pelo aluno */
function addButton(dataType, text, value) {
	console.log("pattranslation.addButton(): the value of the button should be "+value);
	var div = document.getElementById("calculator_keys");
	var buttonnode = document.createElement('input');
	buttonnode.setAttribute('type','button');
	buttonnode.setAttribute('class','pattbtn data');
	buttonnode.setAttribute('value', text);
	buttonnode.setAttribute('id', 'calculator_extra_btn');
	
	if(dataType == "r")	
		value = '('+value+')';
	
	buttonnode.setAttribute('onClick', 'appendValue(\''+value+'\')');
	div.appendChild(buttonnode);
}

/* Remove os botões extra da calculadora */
function removeButtons(){
	if (document.getElementById("calculator_extra_btn")) {
		while (document.getElementById("calculator_extra_btn")) {
			var element = document.getElementById('calculator_extra_btn');
		    element.parentNode.removeChild(element);
		}
	}
	console.log('pattranslation.removeButtons(): The calculator is clean')
}

/* Insere o valor digitado na calculadora no textField */
function appendValue(value) {
	$('#student_input_relation').val($('#student_input_relation').val() + value);
	document.getElementById("student_input_relation").focus();
}

/* limpa o textField onde o aluno digita as expressoes */
function clearField(value) {
	$('#student_input_relation').val("");
}

/* simula a tecla Enter */
function confirm(){
	var e = jQuery.Event("keypress");
	e.which = 13;
	e.keyCode = 13;
	$("#student_input_relation").trigger(e);
}

function setRelationFocus(){
	document.getElementById('student_input_relation').focus();
}

/* Busca no banco o numero de dados e relações do problema */
function getDataRelEqu(id, statement, callback, callbackHistory){
	var data = {
		"id" : problemId
	}
	$.ajax({
		type: "POST",
		contentType : 'application/json; charset=utf-8',
	    dataType : 'json',
		url: "/pat2math/translator/getDataRelEqu",
		data: JSON.stringify(data),
		success: function(data) {
			console.log("pattranslation.getDataRelEqu(): No of Data is "+ data.noData);
			console.log("pattranslation.getDataRelEqu(): No of Relations is "+ data.noRelations);
			numOfData = data.noData;
			numOfDefinedData = 0;
			numOfRelations = data.noRelations;
			numOfDefinedRelations = 0;
			numOfIrrelevantData = data.noIrrelevantData;
			
			// recebendo arrays com os ids dos dados e relacoes do problema
			dataIds = data.dataIds;
			variableIds = data.variableIds;
			relationIds = data.relationIds;
			equationIds = data.equationIds;
			numOfVariables = data.noVariables;
			
			dataHints = data.dataHints;
			variableHints = data.variableHints;
			relationHints = data.relationHints;
			equationHints = data.equationHints;
			
			console.log("pattranslation.getDataRelEqu(): No of Variables is "+ data.noVariables);
			console.log("pattranslation.getDataRelEqu(): No of history strings is "+ data.history.length);
			
			if(data.completed){
				if (id == 8 || id == 10 || id == 9) {
					console.log("pattranslation.getDataRelEqu(): The problem is a worked example. Loading as usual.");
					callback(id, statement);
				} else {
					console.log("pattranslation.getDataRelEqu(): The problem was already completed. Loading history...");
					callbackHistory(data.history);
				}
			} else {
				console.log("pattranslation.getDataRelEqu(): The problem is not completed yet. Continue loading as usual...");
				callback(id, statement, data.history);
			}
			
		}, error: function(req, status, error){
			console.log("pattranslation.getDataRelEqu(): STATUS: "+status);
	        console.log("pattranslation.getDataRelEqu(): REQ: "+req);
	        console.log("pattranslation.getDataRelEqu(): ERROR: "+error);
	        
	        var err = eval("(" + req.responseText + ")");
	        console.log("pattranslation.getDataRelEqu(): ERROR: "+req.Message);
		}
	});
}

/* Limpa todas as linhas da folha */
function clearPaper(){
	for (var i = 2; i <= numLines+2; i++){
		var div = document.getElementById('line'+i);
		div.innerHTML = "";
	}
}

/* Faz as linhas abaixo do nextLine unselectable */
function makeUnselectable(){
	for (var i = nextLine; i <= numLines+2; i++){
		var div = document.getElementById('line'+i);
		div.className += div.className ? ' unselectable' : 'unselectable';
	}
}

/* Desmarca o texto selecionado */
function clearSelection() {
    if ( document.selection ) {
        document.selection.empty();
    } else if ( window.getSelection ) {
        window.getSelection().removeAllRanges();
    }
}

function loadingShow(){
	$('#loading').fadeIn();
	$("#topics").fadeOut();
}

function loadingShowWE() {
	$('#loadingWE').fadeIn();
	$("#topics").fadeOut();
}

function loadingHide(){
	$('#loading').fadeOut();
//	$("#topics").show("slide", { direction: "left" }, 500);
//	$('.hide-menu').fadeIn();
//	$('#topics').fadeIn();
}

function changePlan(idGroup, idPlan) {
	$.post(
		appContext + "group/changePlan",
		{"idGroup": idGroup, "idPlan": idPlan},
		function(data) {
			$(".item").attr("class", "item2");
			$("#plan" + data).attr("class", "item");
		}
	);
}

function changeGroup(idGroup) {
	$.post(
		appContext + "student/changeGroup",
		{"idGroup": idGroup},
		function(data) {
			$(".item").attr("class", "item2");
			$("#group" + data).attr("class", "item");
		}
	);
}

function test56() {
	$(".modal").hide();
	$("#mask").hide();
}

function hideMenu() {
	$("#topics").hide();
}

function watchVideo(id) {
	$.post(
		appContext + "video/watch",
		{"id" : id},
		function(data) {
			$("#video-box").html(data);
			$("#mask").fadeIn(700);
			$("#video-box").fadeIn(700);
		}
	);
	idTaskVideo=id;
}

function msgBox(id, msg, action) {
	if(confirm(msg)) {
		location.href = action + "?group.id=" + id;
	} else {
		//nothing
	}
}

function simulateTyping(field, txt){
	var timeOut;
	var txtLen = txt.length;
	var char = 0;
	var tb = $(field).attr("value", "|");
	(function typeIt() {
	    var humanize = Math.round(Math.random() * (200 - 30)) + 30;
	    timeOut = setTimeout(function () {
	        char++;
	        var type = txt.substring(0, char);
	        tb.attr("value", type + '|');
	        typeIt();

	        if (char == txtLen) {
	            tb.attr("value", tb.attr("value").slice(0, -1))
	            clearTimeout(timeOut);
	        }

	    }, humanize);
	}());
}

function weTutorialPlano2() {
	$.guider({
		next: "1",
		title : "Tutorial do Plano 02",
		description : "Antes de começar a resolver os exercícios do Plano 02 vamos conferir mais alguns conceitos básicos.</center>",	
		overlay : "dark",
		width : 600,
		alignButtons : "right",
		buttons : {
			Próximo: {
				click : true,
				className : "primary",
			}
		}
	}).show();
	
	$.guider({
		name: "1",
		next : "2",
		title : "Relembrando: Qual é o nosso objetivo?",
		description : "Descobrir uma equação que resolva o problema.",
		width : 600,
		alignButtons : "right",
		onShow: function() {window.scrollTo(0, 50);},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$.guider({
		name: "2",
		next : "3",
		title : "Relembrando: O que é uma equação?",
		description : "É uma fórmula matemática, que descreve como resolver o problema.",
		width : 600,
		alignButtons : "right",
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$.guider({
		name: "3",
		next : "4",
		title : "Relembrando: Qual o primeiro passo?",
		description : "Temos de ler o enunciado do problema e identificar os dados, que são as informações concretas que o enunciado nos traz.",
		width : 600,
		alignButtons : "right",
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$("#line2").guider({
		name: "4",
		next : "5",
		title : "Quantos jornais a banca vende por dia?",
		description : "A banca vende 150 jornais por dia. Vamos selecionar essa informação no texto",
		width : 600,
		position: "bottom",
		alignButtons : "right",
		onShow: function() {allowSelection = true; selectText("150 jornais");},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$("#line5").guider({
		name: "5",
		next : "6",
		title : "Definindo um dado",
		description : "Agora vamos informar uma descrição para o dado e digitar o seu valor.",
		width : 600,
		position: "bottom",
		alignButtons : "right",
		onShow: function() {
			simulateTyping("#student_input_description", "150 jornais");
			setTimeout(function () {
				simulateTyping("#student_input_data", "150");
		    }, 2500);
			setTimeout(function () {
				var e = jQuery.Event("keypress");
				e.which = 13;
				e.keyCode = 13;
				$("#student_input_data").trigger(e);
		    }, 4000);
		},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$("#line2").guider({
		name: "6",
		next : "7",
		title : "Quantos jornais a banca vende a mais no domingo?",
		description : "A banca vende 100 jornais a mais no domingo. Vamos selecionar esse dado no enunciado.",
		width : 600,
		position: "bottom",
		alignButtons : "right",
		onShow: function() {allowSelection = true; selectText("100 jornais");},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$("#line6").guider({
		name: "7",
		next : "8",
		title : "Definindo um dado",
		description : "Agora vamos definir uma descrição e também digitar o valor do dado.",
		width : 600,
		position: "bottom",
		alignButtons : "right",
		onShow: function() {
			simulateTyping("#student_input_description", "100 Jornais");
			setTimeout(function () {
				simulateTyping("#student_input_data", "100");
		    }, 1500);
			setTimeout(function () {
				var e = jQuery.Event("keypress");
				e.which = 13;
				e.keyCode = 13;
				$("#student_input_data").trigger(e);
		    }, 2500);
		},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$.guider({
		name: "8",
		next : "9",
		title : "Definindo uma incógnita",
		description : "Agora precisamos definir a incógnita, que é a pergunta que o enunciado quer responder. Lembre-se de que para selecioná-la, vamos precisar do marcador verde!",
		width : 600,
		alignButtons : "right",
		onShow: function() {selectMarker();allowSelection = true; selectText("Quantos jornais são vendidos numa semana?");},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$.guider({
		name: "9",
		next : "10",
		title : "Definindo Relações",
		description : "Após definir todos os dados e incógnitas uma calculadora aparece para auxiliá-lo nos próximos passos.",
		width : 600,
		alignButtons : "right",
		onShow: function() {selectMarker();},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$.guider({
		name: "10",
		next : "11",
		title : "O que é uma relação?",
		description : "Uma relação é uma equação que montamos, utilizando dois ou mais dados já definidos, criando um terceiro dado.",
		width : 600,
		alignButtons : "right",
		onShow: function() {selectMarker();},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$("#line7").guider({
		name: "11",
		next : "12",
		title : "Definindo Relações",
		description : "Por exemplo, neste caso, precisamos definir quantos jornais a banca vende ao todo no domingo.",
		width : 600,
		alignButtons : "right",
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$("#line7").guider({
		name: "12",
		next : "13",
		title : "Definindo Relações",
		description : "Primeiro, para facilitar, definimos uma descrição para a relação",
		width : 600,
		alignButtons : "right",
		onShow: function() {simulateTyping("#student_input_description", "Jornais vendidos domingo");},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$("#line7").guider({
		name: "13",
		next : "14",
		title : "Definindo Relações",
		description : "Agora, utilizando a calculadora ou digitando, inserimos a expressão da relação. Repare que a calculadora possui um botão para cada dado que você definiu anteriormente, isso facilita o seu trabalho nessa parte.",
		width : 600,
		alignButtons : "right",
		onShow: function() {
			simulateTyping("#student_input_relation", "150+100");
			setTimeout(function () {
				var e = jQuery.Event("keypress");
				e.which = 13;
				e.keyCode = 13;
				$("#student_input_relation").trigger(e);
		    }, 2000);
		},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$("#line8").guider({
		name: "14",
		next : "15",
		title : "Definindo a Equação",
		description : "Após definir todas as relações é necessário definir a equação final.",
		width : 600,
		alignButtons : "right",
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$("#line8").guider({
		name: "15",
		next : "16",
		title : "Definindo a Equação",
		description : "Utilizando todos os dados, incógnitas e relações já definidos, vamos definir uma equação que seja capaz de responder a pergunta do enunciado.",
		width : 600,
		alignButtons : "right",
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$("#line8").guider({
		name: "16",
		next : "17",
		title : "Definindo a Equação",
		description : "Sabemos que a banca vende (150+100) jornais no domingo e que, nos demais dias ela vende 100 jornais. Sabemos também que a semana tem 7 dias, logo, durante 6 dias a banca vende 100 jornais. Agora vamos por isso no papel, ou melhor, na tela.",
		width : 600,
		alignButtons : "right",
		onShow: function() {
			simulateTyping("#student_input_relation", "x=(150+100)+6*150");
			setTimeout(function () {
				var e = jQuery.Event("keypress");
				e.which = 13;
				e.keyCode = 13;
				$("#student_input_relation").trigger(e);
		    }, 2500);
		},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$.guider({
		name: "17",
		//next : "18",
		title : "Terminamos!",
		description : "Agora você já sabe como resolver os problemas do Plano 02. Passe para o próximo exercício e tente resolvê-lo.",
		width : 600,
		alignButtons : "right",
		onShow: function() {selectMarker();},
		buttons : {
			Finalizar: {
				click : true,
				className : "primary"
			}
		}
	});
}

function weTutorialPlano1() {
	$.guider({
		next: "1",
		title : "<center> <img src=/pat2math/patequation/img/logo200x166.png></img><br> Bem-vindo! </center>",
		description : "<center>O PATtranslation é um programa que auxilia na resolução de problemas matemáticos. Vamos conferir alguns conceitos básicos antes de começar...</center>",
		overlay : "dark",
		width : 600,
		alignButtons : "right",
		buttons : {
			Próximo: {
				click : true,
				className : "primary",
			}
		}
	}).show();
	
	$.guider({
		name: "1",
		next : "2",
		title : "Qual é o nosso objetivo?",
		description : "Descobrir uma equação que resolva o problema.",
		width : 600,
		alignButtons : "right",
		onShow: function() {window.scrollTo(0, 50);},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$.guider({
		name: "2",
		next : "3",
		title : "E o que é uma equação?",
		description : "É uma fórmula matemática, que descreve como resolver o problema.",
		width : 600,
		alignButtons : "right",
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$.guider({
		name: "3",
		next : "4",
		title : "Qual o primeiro passo?",
		description : "Temos de ler o enunciado do problema e identificar os dados.",
		width : 600,
		alignButtons : "right",
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$.guider({
		name: "4",
		next : "5",
		title : "O que são dados?",
		description : "Dados são as informações concretas que o enunciado nos traz.",
		width : 600,
		alignButtons : "right",
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$("#line2").guider({
		name: "5",
		next : "6",
		title : "O que Matheus comprou?",
		description : "Por exemplo, sabemos que a Matheus comprou 1 televisão. Isso é um dado. Vamos selecionar essa informação no texto",
		width : 600,
		position: "bottom",
		alignButtons : "right",
		onShow: function() {allowSelection = true; selectText("Matheus comprou uma Televisão");},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$("#line5").guider({
		name: "6",
		next : "7",
		title : "Definindo um dado",
		description : "Agora vamos informar uma descrição para o dado e digitar o seu valor.",
		width : 600,
		position: "bottom",
		alignButtons : "right",
		onShow: function() {
			simulateTyping("#student_input_description", "Televisao");
			setTimeout(function () {
				simulateTyping("#student_input_data", "1");
		    }, 2000);
			setTimeout(function () {
				var e = jQuery.Event("keypress");
				e.which = 13;
				e.keyCode = 13;
				$("#student_input_data").trigger(e);
		    }, 3500);
		},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$("#line2").guider({
		name: "7",
		next : "8",
		title : "Em quantas parcelas ele pagou pela televisão?",
		description : "Matheus pagou pela televisão em 9 parcelas. Vamos selecionar esse dado no enunciado.",
		width : 600,
		position: "bottom",
		alignButtons : "right",
		onShow: function() {allowSelection = true; selectText("9 parcelas");},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$("#line6").guider({
		name: "8",
		next : "9",
		title : "Definindo um dado",
		description : "Agora vamos definir uma descrição e também digitar o valor do dado.",
		width : 600,
		position: "bottom",
		alignButtons : "right",
		onShow: function() {
			simulateTyping("#student_input_description", "Parcelas");
			setTimeout(function () {
				simulateTyping("#student_input_data", "9");
		    }, 1500);
			setTimeout(function () {
				var e = jQuery.Event("keypress");
				e.which = 13;
				e.keyCode = 13;
				$("#student_input_data").trigger(e);
		    }, 2500);
		},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$("#line2").guider({
		name: "9",
		next : "10",
		title : "Qual o valor de cada parcela?",
		description : "Matheus pagou pela televisão em 9 parcelas de R$ 136,00. Vamos selecionar esse dado no enunciado.",
		width : 600,
		position: "bottom",
		alignButtons : "right",
		onShow: function() {allowSelection = true; selectText("R$ 136,00");},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$("#line7").guider({
		name: "10",
		next : "11",
		title : "Definindo um dado",
		description : "Agora vamos definir uma descrição e também digitar o valor do dado.",
		width : 600,
		position: "bottom",
		alignButtons : "right",
		onShow: function() {
			simulateTyping("#student_input_description", "Valor da Parcela");
			setTimeout(function () {
				simulateTyping("#student_input_data", "136");
		    }, 2500);
			setTimeout(function () {
				var e = jQuery.Event("keypress");
				e.which = 13;
				e.keyCode = 13;
				$("#student_input_data").trigger(e);
		    }, 4000);
		},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$.guider({
		name: "11",
		next : "12",
		title : "Definindo uma incógnita",
		description : "Agora precisamos definir a incógnita. A incógnita é a pergunta que o enunciado quer responder.",
		width : 600,
		alignButtons : "right",
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$.guider({
		name: "12",
		next : "13",
		title : "Definindo uma incógnita",
		description : "Para selecionar incógnitas, precisamos utilizar o marcador verde. Para isto, você deve clicar sobre o marcador amarelo ali à direita da folha.",
		width : 600,
		alignButtons : "right",
		onShow: function() {selectMarker();},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$.guider({
		name: "13",
		next : "14",
		title : "Definindo uma incógnita",
		description : "Agora, com o marcador verde, vamos selecionar a pergunta no texto.",
		width : 600,
		alignButtons : "right",
		onShow: function() {allowSelection = true; selectText("Quanto o aparelho custou?");},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$("#line8").guider({
		name: "14",
		next : "15",
		title : "Definindo a Equação",
		description : "Após definir todas as relações é necessário definir a equação final.",
		width : 600,
		alignButtons : "right",
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$("#line8").guider({
		name: "15",
		next : "16",
		title : "Definindo a Equação",
		description : "Utilizando todos os dados e incógnitas já definidos, vamos definir uma equação que seja capaz de responder a pergunta do enunciado.",
		width : 600,
		alignButtons : "right",
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$("#line8").guider({
		name: "16",
		next : "17",
		title : "Definindo a Equação",
		description : "Sabemos que Matheus comprou apenas 1 televisão. Sabemos também que ele pagou por ela em 9 parcelas de R$ 136,00. Agora vamos por isso no papel, ou melhor, na tela.",
		width : 600,
		alignButtons : "right",
		onShow: function() {
			simulateTyping("#student_input_relation", "x=9*136");
			setTimeout(function () {
				var e = jQuery.Event("keypress");
				e.which = 13;
				e.keyCode = 13;
				$("#student_input_relation").trigger(e);
		    }, 2500);
		},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$.guider({
		name: "17",
		next : "18",
		title : "Terminamos!",
		description : "Agora você já sabe como resolver os problemas do PATtranslation 2.0.",
		width : 600,
		alignButtons : "right",
		onShow: function() {selectMarker();},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$.guider({
		name: "18",
		next : "19",
		title : "Dicas!",
		description : "Ah, quase me esqueci! A qualquer momento você pode clicar no botão dica, ali no canto superior direito da folha para exibir uma dica sobre o problema que estiver resolvendo",
		width : 600,
		alignButtons : "right",
		onShow: function() {selectMarker();},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$.guider({
		name: "19",
		next : "20",
		title : "Menu",
		description : "O Menu ali à esquerda com os problemas, vai desaparecer para que você se concentre apenas na folha e no exercício que estiver resolvendo. Mas não se preocupe, basta passar o mouse por ali que ele reaparecerá.",
		width : 600,
		alignButtons : "right",
		onShow: function() {selectMarker();},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$.guider({
		name: "20",
		next : "21",
		title : "Resolvendo as Equações",
		description : "Após concluir a resolução do problema, aparecerá um botão 'Resolver no PATequation'. Clicando nele você poderá resolver a equação para encontrar a resposta do problema.",
		width : 600,
		alignButtons : "right",
		onShow: function() {selectMarker();},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$.guider({
		name: "21",
		next : "22",
		title : "Terminamos!",
		description : "Agora vá para os próximos exercícios e tente resolvê-los.",
		width : 600,
		alignButtons : "right",
		onShow: function() {selectMarker();},
		buttons : {
			Finalizar: {
				click : true,
				className : "primary"
			}
		}
	});
}



function weTutorialPlano03() {
	$.guider({
		next: "1",
		title : "Tutorial do Plano 03",
		description : "Antes de começar a resolver os exercícios do Plano 03 vamos conferir mais alguns conceitos básicos.</center>",
		overlay : "dark",
		width : 600,
		alignButtons : "right",
		buttons : {
			Próximo: {
				click : true,
				className : "primary",
			}
		}
	}).show();
	
	$.guider({
		name: "1",
		next : "2",
		title : "Qual é o nosso objetivo?",
		description : "Já sabemos que o nosso primeiro objetivo é ler o enunciado e identificar os dados do problema. Então vamos lá!",
		width : 600,
		alignButtons : "right",
		onShow: function() {window.scrollTo(0, 50);},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$("#line5").guider({
		name: "2",
		next : "3",
		title : "Definindo um dado",
		description : "Segundo o enunciado, comprei 3 cadernos, vamos selecionar esse dado no texto e inserir seu valor.",
		width : 600,
		position: "bottom",
		alignButtons : "right",
		onShow: function() {
			allowSelection = true; 
			selectText("3 cadernos");
			simulateTyping("#student_input_description", "3 cadernos");
			setTimeout(function () {
				simulateTyping("#student_input_data", "3");
		    }, 2500);
			setTimeout(function () {
				var e = jQuery.Event("keypress");
				e.which = 13;
				e.keyCode = 13;
				$("#student_input_data").trigger(e);
		    }, 4000);
		},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$("#line6").guider({
		name: "3",
		next : "4",
		title : "Definindo um dado",
		description : "Vamos seguir lendo o enunciado. Paguei R$ 18,00 por cada caderno.",
		width : 600,
		position: "bottom",
		alignButtons : "right",
		onShow: function() {
			allowSelection = true; 
			selectText("R$ 18,00");
			simulateTyping("#student_input_description", "Valor de 1 Caderno");
			setTimeout(function () {
				simulateTyping("#student_input_data", "18");
		    }, 2500);
			setTimeout(function () {
				var e = jQuery.Event("keypress");
				e.which = 13;
				e.keyCode = 13;
				$("#student_input_data").trigger(e);
		    }, 4000);
		},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$("#line7").guider({
		name: "4",
		next : "5",
		title : "Definindo um dado",
		description : "Segundo o enunciado, comprei 2 borrachas, vamos selecionar esse dado no texto e inserir seu valor.",
		width : 600,
		position: "bottom",
		alignButtons : "right",
		onShow: function() {
			allowSelection = true; 
			selectText("2 borrachas");
			simulateTyping("#student_input_description", "2 borrachas");
			setTimeout(function () {
				simulateTyping("#student_input_data", "2");
		    }, 2500);
			setTimeout(function () {
				var e = jQuery.Event("keypress");
				e.which = 13;
				e.keyCode = 13;
				$("#student_input_data").trigger(e);
		    }, 4000);
		},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$("#line8").guider({
		name: "5",
		next : "6",
		title : "Definindo um dado",
		description : "Vamos seguir lendo o enunciado. Paguei R$ 1,00 por cada borracha.",
		width : 600,
		position: "bottom",
		alignButtons : "right",
		onShow: function() {
			allowSelection = true; 
			selectText("R$ 1,00");
			simulateTyping("#student_input_description", "Valor de 1 Borracha");
			setTimeout(function () {
				simulateTyping("#student_input_data", "1");
		    }, 2500);
			setTimeout(function () {
				var e = jQuery.Event("keypress");
				e.which = 13;
				e.keyCode = 13;
				$("#student_input_data").trigger(e);
		    }, 4000);
		},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$("#line9").guider({
		name: "6",
		next : "7",
		title : "Definindo um dado",
		description : "Segundo o enunciado, comprei 6 canetas, vamos selecionar esse dado no texto e inserir seu valor.",
		width : 600,
		position: "bottom",
		alignButtons : "right",
		onShow: function() {
			allowSelection = true; 
			selectText("meia dúzia de canetas");
			simulateTyping("#student_input_description", "6 canetas");
			setTimeout(function () {
				simulateTyping("#student_input_data", "6");
		    }, 2500);
			setTimeout(function () {
				var e = jQuery.Event("keypress");
				e.which = 13;
				e.keyCode = 13;
				$("#student_input_data").trigger(e);
		    }, 4000);
		},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$("#line10").guider({
		name: "7",
		next : "8",
		title : "Definindo um dado",
		description : "Vamos seguir lendo o enunciado. Paguei R$ 2,50 por cada caneta.",
		width : 600,
		position: "bottom",
		alignButtons : "right",
		onShow: function() {
			allowSelection = true; 
			selectText("R$ 2,50");
			simulateTyping("#student_input_description", "Valor de 1 Caneta");
			setTimeout(function () {
				simulateTyping("#student_input_data", "2,50");
		    }, 2500);
			setTimeout(function () {
				var e = jQuery.Event("keypress");
				e.which = 13;
				e.keyCode = 13;
				$("#student_input_data").trigger(e);
		    }, 4000);
		},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$("#line11").guider({
		name: "8",
		next : "10",
		title : "Identificando a incógnita",
		description : "Lendo o que restou do enunciado, qual a pergunta que ele quer responder? Vamos pegar a caneta verde e selecionar a pergunta no texto.",
		width : 600,
		position: "bottom",
		alignButtons : "right",
		onShow: function() {selectMarker(); allowSelection = true; selectText("Quanto gastei?");},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$("#line11").guider({
		name: "10",
		next : "11",
		title : "Identificando as relações",
		description : "Apareceu a calculadora para nos auxiliar. Vamos agora definir as relações. Primeiramente, vamos descobrir quanto gastamos ao todo com os cadernos. Sabemos que comprei 3 cadernos, e que cada um custou R$ 18,00.",
		width : 600,
		alignButtons : "right",
		onShow: function() {
			simulateTyping("#student_input_description", "custo dos cadernos");
			setTimeout(function () {
				simulateTyping("#student_input_relation", "3*18");
		    }, 3000);
			setTimeout(function () {
				var e = jQuery.Event("keypress");
				e.which = 13;
				e.keyCode = 13;
				$("#student_input_relation").trigger(e);
		    }, 4500);
		},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$("#line12").guider({
		name: "11",
		next : "12",
		title : "Identificando as relações",
		description : "Vamos descobrir quanto gastamos ao todo com as borrachas. Sabemos que comprei 2, e que cada uma custou R$ 1,00",
		width : 600,
		alignButtons : "right",
		onShow: function() {
			simulateTyping("#student_input_description", "custo das borrachas");
			setTimeout(function () {
				simulateTyping("#student_input_relation", "2*1");
		    }, 3000);
			setTimeout(function () {
				var e = jQuery.Event("keypress");
				e.which = 13;
				e.keyCode = 13;
				$("#student_input_relation").trigger(e);
		    }, 4500);
		},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$("#line13").guider({
		name: "12",
		next : "13",
		title : "Identificando as relações",
		description : "Vamos descobrir quanto gastamos ao todo com as canetas. Sabemos que comprei 6, e que cada uma custou R$ 2,50.",
		width : 600,
		alignButtons : "right",
		onShow: function() {
			simulateTyping("#student_input_description", "custo das canetas");
			setTimeout(function () {
				simulateTyping("#student_input_relation", "6*2.5");
		    }, 3000);
			setTimeout(function () {
				var e = jQuery.Event("keypress");
				e.which = 13;
				e.keyCode = 13;
				$("#student_input_relation").trigger(e);
		    }, 4500);
		},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$("#line14").guider({
		name: "13",
		next : "14",
		title : "Montando a equação",
		description : "Agora precisamos montar uma equação que resolva o problema. Vamos somar todas as nossas relações para isso.",
		width : 600,
		alignButtons : "right",
		onShow: function() {
			simulateTyping("#student_input_relation", "x=(3*18)+(2*1)+(6*2.5)");
			setTimeout(function () {
				var e = jQuery.Event("keypress");
				e.which = 13;
				e.keyCode = 13;
				$("#student_input_relation").trigger(e);
		    }, 3500);
		},
		buttons : {
			Próximo: {
				click : true,
				className : "primary"
			}
		}
	});
	
	$.guider({
		name: "14",
//		next : "15",
		title : "Terminamos!",
		description : "Agora você já sabe como resolver os problemas do Plano 03. Passe para o próximo exercício e tente resolvê-lo.",
		width : 600,
		alignButtons : "right",
		buttons : {
			Finalizar: {
				click : true,
				className : "primary"
			}
		}
	});
}