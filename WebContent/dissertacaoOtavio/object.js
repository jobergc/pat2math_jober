/**
 * @author Felipe de Morais
 * 
 * @description Constructor of a equation
 * 
 * @param {String} equation - equation with pat2math text notation
 * @param {int} points - the total of points of a equation
 *      
 * */
function Equation(equation, points) {
    this.equation = equation;  //the equation string in pat2math text notation 
    this.equationToString = replaceAll (equation, "*(", "(");
    this.points = points; //maximum points of this equation (come from the system)
    this.userPoints = 0; //points that the user get
    this.userPassNumber = 0; //number of pass concluded in this equation
    this.userErrorPoints = 0; // error points in each pass of the resolution
    this.nAnswers = 1; // number of answears that the user entry
    this.twoAnswers = false; // if this equation has two (true) or one (false) answears
    this.initialEquation = equation;// use just for bhaskara, contains the initial valid equation
    this.isComplete = false; // if the equation is complete
    this.isAnswer; //if the system are waiting for a response
    this.lastStep = null; //a object Step with the last valid step from user
    this.currentStep = ""; //the current step, this step is the one that the user is setting (no checked still) and in the natural format
    this.steps = new Array(); //array of Step (object Step)

    this.addPoints = addPoints;
 
    function addPoints(value) {
    	if (value > 0) { 
            this.userPassNumber += 1;        
            this.userPoints += value;
    	}
    	
    	else {
    		this.userPoints += value;
            this.userErrorPoints -= value;     
        }
    	
    	calculatePoints(this);
    	
    	if (levelGamification !== undefined && levelGamification !== "without" && isLoadEquation === false) {
    		updateScoreEquationCookie();
    		
    		if (levelGamification === "full")
    			addOrRemoveScore(value);
    	}
    	
    	

    }

    this.isAnswer = isAnswer;
    function isAnswer() {
        if ((points / 10) === this.userPassNumber + 1) {
            return true;
        }
        return false;
    }
}

function Step(step, type) {
    this.step = step; //string that contains the step of resolution
    this.type = type; //type of the step (NORMAL_STEP, NORMAL_SOLUTION, DELTA_SOLUTION, x1_SOLUTION e x2_SOLUTION)
}

function TaskItem (taskSetID, taskID){
	this.taskSetID=taskSetID; //The id of the set of tasks
	this.taskID = taskID; // the id of the task
}

function Regra (nome, explicacao) {
	this.nome = nome;
	this.explicacao = explicacao;
}

//function EquationPlan (idFirstEquation, idNextPlan) {
//	this.idFirstEquation = idFirstEquation;
//	this.idNextPlan = idNextPlan;
//}

function Fraction (numerator, denominator) {
	this.numerator = numerator;
	this.denominator = denominator;
	this.toString = numerator + "/" + denominator;
}

function ResolutionEquation(id) {
	this.id = id;
	this.steps = new Array();
}

//O termo passado como parametro deve estar no formato [coeficiente]x ou somente o coeficiente se nao for um termo em funcao de X
//Se for uma fracao, o coeficiente 
function Term(term, position) {
	this.toString = term;
	
	if (term.indexOf("(") !== -1) {
		term = term.replace("(", "");
		term = term.replace(")", "");
	}
	
	if (term === "x") {
		term = "1x";
	}
	
	else if (term === "-x") {
		term = "-1x";
	}

	//O JavaScript consegue converter para numero mesmo se houver outros caracteres depois do numero
	//Por exemplo, ele consegue converter 42x para 42
	this.coefficient = parseInt(term); 
	this.hasX = term.indexOf("x") !== -1; //Indica se o termo possui a incognita x ou se eh somente uma constante numerica
	this.isPositive = this.coefficient >= 0; //Indica se o termo eh positivo (true) ou negativo (false)
	this.position = position; //Objeto do tipo position declarado mais abaixo
}

//O sinal do termo tambem deve ser incluido na posicao, ver exemplo mais abaixo
function Position(start, end) {
	this.start = start; //Posicao inicial da String que o termo faz parte
	this.end = end; //Posicao final da String que o termo faz parte
	//Exemplo: O termo +2x esta na posicao (5, 7) na String 5-10x+2x+10
}

//Caso em que o termo eh uma fracao
//O termo passado como parametro deve estar no formato [numerador]/[denominador]
function FractionalTerm(term, position) {
	var barPosition = term.indexOf("/");
	var barPositionInEquation = barPosition + position.start;
	
	//Os parenteses devem ser desconsiderados na posicao
	var positionNumerator = new Position(position.start + 1, barPositionInEquation - 2);
	var positionDenominator = new Position(barPositionInEquation + 2, position.end - 1);
	
	var split = term.split(")/(");
	split[0] = split[0].replace("(", ""); //Remove o primeiro parentese
	split[1] = split[1].replace(")", ""); //Remove o ultimo parentese
	
	this.position = position;
	this.isPositive = term[0] === "+" || term[0] === "(";
	this.toString = term;
	this.numerator = null;
	this.denominator = null;
	
	if (findTermWithDistributivePropertyFromStart(split[0]) !== null) {
		this.numerator = new TermWithDistributiveProperty(split[0], positionNumerator)
	}
	
	//Lembrando que se o operador estiver na primeira posicao, na verdade ele eh um sinal
	else if (hasOperator(split[0], 1)) {
		this.numerator = new CompoundTerm(split[0], positionNumerator);
	}
	
	else {
		this.numerator = new Term(split[0], positionNumerator);
	}
	
	if (findTermWithDistributivePropertyFromStart(split[1]) !== null) {
		this.denominator = new TermWithDistributiveProperty(split[0], positionDenominator)
	}
	
	else if (hasOperator(split[1], 1)) {
		this.denominator = new CompoundTerm(split[1], positionDenominator);
	}
	
	else {
		this.denominator = new Term(split[1], positionDenominator);
	}
	
	this.hasX = this.numerator.hasX || this.denominator.hasX;
}

//Os termos compostos possuem uma ou mais operacoes. Por exemplo: 4x + 2
function CompoundTerm(term, position) {
	this.terms = findAllTerms(term);
	
	for (var i = 0; i < this.terms.length; i++) {
		this.terms[i] = this.terms[i].term;
	}
	
	this.toString = term;
	this.position = position;
	this.operators = findOperators(term);
}

//Expressoes como +(5-x+4) ou -(10x+2)
function ExpressionInParentheses(term, position) {
	this.operator = term[0];
	
	var termInParentheses = term.substring(2, term.length - 1);
	var termInParenthesesPosition = new Position(position.start + 2, position.end - 1);
	
	this.isPositive = this.operator === "+";
	this.termInParentheses = new CompoundTerm(termInParentheses, termInParenthesesPosition);
	this.toString = term;
	this.position = position;
}

function TermWithDistributiveProperty(term, position) {
	var split;
	
	if (term.indexOf("*") !== -1) {
		split = term.split("*(");
	}
	
	else {
		split = term.split("(");
	}
	
	var termInParentheses = split[1].replace(")", "");
	var termInParenthesesPosition = new Position(term.indexOf("(") + 1, position.end - 1);
	
	var multiplierPosition = new Position(position.start, position.start + split[0].length);
	this.multiplier = new Term(split[0], multiplierPosition); //Termo que esta fora do parenteses multiplicando
	
	this.isPositive = parseFloat(split[0]) >= 0;
	this.termInParentheses = new CompoundTerm(termInParentheses, termInParenthesesPosition);
	this.toString = term;
	this.position = position;
}

function TermGeneral(term) {
	this.term = term;
	this.position = term.position;
	this.type = "Term";
	this.hasX = true;
	
	if (term instanceof Term || term instanceof FractionalTerm) {
		this.hasX = term.hasX;
		
		if (term instanceof FractionalTerm) {
			this.type = "FractionalTerm";
		}
	}
	
	//Os termos abaixo sempre possuem x
	else if (term instanceof TermWithDistributiveProperty) {
		this.type = "TermWithDistributiveProperty";
	}
	
	else if (term instanceof ExpressionInParentheses) {
		this.type = "ExpressionInParentheses";
	}
	
	else {
		this.type = "CompoundTerm";
	}
}

function EquationTerms(equation) {
	this.termsLeftSide = findAllTermsOnSide(equation, "left");
	this.termsRightSide = findAllTermsOnSide(equation, "right");
	this.toString = equation;
}

function StepSimplification(numerator, denominator, newNumerator, newDenominator, divisor) {
    this.numerator = numerator;
    this.denominator = denominator;
    this.newNumerator = newNumerator;
    this.newDenominator = newDenominator;
    this.divisor = divisor;
}

function StepDataBase(step, isCorrect, isFinalAnswer, idEquation) {
	this.step = step;
	this.isCorrect = isCorrect;
	this.isFinalAnswer = isFinalAnswer;
	this.idEquation = idEquation;
}