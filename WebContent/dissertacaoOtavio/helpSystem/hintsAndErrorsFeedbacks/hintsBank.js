//PORTUGUES (BRASIL)
hints_ptBR["-x=c"] = ["Observe que o X dessa equação é negativo, e para a resposta final precisamos que ele seja positivo. Como podemos torná-lo positivo sem prejudicar a igualdade da equação?",
					  "Se você multiplicar os dois lados da equação por um número qualquer (positivo ou negativo), a igualdade não será prejudicada. Por qual número você pode multiplicar os dois lados da equação para que X se torne positivo, e sem alterar os valores numéricos?",
                      "Se você multiplicar toda a equação por 1, ela continuará exatamente a mesma. O que você pode colocar na frente de 1 para que a multiplicação altere os sinais da equação e torne X positivo?",
                      "Multiplique os dois lados da equação por -1",
                      "O próximo passo da equação (a linha inteira) é [STEP]"];
hints_ptBR["-x=-c"] = hints_ptBR["-x=c"];

hints_ptBR["x=b+c"] = ["Existem dois números que podem ser somados, tente descobrir quais são eles",
					   "Os números [b] e [c] devem ser somados",
					   "Qual é o resultado da soma [b] + [c]?",
					   "O resultado da soma [b] + [c] é [RESULT]",
					   "O próximo passo da equação (a linha inteira) é [STEP]"];
hints_ptBR["-x=b+c"] = hints_ptBR["x=b+c"];
hints_ptBR["ax=b+c"] = hints_ptBR["x=b+c"];
hints_ptBR["-ax=b+c"] = hints_ptBR["x=b+c"];

hints_ptBR["x=b-c"] = ["Existem dois números que podem ser subtraídos, tente descobrir quais são eles",
	   "Os números [b] e [c] devem ser subtraídos",
	   "Qual é o resultado da diferença [b] - [c]?",
	   "O resultado da diferença [b] - [c] é [RESULT]",
	   "O próximo passo da equação (a linha inteira) é [STEP]"];
hints_ptBR["-x=b-c"] = hints_ptBR["x=b-c"];
hints_ptBR["ax=b-c"] = hints_ptBR["x=b-c"];
hints_ptBR["-ax=b-c"] = hints_ptBR["x=b-c"];

hints_ptBR["x=b*c"] = ["Existem dois números que podem ser multiplicados, tente descobrir quais são eles",
					   "Os números [b] e [c] devem ser multiplicados",
					   "Qual é o resultado do produto [b] * [c]?",
					   "O resultado do produto [b] * [c] é [RESULT]",
					   "O próximo passo da equação (a linha inteira) é [STEP]"];
hints_ptBR["-x=b*c"] = hints_ptBR["x=b*c"];
hints_ptBR["ax=b*c"] = hints_ptBR["x=b*c"];
hints_ptBR["-ax=b*c"] = hints_ptBR["x=b*c"];

hints_ptBR["x=b/c"] = [new Array(), new Array()];
//Se a divisao tem como resultado um numero inteiro
hints_ptBR["x=b/c"][0] = ["Existem dois números que podem ser divididos, tente descobrir quais são eles",
						  "Os números [b] e [c] devem ser divididos",
						  "Qual é o resultado da divisão [b] / [c]?",
						  "O resultado da divisão [b] / [c] é [RESULT]",
						  "O próximo passo da equação (a linha inteira) é [STEP]"];
//Se a divisao tem como resultado uma fracao
hints_ptBR["x=b/c"][1] = ["A fração [b] / [c] deve ser simplificada",
						  "Para simplificar uma fração, você deve dividir o numerador e o denominador pelo mesmo número.",
						  "Lembre-se dos critérios de divisibilidade para escolher o próximo número que pode dividir o numerador e o denominador ao mesmo tempo. <a href='https://brasilescola.uol.com.br/matematica/criterios-divisibilidade.htm'>Clique aqui</a> para conferir esses critérios",
						  "Divida o numerador e o denominador pelo número [NUMBER]",
						  "O próximo passo da equação (a linha inteira) é [STEP]"];
hints_ptBR["-x=b/c"] = hints_ptBR["x=b/c"];
hints_ptBR["ax=b/c"] = hints_ptBR["x=b/c"];
hints_ptBR["-ax=b/c"] = hints_ptBR["x=b/c"];

hints_ptBR["x=-b+c"] = ["Existem dois números que podem ser somados, tente descobrir quais são eles",
					    "Os números [b] e [c] devem ser somados",
					    "Você sabia que a soma [b] + [c] pode ser calculada da forma [c] - [b]?",
					    "O resultado da soma [b] + [c] é [RESULT]",
					    "O próximo passo da equação (a linha inteira) é [STEP]"];
hints_ptBR["-x=-b+c"] = hints_ptBR["x=-b+c"];
hints_ptBR["ax=-b+c"] = hints_ptBR["x=-b+c"];
hints_ptBR["-ax=-b+c"] = hints_ptBR["x=-b+c"];

hints_ptBR["x=-b-c"] = ["Existem dois números que podem ser subtraídos, tente descobrir quais são eles",
					    "Os números [b] e [c] devem ser subtraídos",
					    "Você pode considerar a diferença [b] - [c] como uma soma de dívidas",
					    "O resultado da diferença  [b] - [c] é [RESULT]",
					    "O próximo passo da equação (a linha inteira) é [STEP]"];
hints_ptBR["-x=-b-c"] = hints_ptBR["x=b-c"];
hints_ptBR["ax=-b-c"] = hints_ptBR["x=b-c"];
hints_ptBR["-ax=-b-c"] = hints_ptBR["x=b-c"];

hints_ptBR["x=-b*c"] = ["Existem dois números que podem ser multiplicados, tente descobrir quais são eles",
					    "Os números [b] e [c] devem ser multiplicados",
					    "Qual é o resultado do produto [b] * [c]? Lembre-se que a multiplicação de um número positivo com um número negativo resulta em um número negativo",
					    "O resultado do produto [b] * [c] é [RESULT]",
					    "O próximo passo da equação (a linha inteira) é [STEP]"];
hints_ptBR["x=b*-c"] = hints_ptBR["x=-b*c"];
hints_ptBR["-x=-b*c"] = hints_ptBR["x=-b*c"]
hints_ptBR["-x=b*-c"] = hints_ptBR["x=-b*c"];
hints_ptBR["ax=-b*c"] = hints_ptBR["x=-b*c"];
hints_ptBR["ax=b*-c"] = hints_ptBR["x=-b*c"];
hints_ptBR["-ax=-b*c"] = hints_ptBR["x=-b*c"]
hints_ptBR["-ax=b*-c"] = hints_ptBR["x=-b*c"];

hints_ptBR["x=-b*-c"] = ["Existem dois números que podem ser multiplicados, tente descobrir quais são eles",
					     "Os números [b] e [c] devem ser multiplicados",
					     "Qual é o resultado do produto [b] * [c]? Lembre-se que a multiplicação de dois números negativos resulta em um número positivo",
					     "O resultado do produto [b] * [c] é [RESULT]",
					     "O próximo passo da equação (a linha inteira) é [STEP]"];
hints_ptBR["-x=-b*-c"] = hints_ptBR["x=-b*c"];
hints_ptBR["ax=-b*-c"] = hints_ptBR["x=-b*c"];
hints_ptBR["-ax=-b*-c"] = hints_ptBR["x=-b*c"];

hints_ptBR["x=-b/c"] = [new Array(), new Array()];
//Se a divisao tem como resultado um numero inteiro
hints_ptBR["x=-b/c"][0] = ["Existem dois números que podem ser divididos, tente descobrir quais são eles",
						  "Os números [b] e [c] devem ser divididos",
						  "Qual é o resultado da divisão [b] / [c]? Lembre-se que a divisão de um número positivo por um número negativo (ou vice-versa) resulta em um número negativo",
						  "O resultado da divisão [b] / [c] é [RESULT]",
						  "O próximo passo da equação (a linha inteira) é [STEP]"];
//Se a divisao tem como resultado uma fracao
hints_ptBR["x=-b/c"][1] = hints_ptBR["x=b/c"][1];

hints_ptBR["x=b/-c"] = hints_ptBR["x=-b/c"];
hints_ptBR["-x=-b/c"] = hints_ptBR["x=-b/c"];
hints_ptBR["-x=b/-c"] = hints_ptBR["x=-b/c"];
hints_ptBR["ax=-b/c"] = hints_ptBR["x=-b/c"];
hints_ptBR["ax=b/-c"] = hints_ptBR["x=-b/c"];
hints_ptBR["-ax=-b/c"] = hints_ptBR["x=-b/c"];
hints_ptBR["-ax=b/-c"] = hints_ptBR["x=-b/c"];


hints_ptBR["x=-b/-c"] = [new Array(), new Array()];
//Se a divisao tem como resultado um numero inteiro
hints_ptBR["x=-b/-c"][0] = ["Existem dois números que podem ser divididos, tente descobrir quais são eles",
						  "Os números [b] e [c] devem ser divididos",
						  "Qual é o resultado da divisão [b] / [c]? Lembre-se que a divisão de dois números negativos resulta em um número positivo",
						  "O resultado da divisão [b] / [c] é [RESULT]",
						  "O próximo passo da equação (a linha inteira) é [STEP]"];
//Se a divisao tem como resultado uma fracao
hints_ptBR["x=-b/-c"][1] = hints_ptBR["x=b/c"][1];

hints_ptBR["-x=-b/-c"] = hints_ptBR["x=-b/-c"];
hints_ptBR["ax=-b/-c"] = hints_ptBR["x=-b/-c"];
hints_ptBR["-ax=-b/-c"] = hints_ptBR["x=-b/-c"];

hints_ptBR["c=x"] = ["Você percebeu que os termos dessa equação estão em lados trocados?",
					 "Lembre-se que objetivo é sempre deixar os termos em função de X no lado esquerdo e os números no lado direito da equação",
					 "O que é necessário fazer para passar o termo [x] para o lado esquerdo e [c] para o lado direito?",
					 "Troque os termos de lado da equação, passando [x] para o lado esquerdo e [c] para o lado direito. Não é necessário inverter as suas operações, pois eles são os dois únicos elementos da equação",
					 "O próximo passo da equação (a linha inteira) é [STEP]"];
hints_ptBR["c=-x"] = hints_ptBR["c=x"];
hints_ptBR["c=ax"] = hints_ptBR["c=x"];
hints_ptBR["c=-ax"] = hints_ptBR["c=x"];
hints_ptBR["-c=x"] = hints_ptBR["c=x"];
hints_ptBR["-c=-x"] = hints_ptBR["c=x"];
hints_ptBR["-c=ax"] = hints_ptBR["c=x"];
hints_ptBR["-c=-ax"] = hints_ptBR["c=x"];

hints_ptBR["x+b=c"] = ["Existem dois termos que podem ser operados, porém eles estão em lados diferentes da equação. O que você deve fazer nesse caso?",
					   "Os termos [b] e [c] devem ser operados, mas eles estão em diferentes lados da equação. Adicionalmente, lembre-se que o objetivo é deixar os números no lado direito da equação",
					   "A operação que deve ser efetuada nos dois lados da equação é a inversa do termo [b]. Considerando que [b] está somando à X, qual é a operação inversa da adição?",
					   "A operação inversa da adição é a subtração. Assim, subtraia os dois lados da equação pelo número [b]",
					   "O próximo passo da equação (a linha inteira) é [STEP]"];
hints_ptBR["x+b=-c"] = hints_ptBR["x+b=c"];
hints_ptBR["-x+b=c"] = hints_ptBR["x+b=c"];
hints_ptBR["-x+b=-c"] = hints_ptBR["x+b=c"];
hints_ptBR["ax+b=c"] = hints_ptBR["x+b=c"];
hints_ptBR["ax+b=-c"] = hints_ptBR["x+b=c"];
hints_ptBR["-ax+b=c"] = hints_ptBR["x+b=c"];
hints_ptBR["-ax+b=-c"] = hints_ptBR["x+b=c"];

hints_ptBR["x-b=c"] = ["Existem dois termos que podem ser operados, porém eles estão em lados diferentes da equação. O que você deve fazer nesse caso?",
					   "Os termos [b] e [c] devem ser operados, mas eles estão em diferentes lados da equação. Adicionalmente, lembre-se que o objetivo é deixar os números no lado direito da equação",
					   "A operação que deve ser efetuada nos dois lados da equação é a inversa do termo [b]. Considerando que [b] está subtraindo de X, qual é a operação inversa da subtração?",
					   "A operação inversa da subtração é a adição. Assim, some os dois lados da equação pelo número [b]",
	   "O próximo passo da equação (a linha inteira) é [STEP]"];
hints_ptBR["x-b=-c"] = hints_ptBR["x-b=c"];
hints_ptBR["-x-b=c"] = hints_ptBR["x-b=c"];
hints_ptBR["-x-b=-c"] = hints_ptBR["x-b=c"];
hints_ptBR["ax-b=c"] = hints_ptBR["x-b=c"];
hints_ptBR["ax-b=-c"] = hints_ptBR["x-b=c"];
hints_ptBR["-ax-b=c"] = hints_ptBR["x-b=c"];
hints_ptBR["-ax-b=-c"] = hints_ptBR["x-b=c"];

hints_ptBR["ax=c"] = ["Nessa equação, X possui um coeficiente. O que é necessário fazer para remover esse coeficiente de maneira que X fique isolado no lado esquerdo da equação?",
					  "Para remover o coeficiente de X, você deve efetuar uma operação nos dois lados da equação de maneira que esse coeficiente se torne 1",
					  "A operação que deve ser efetuada nos dois lados da equação é a inversa do coeficiente. Considerando que o coeficiente está multiplicando X, qual é a operação inversa da multiplicação?",
					  "A operação inversa da multiplicação é a divisão. Assim, divida os dois lados da equação pelo valor do coeficiente [a]",
					  "O próximo passo da equação (a linha inteira) é [STEP]"];
hints_ptBR["ax=-c"] = hints_ptBR["ax=c"];

hints_ptBR["-ax=c"] = ["Nessa equação, X possui um coeficiente. O que é necessário fazer para remover esse coeficiente de maneira que X fique isolado no lado esquerdo da equação?",
					   "Para remover o coeficiente de X, você deve efetuar uma operação nos dois lados da equação de maneira que esse coeficiente se torne 1",
					   "A operação que deve ser efetuada nos dois lados da equação é a inversa do coeficiente. Considerando que o coeficiente está multiplicando X, qual é a operação inversa da multiplicação?",
					   "A operação inversa da multiplicação é a divisão. Assim, divida os dois lados da equação pelo valor do coeficiente [a] (lembre-se que o coeficiente é um número negativo, e na divisão você deverá perservar o sinal de menos). ",
					   "O próximo passo da equação (a linha inteira) é [STEP]"];
hints_ptBR["-ax=-c"] = hints_ptBR["-ax=c"];

hints_ptBR["x/b=c"] = ["Nessa equação, X está em uma fração. O que é necessário fazer para remover o denominador da fração de maneira que X fique isolado no lado esquerdo da equação?",
					   "Para remover o denominador, você deve efetuar uma operação nos dois lados da equação de maneira que esse denominador se torne 1",
					   "A operação que deve ser efetuada nos dois lados da equação é a inversa do denominador. Considerando que o denominador está dividindo X, qual é a operação inversa da divisão?",
					   "A operação inversa da divisão é a multiplicação. Assim, multiplique os dois lados da equação pelo valor do denominador [b]",
					   "O próximo passo da equação (a linha inteira) é [STEP]"];
hints_ptBR["x/b=-c"] = hints_ptBR["x/b=c"];

hints_ptBR["-x/b=c"] = ["Nessa equação, X está em uma fração. O que é necessário fazer para remover o denominador da fração de maneira que X fique isolado no lado esquerdo da equação?",
					    "Para remover o denominador, você deve efetuar uma operação nos dois lados da equação de maneira que esse denominador se torne 1",
					    "A operação que deve ser efetuada nos dois lados da equação é a inversa do denominador. Considerando que o denominador está dividindo X, qual é a operação inversa da divisão?",
					    "A operação inversa da divisão é a multiplicação. Assim, multiplique os dois lados da equação pelo valor do denominador [b] (para que X não continue negativo, você pode multiplicar os dois lados por -[b])",
					    "O próximo passo da equação (a linha inteira) é [STEP]"];
hints_ptBR["-x/b=-c"] = hints_ptBR["x/b=c"];

hints_ptBR["ax/b=c"] = [new Array(), new Array(), new Array()];
//Math.abs(a) = Math.abs(b)
hints_ptBR["ax/b=c"][0] = ["Existem dois números que podem ser divididos, tente descobrir quais são eles",
						   "Apesar de [a] estar multiplicando o X, você ainda pode dividir [a] por [b]",
						   "Observe que o número que está multiplicando X é igual ao denominador da fração. Qual é o resultado da divisão de um número por ele mesmo? (nesse caso seria [a] / [b], mas lembre-se que o resultado sempre é o mesmo na divisão de números iguais)",
						   "O resultado da divisão [a] / [b] é 1. Lembre-se que quando o coeficiente de X é 1, não é necessário explicitá-lo na equação; assim você pode escrever apenas X",
						   "O próximo passo da equação (a linha inteira) é [STEP]"];
//ax/b pode ser simplificada
hints_ptBR["ax/b=c"][1] = ["A fração [a] / [b] deve ser simplificada (por mais que [a] esteja multiplicando X, as regras de simplificação são as mesmas)",
						   "Para simplificar uma fração, você deve dividir o numerador e o denominador pelo mesmo número",
						   "Lembre-se dos critérios de divisibilidade para escolher o próximo número que pode dividir o numerador e o denominador ao mesmo tempo. <a href='https://brasilescola.uol.com.br/matematica/criterios-divisibilidade.htm'>Clique aqui</a> para conferir esses critérios",
						   "Divida o numerador e o denominador pelo número [NUMBER]",
						   "O próximo passo da equação (a linha inteira) é [STEP]"];
//ax/b nao pode ser simplificada
hints_ptBR["ax/b=c"][2] = ["Nessa equação, X e o seu coeficiente estão em uma fração e não é possível simplificar. O que é necessário fazer para remover o denominador da fração de maneira que X e seu coeficiente fiquem isolados no lado esquerdo da equação?",
						   "Para remover o denominador, você deve efetuar uma operação nos dois lados da equação de maneira que esse denominador se torne 1",
						   "A operação que deve ser efetuada nos dois lados da equação é a inversa do denominador. Considerando que o denominador está dividindo X e seu coeficiente, qual é a operação inversa da divisão?",
						   "A operação inversa da divisão é a multiplicação. Assim, multiplique os dois lados da equação pelo valor do denominador [b]",
						   "O próximo passo da equação (a linha inteira) é [STEP]"];

hints_ptBR["-ax/b=c"] = [new Array(), new Array(), new Array()];
hints_ptBR["-ax/b=c"][0] = ["Existem dois números que podem ser divididos, tente descobrir quais são eles",
	   "Apesar de [a] estar multiplicando o X, você ainda pode dividir [a] por [b]",
	   "Observe que o número que está multiplicando X possui o mesmo valor absoluto que o denominador da fração (isto é, os números são iguais desconsiderando o sinal). Qual é o resultado da divisão de um número positivo por um número negativo com o mesmo valor absoluto? (nesse caso seria [a] / [b], mas lembre-se que o resultado sempre é o mesmo na divisão de dois números com o mesmo valor absoluto)",
	   "O resultado da divisão [a] / [b] é -1. Lembre-se que quando o coeficiente de X é -1, não é necessário explicitá-lo na equação; assim você pode escrever apenas -X",
	   "O próximo passo da equação (a linha inteira) é [STEP]"];
hints_ptBR["-ax/b=c"][1] = hints_ptBR["ax/b=c"][1];
hints_ptBR["-ax/b=c"][2] = hints_ptBR["ax/b=c"][2];

hints_ptBR["many terms with multiplication positive"] = ["Existem termos que podem ser multiplicados, tente descobrir quais são eles",
													 	 "Os termos [TERMS] devem ser multiplicados",
														 "Qual é o resultado do produto [TERMS MULTIPLICATION]?",
														 "O resultado do produto [TERMS MULTIPLICATION] é [RESULT]",
														 "O próximo passo da equação (a linha inteira) é [STEP]"];
hints_ptBR["many terms with multiplication negative"] = ["Existem termos que podem ser multiplicados, tente descobrir quais são eles",
														 "Os termos [TERMS] devem ser multiplicados",
														 "Qual é o resultado do produto [TERMS MULTIPLICATION]? Lembre-se que a multiplicação de dois números negativos resulta em um número positivo",
														 "O resultado do produto [TERMS MULTIPLICATION] é [RESULT]",
														 "O próximo passo da equação (a linha inteira) é [STEP]"];
hints_ptBR["many terms with multiplication positive and negative"] = ["Existem termos que podem ser multiplicados, tente descobrir quais são eles",
																	  "Os termos [TERMS] devem ser multiplicados",
																	  "Qual é o resultado do produto [TERMS MULTIPLICATION]? Lembre-se que a multiplicação de um número positivo com um número negativo resulta em um número negativo",
																	  "O resultado do produto [TERMS MULTIPLICATION] é [RESULT]",
																	  "O próximo passo da equação (a linha inteira) é [STEP]"];



hints_ptBR["numbers on different sides (left is positive)"] = ["Existem dois termos que podem ser operados, porém eles estão em lados diferentes da equação. O que você deve fazer nesse caso?",
															   "Os termos [a] e [b] devem ser operados, mas eles estão em diferentes lados da equação. Para resolver esse problema, você deve efetuar uma operação nos dois lados da equação de maneira que [a] se torne zero. Adicionalmente, lembre-se que o objetivo é deixar os números no lado direito da equação",
															   "A operação que deve ser efetuada nos dois lados da equação é a inversa do termo [a]. Considerando que [a] está somando à X, qual é a operação inversa da adição?",
															   "A operação inversa da adição é a subtração. Assim, subtraia os dois lados da equação pelo número [a]",
															   "O próximo passo da equação (a linha inteira) é [STEP]"];

hints_ptBR["numbers on different sides (left is negative)"] = ["Existem dois termos que podem ser operados, porém eles estão em lados diferentes da equação. O que você deve fazer nesse caso?",
															   "Os termos [a] e [b] devem ser operados, mas eles estão em diferentes lados da equação. Para resolver esse problema, você deve efetuar uma operação nos dois lados da equação de maneira que [a] se torne zero. Adicionalmente, lembre-se que o objetivo é deixar os números no lado direito da equação",
															   "A operação que deve ser efetuada nos dois lados da equação é a inversa do termo [a]. Considerando que [a] está subtraindo de X, qual é a operação inversa da subtração?",
															   "A operação inversa da subtração é a adição. Assim, some os dois lados da equação pelo número [a]",
															   "O próximo passo da equação (a linha inteira) é [STEP]"];

hints_ptBR["sum of terms with x on the same side"] = ["Existem dois termos semelhantes que podem ser somados, tente descobrir quais são eles",
													  "Os termos [a] e [b] devem ser somados",
													  "Para efetuar a adição de dois termos semelhantes, basta somar os números e preservar o X. Por exemplo, a soma 2x + 5x é igual a 7x",
													  "O resultado da soma [a] + [b] é [RESULT]",
													  "O próximo passo da equação (a linha inteira) é [STEP]"];

hints_ptBR["subtraction of terms with x on the same side"] = ["Existem dois termos semelhantes que podem ser subtraídos, tente descobrir quais são eles",
															  "Os termos [a] e [b] devem ser subtraídos",
															  "Para efetuar a subtração de dois termos semelhantes, basta subtrair os números e preservar o X. Por exemplo, a diferença 7x - 5x é igual a 2x",
															  "O resultado da diferença [a] - [b] é [RESULT]",
															  "O próximo passo da equação (a linha inteira) é [STEP]"];

hints_ptBR["terms with X on different sides (right is positive)"] = ["Existem dois termos que podem ser operados, porém eles estão em lados diferentes da equação. O que você deve fazer nesse caso?",
		   															 "Os termos [a] e [b] devem ser operados, mas eles estão em diferentes lados da equação. Para resolver esse problema, você deve efetuar uma operação nos dois lados da equação de maneira que a se torne zero. Adicionalmente, lembre-se que o objetivo é deixar os termos em função de X no lado esquerdo da equação",
		   															 "A operação que deve ser efetuada nos dois lados da equação é a inversa do termo [b]. Considerando que [b] possui o sinal de mais, qual é a operação inversa da adição?",
		   															 "A operação inversa da adição é a subtração. Assim, subtraia os dois lados da equação pelo termo [b]",
		   															 "O próximo passo da equação (a linha inteira) é [STEP]"];

hints_ptBR["terms with X on different sides (right is negative)"] = ["Existem dois termos que podem ser operados, porém eles estão em lados diferentes da equação. O que você deve fazer nesse caso?",
																	 "Os termos [a] e [b] devem ser operados, mas eles estão em diferentes lados da equação. Para resolver esse problema, você deve efetuar uma operação nos dois lados da equação de maneira que a se torne zero. Adicionalmente, lembre-se que o objetivo é deixar os termos em função de X no lado esquerdo da equação",
																	 "A operação que deve ser efetuada nos dois lados da equação é a inversa do termo [b]. Considerando que [b] possui o sinal de menos, qual é a operação inversa da subtração?",
																	 "A operação inversa da subtração é a adição. Assim, some os dois lados da equação pelo termo [b]",
																	 "O próximo passo da equação (a linha inteira) é [STEP]"];

hints_ptBR["parentheses with plus sign"] = ["Essa equação possui termos entre parênteses, tente descobrir alguma maneira para remover esses parênteses",
											"Observe que há um sinal de mais na frente dos parênteses [PARENTHESES]. Você lembra da regra do jogo de sinais?",
											"Quando há um sinal de mais na frente dos parênteses, você pode remover esses parênteses sem alterar os sinais dos termos que estão dentro deles",
											"Para o próximo passo, você só precisa escrever a equação removendo os parênteses e o sinal de mais que está na frente deles na expressão +[PARENTHESES]",
											"O próximo passo da equação (a linha inteira) é [STEP]"];

hints_ptBR["parentheses with minus sign"] = ["Essa equação possui termos entre parênteses, tente descobrir alguma maneira para remover esses parênteses",
											 "Observe que há um sinal de menos na frente dos parênteses [PARENTHESES]. Você lembra da regra do jogo de sinais?",
											 "Quando há um sinal de menos na frente dos parênteses, você deve inverter os sinais dos termos que estão dentro deles (os sinais de mais se tornam sinais de menos, e os sinais de menos passam a ser sinais de mais)",
											 "Para o próximo passo, você só precisa escrever a equação removendo os parênteses da expressão [PARENTHESES] e invertendo os sinais de seus termos. Eles ficarão da seguinte forma: [RESULT]",
											 "O próximo passo da equação (a linha inteira) é [STEP]"];

hints_ptBR["distributive property"] = ["Essa equação possui propriedade distributiva, isto é, um número multiplicando termos entre parênteses. O que você deve fazer nesse caso?",
									   "Para resolver uma propriedade distributiva, você precisa multiplicar o número que está fora dos parênteses pelo primeiro termo, e depois pelo segundo termo. Cuidado com o jogo de sinais",
									   "Observe um exemplo de resolução de uma propriedade distributiva: 4(2-5x) = 4*2-4*5x = 8-20x",
									   "O resultado da propriedade distributiva [DISTRIBUTIVE PROPERTY] é [RESULT]",
									   "O próximo passo da equação (a linha inteira) é [STEP]"];

hints_ptBR["reason and proportion"] = ["Essa equação é formada por uma razão e proporção, isto é, uma igualdade de frações. Para resolvê-la, você pode utilizar as mesmas regras da Regra de 3",
									   "Existem três maneiras para resolver uma razão e proporção. A primeira é utilizando o Mínimo Múltiplo Comum (MMC), mas existem formas mais simples. A segunda é a seguinte forma:<br>1. Multiplicar o denominador do lado esquerdo pelo numerador do lado direito<br>2. Multiplicar o denominador do lado direito pelo numerador do lado esquerdo",
									   "A outra maneira de resolver uma razão e proporção é partindo do princípio da operação inversa: quais termos seriam interessantes de passar para o lado oposto na equação?",
									   "Para o próximo passo, faça os seguintes procedimentos:<br>1. Passe o denominador do lado esquerdo ([LEFT DENOMINATOR]) da equação para o lado direito, multiplicando todos os seus termos<br>2. Em seguida, passe o denominador do lado direito ([RIGHT DENOMINATOR]) para o lado esquerdo, multiplicando todos os seus termos.<br><a href=# onclick=reasonAndProportionHelp()>Clique aqui para conferir a fórmula que representa essas instruções</a>",
									   "O próximo passo da equação (a linha inteira) é [STEP]"];

hints_ptBR["fractions with simple numerator"] = ["Essa equação é composta por frações com denominadores diferentes. O que você deve fazer nesse caso?",
												 "O seu objetivo será remover as frações da equação. A primeira etapa é calcular o Mínimo Múltiplo Comum (MMC) dos denominadores. Em seguida, tente descobrir em quais termos das frações você deve aplicar as operações de divisão e multiplicação",
												 "Após calcular o MMC, para cada termo da equação, você deverá dividir o valor obtido pelo seu denominador (se o termo não tiver denominador, considere-o como 1), e multiplicar pelo seu numerador. Observe que após esse procedimento você terá removido todos os denominadores",
												 "O valor do MMC de todos denominadores é [MMC]. Confira abaixo como ficam os cálculos:",
												 "O próximo passo da equação (a linha inteira) é [STEP]"];

hints_ptBR["fractions with compound numerator"] = ["Essa equação é composta por frações com denominadores diferentes. O que você deve fazer nesse caso?",
												   "O seu objetivo será remover as frações da equação. A primeira etapa é calcular o Mínimo Múltiplo Comum (MMC) dos denominadores. Em seguida, tente descobrir em quais termos das frações você deve aplicar as operações de divisão e multiplicação",
												   "Após calcular o MMC, para cada termo da equação, você deverá dividir o valor obtido pelo seu denominador (se o termo não tiver denominador, considere-o como 1), e multiplicar pelo seu numerador (cuidado com as frações que possuem uma expressão no lugar do numerador. A multiplicação deverá ser efetuada em todos os elementos dessa expressão, montando assim uma propriedade distributiva). Observe que após esse procedimento você terá removido todos os denominadores",
												   "O valor do MMC de todos denominadores é [MMC]. Confira abaixo como ficam os cálculos:",
												   "O próximo passo da equação (a linha inteira) é [STEP]"];

hints_ptBR["fractions with distributive property"] = ["Essa equação é composta por frações com denominadores diferentes. O que você deve fazer nesse caso?",
												   "O seu objetivo será remover as frações da equação. Se desejar, você pode calcular a propriedade distributiva antes de avançar para a etapa de calcular o Mínimo Múltiplo Comum (MMC) dos denominadores. Em seguida, tente descobrir em quais termos das frações você deve aplicar as operações de divisão e multiplicação",
												   "Após calcular o MMC, para cada termo da equação, você deverá dividir o valor obtido pelo seu denominador (se o termo não tiver denominador, considere-o como 1), e multiplicar pelo seu numerador (cuidado com as frações que possuem uma expressão ou propriedade distributiva no lugar do numerador). Observe que após esse procedimento você terá removido todos os denominadores",
												   "O valor do MMC de todos denominadores é [MMC]. Confira abaixo como ficam os cálculos:",
												   "O próximo passo da equação (a linha inteira) é [STEP]"];

hints_ptBR["fractions with multiplication"] = [new Array(), new Array()];
//O numero que esta multiplicando eh igual ao denominador da fracao
hints_ptBR["fractions with multiplication"][0] = ["Existem dois termos que podem ser multiplicados, tente descobrir quais são eles",
																 "Os termos [b] e [c] devem ser multiplicados. Lembre-se que você pode aplicar a regra do cancelamento nesse caso.",
																 'A regra do cancelamento consiste em "cortar" dois termos iguais de uma fração, respeitando as seguintes regras:<br>1. Um termo deve estar no numerador (ou multiplicando a fração) e o outro no denominador<br>2. O numerador deve ser composto apenas por um número ou por uma multiplicação para aplicar a regra, a não ser que um número externo esteja multiplicando a fração (nesse caso você pode cortar esse número com o denominador). <br><a href=# onclick=cancellationRuleHelp()>Clique aqui para conferir alguns exemplos</a>',
																 "Aplicando a regra do cancelamento, removemos o denominador da fração e o número que está multiplicando, tendo como resultado o próprio numerador da fração: [RESULT]",
																 "O próximo passo da equação (a linha inteira) é [STEP]"];
//O numero que esta multiplicando eh diferente do denominador da fracao
hints_ptBR["fractions with multiplication"][1] = ["Existem dois termos que podem ser multiplicados, tente descobrir quais são eles",
																 "Os termos [b] e [c] devem ser multiplicados. Você lembra como se multiplica uma fração por um número inteiro?",
																 "É só multiplicar o numerador da fração por esse número, e colocar o resultado no numerador (o denominador permanece o mesmo)",
																 "O resultado do produto [b] * [c] é [RESULT]",
																 "O próximo passo da equação (a linha inteira) é [STEP]"];

//************************************************************************************************************************
//INGLES (INGLATERRA)
hints_enGB["-"] = ["Notice that the X of this equation is negative and we need it to be positive for the final answer. How can we turn it positive without harming the equation's equality?", 
	   "If you multiply both equation's sides by any number (positive or negative), the equality won’t be injured. By what number can you multiply both sides so X turns positive without changing numeric values?",
	   "If you multiply the entire equation by 1, it’ll keep exactly the same. What can you place in front of 1 so the multiplication change the equation’s signs and makes X positive?",
	   "Multiply equation’s two sides by -1",
	   "The next step of the equation (the whole line) is [STEP]"];


hints_enGB["-x=-c"] = hints_enGB["-x=c"];

hints_enGB["x=b+c"] = ["There are two numbers that can be summed, try to find out which are they",
		   "The numbers [b] and [c] must be summed",
		   "What is sum's [b] + [c] result?",
		   "The sum [b] + [c] result is [RESULT]",
		   "The next equation’s step (the whole line) is [STEP]"];
hints_enGB["-x=b+c"] = hints_enGB["x=b+c"];
hints_enGB["ax=b+c"] = hints_enGB["x=b+c"];
hints_enGB["-ax=b+c"] = hints_enGB["x=b+c"];

hints_enGB["x=b-c"] = ["There are two number that can be subtracted, try to find out which are they",
		   "The numbers [b] and [c] must be subtracted",
		   "What is difference's [b] - [c] result?",
		   "The difference [b] - [c] result is [RESULT]",
		   "The next equation’s step (the whole line) is [STEP]"];
hints_enGB["-x=b-c"] = hints_enGB["x=b-c"];
hints_enGB["ax=b-c"] = hints_enGB["x=b-c"];
hints_enGB["-ax=b-c"] = hints_enGB["x=b-c"];

hints_enGB["x=b*c"] = ["There are two number that can be multiplied, try to find out which are they",
		   "The numbers [b] and [c] must be multiplied",
		   "What is result of product [b] * [c]?",
		   "The product [b] * [c] result is [RESULT]",
		   "The next equation’s step (the whole line) is [STEP]"];
hints_enGB["-x=b*c"] = hints_enGB["x=b*c"];
hints_enGB["ax=b*c"] = hints_enGB["x=b*c"];
hints_enGB["-ax=b*c"] = hints_enGB["x=b*c"];

hints_enGB["x=b/c"] = [new Array(), new Array()];
//Se a divisao tem como resultado um numero inteiro
hints_enGB["x=b/c"][0] = ["There are two number that can be divided, try to find out which are they",
			  "The numbers [b] and [c] must be divided",
			  "What is result of division [b] / [c]?",
			  "The division [b] / [c] result is [RESULT]",
			  "The next equation’s step (the whole line) is [STEP]"];
//Se a divisao tem como resultado uma fracao
hints_enGB["x=b/c"][1] = ["The fraction [b] / [c] must be simplified",
			  "To simplify a fraction, you have to divide numerator and denominator by the same number.",
			  "Remind the divisibility criteria to choose the next number that can divide the numerator and denominator at the same time. <a href='https://brasilescola.uol.com/matematica/criterios-divisibilidade.htm'>Click here</a> to confer those criteria.",
			  "Divide the numerator and denominator by the same number [NUMBER]",
			  "The next equation’s step (the whole line) is [STEP]"];
hints_enGB["-x=b/c"] = hints_enGB["x=b/c"];
hints_enGB["ax=b/c"] = hints_enGB["x=b/c"];
hints_enGB["-ax=b/c"] = hints_enGB["x=b/c"];

hints_enGB["x=-b+c"] = ["There are two numbers that can be summed, try to find out which are they",
			"The numbers [b] and [c] must be summed",
			"Did you know the sum [b] + [c] can be calculated in the [c] - [b] format?",
			"The sum [b] + [c] result is [RESULT]",
			"The next equation’s step (the whole line) is [STEP]"];
hints_enGB["-x=-b+c"] = hints_enGB["x=-b+c"];
hints_enGB["ax=-b+c"] = hints_enGB["x=-b+c"];
hints_enGB["-ax=-b+c"] = hints_enGB["x=-b+c"];

hints_enGB["x=-b-c"] = ["There are two numbers that can be subtracted, try to find out which are they",
			"The numbers [b] and [c] must be subtracted",
			"You can treat the difference [b] - [c] as a sum of debts",
			"The difference [b] - [c] result is [RESULT]",
			"The next equation’s step (the whole line) is [STEP]"];				    
hints_enGB["-x=-b-c"] = hints_enGB["x=-b-c"];
hints_enGB["ax=-b-c"] = hints_enGB["x=-b-c"];
hints_enGB["-ax=-b-c"] = hints_enGB["x=-b-c"];

hints_enGB["x=-b*c"] = ["There are two numbers that can be multiplied, try to find out which are they",
		    "The numbers [b] and [c] must be multiplied",
		 	"What is result of product [b] * [c]? Remember that the multiplication of a positive number with a negative one results in a negative number",
		 	"The product [b] * [c] result is [RESULT]",
		 	"The next equation’s step (the whole line) is [STEP]"];
hints_enGB["x=b*-c"] = hints_enGB["x=-b*c"];
hints_enGB["-x=-b*c"] = hints_enGB["x=-b*c"]
hints_enGB["-x=b*-c"] = hints_enGB["x=-b*c"];
hints_enGB["ax=-b*c"] = hints_enGB["x=-b*c"];
hints_enGB["ax=b*-c"] = hints_enGB["x=-b*c"];
hints_enGB["-ax=-b*c"] = hints_enGB["x=-b*c"]
hints_enGB["-ax=b*-c"] = hints_enGB["x=-b*c"];

hints_enGB["x=-b*-c"] = ["There are two numbers that can be multiplied, try to find out which are they",
			 "The numbers [b] and [c] must be multiplied",
			 "What is result of product [b] * [c]? Remember that the multiplication of two negative numbers results in a positive one",
			 "The product [b] * [c] result is [RESULT]",
			 "The next equation’s step (the whole line) is [STEP]"];
hints_enGB["-x=-b*-c"] = hints_enGB["x=-b*c"];
hints_enGB["ax=-b*-c"] = hints_enGB["x=-b*c"];
hints_enGB["-ax=-b*-c"] = hints_enGB["x=-b*c"];

hints_enGB["x=-b/c"] = [new Array(), new Array()];
//Se a divisao tem como resultado um numero inteiro
hints_enGB["x=-b/c"][0] = ["There are two numbers that can be divided, try to find out which are they",
			   "The numbers [b] and [c] must be divided",
			   "What is [b] / [c] division's result? Remember that the division of a positive number by a negative one(and vice versa) results in a negative number",
			   "The division [b] / [c] result is [RESULT]",
			   "The next equation’s step (the whole line) is [STEP]"];
//Se a divisao tem como resultado uma fracao
hints_enGB["x=-b/c"][1] = hints_enGB["x=b/c"][1];

hints_enGB["x=b/-c"] = hints_enGB["x=-b/c"];
hints_enGB["-x=-b/c"] = hints_enGB["x=-b/c"];
hints_enGB["-x=b/-c"] = hints_enGB["x=-b/c"];
hints_enGB["ax=-b/c"] = hints_enGB["x=-b/c"];
hints_enGB["ax=b/-c"] = hints_enGB["x=-b/c"];
hints_enGB["-ax=-b/c"] = hints_enGB["x=-b/c"];
hints_enGB["-ax=b/-c"] = hints_enGB["x=-b/c"];

hints_enGB["x=-b/-c"] = [new Array(), new Array()];
//Se a divisao tem como resultado um numero inteiro
hints_enGB["x=-b/-c"][0] = ["There are two numbers that can be divided, try to find out which are they",
			   "The numbers [b] and [c] must be divided",
			   "What is [b] / [c] division's result? Remember that the division of two negative numbers results in a positive one",
			   "The division [b] / [c] result is [RESULT]",
			   "The next equation’s step (the whole line) is [STEP]"];
//Se a divisao tem como resultado uma fracao
hints_enGB["x=-b/-c"][1] = hints_enGB["x=b/c"][1];

hints_enGB["-x=-b/-c"] = hints_enGB["x=-b/-c"];
hints_enGB["ax=-b/-c"] = hints_enGB["x=-b/-c"];
hints_enGB["-ax=-b/-c"] = hints_enGB["x=-b/-c"];

hints_enGB["c=x"] = ["Have you realized this equation's terms are in sides changed?",
		 "Remember that goal is always to leave the terms with X on the left side and the numbers on the right side of the equation",
		 "What do we need to do to pass term [x] to the left side and [c] to the right side?",
		 "Swap sides of equation's terms, passing [x] to the left side and [c] to the right side. It isn't necessary to swhich its operations, since they're the only two equation's elements",
		 "The next equation’s step (the whole line) is [STEP]"];
hints_enGB["c=-x"] = hints_enGB["c=x"];
hints_enGB["c=ax"] = hints_enGB["c=x"];
hints_enGB["c=-ax"] = hints_enGB["c=x"];
hints_enGB["-c=x"] = hints_enGB["c=x"];
hints_enGB["-c=-x"] = hints_enGB["c=x"];
hints_enGB["-c=ax"] = hints_enGB["c=x"];
hints_enGB["-c=-ax"] = hints_enGB["c=x"];

hints_enGB["x+b=c"] = ["There are two terms that can be operated, however they are in different equation sides. What do you have to do in that case?",
		   "Terms [b] and [c] should be operated, but they are in different equation sides. To solve this problem, you must perform an operation at both equation's sides so that [b] becomes zero. Another way of thinking is term [b] should be passed to equation's other side. Besides, remind that goal to let the numbers in the equation's right side",
		   "The operation that should be performed at both equation's sides is the inverse of [b] term. In other words, you have to pass [b] to equation's other side, reversing its operation. Considering [b] is summing X, what's the addition's inverse operation?",
		   "Addition's inverse operation is subtraction. Thus, subtract equation's two sides by number [b]",
		   "The next equation’s step (the whole line) is [STEP]"];
hints_enGB["x+b=-c"] = hints_enGB["x+b=c"];
hints_enGB["-x+b=c"] = hints_enGB["x+b=c"];
hints_enGB["-x+b=-c"] = hints_enGB["x+b=c"];
hints_enGB["ax+b=c"] = hints_enGB["x+b=c"];
hints_enGB["ax+b=-c"] = hints_enGB["x+b=c"];
hints_enGB["-ax+b=c"] = hints_enGB["x+b=c"];
hints_enGB["-ax+b=-c"] = hints_enGB["x+b=c"];

hints_enGB["x-b=c"] = ["There are two terms that can be operated, however they are in different equation sides. What do you have to do in that case?",
		   "Terms [b] and [c] should be operated, but they are in but they are in different equation sides. To solve this problem, you must perform an operation at both equation's sides so that [b] becomes zero. Another way of thinking is term [b] should be passed to equation's other side. Besides, remind that goal to let the numbers in the equation's right side",
		   "The operation that should be performed at both equation's sides is the inverse of [b] term. In other words, you have to pass [b] to equation's other side, reversing its operation. Considering [b] is subtracting X, what's the subtraction's inverse operation?",
		   "Subtraction's inverse operation is addition. Thus, sum equation's two sides by number [b]",
		   "The next equation’s step (the whole line) is [STEP]"];
hints_enGB["x-b=-c"] = hints_enGB["x-b=c"];
hints_enGB["-x-b=c"] = hints_enGB["x-b=c"];
hints_enGB["-x-b=-c"] = hints_enGB["x-b=c"];
hints_enGB["ax-b=c"] = hints_enGB["x-b=c"];
hints_enGB["ax-b=-c"] = hints_enGB["x-b=c"];
hints_enGB["-ax-b=c"] = hints_enGB["x-b=c"];
hints_enGB["-ax-b=-c"] = hints_enGB["x-b=c"];

hints_enGB["ax=c"] = ["In that equation, X has a coefficient. What do we have to do to remove this coefficient so that X be isolated in equation's left side?",
		  "To remove X's coefficient, you must performe an operation at both equation's sides so this coefficient becomes 1. Another way of thinking is that this term that's multiplying X should be passed to equation's other side, so X be isolated",
		  "The operation that should be performed at both equation's sides is the coefficient's inverse operation. In other words you must pass X's coefficient to equation's other side, reversing its operation. Considering the coefficient is multiplying X, what's the multiply's inverse operation?",
		  "Multiplication's inverse operation is division. Thus, divide equation's two sides by the coefficient value [a]",
		  "The next equation’s step (the whole line) is [STEP]"];
hints_enGB["ax=-c"] = hints_enGB["ax=c"];

hints_enGB["-ax=c"] = ["In that equation, X has a coefficient. What do we have to do to remove this coefficient so that X be isolated in equation's left side?",
		   "To remove X's coefficient, you must performe an operation at both equation's sides so this coefficient becomes 1. Another way of thinking is that this term that's multiplying X should be passed to equation's other side, so X be isolated",
		   "The operation that should be performed at both equation's sides is the coefficient's inverse operation. In other words you must pass X's coefficient to equation's other side, reversing its operation. Considering the coefficient is multiplying X, what's the multiply's inverse operation?",
		   "Multiplication's inverse operation is division. Thus, divide equation's two sides by the coefficient value [a](remember that this coefficient is a negative number and at division you must preserve the minus sign).",
		   "The next equation’s step (the whole line) is [STEP]"];

hints_enGB["-ax=-c"] = hints_enGB["-ax=c"];

hints_enGB["x/b=c"] = ["In that equation, X its in a fraction. What do we have to do to remove its denominator so that X be isolated in equation's left side?",
		   "To remove the denominator, you must performe an operation at both equation's sides so this coefficient becomes 1. Another way of thinking is that this term that's dividing X should be passed to equation's other side, so X be isolated",
		   "The operation that should be performed at both equation's sides is the denominator's inverse operation. In other words you must pass the denominator to equation's other side, reversing its operation. Considering the denominator is dividing X, what's the division's inverse operation?",
		   "Division's inverse operation is multiplication. Thus, multiply equation's two sides by the denominator value [b]",
		   "The next equation’s step (the whole line) is [STEP]"];
hints_enGB["x/b=-c"] = hints_enGB["x/b=c"];

hints_enGB["-x/b=c"] = ["In that equation, X its in a fraction. What do we have to do to remove its denominator so that X be isolated in equation's left side?",
			"To remove the denominator, you must performe an operation at both equation's sides so this coefficient becomes 1. Another way of thinking is that this term that's dividing X should be passed to equation's other side, so X be isolated",
			"The operation that should be performed at both equation's sides is the denominator's inverse operation. In other words you must pass the denominator to equation's other side, reversing its operation. Considering the denominator is dividing X, what's the division's inverse operation?",
			"Division's inverse operation is multiplication. Thus, multiply equation's two sides by the denominator value [b](so that X don't keep negative, you can multiply both sides by -[b])",
			"The next equation’s step (the whole line) is [STEP]"];
hints_enGB["-x/b=-c"] = hints_enGB["x/b=c"];

hints_enGB["ax/b=c"] = [new Array(), new Array(), new Array()];
//Math.abs(a) = Math.abs(b)
hints_enGB["ax/b=c"][0] = ["There are two numbers that can be divided, try to find out which are they",
		   	   "Although [a] is multiplying X, you still can divide [a] by [b]",
		   	   "Notice the number that's multiplying X is identic to the fraction's denominator. What is division's result of a number by itself?(in that case it would be [a] / [b], but remember that result is always the same in identical numbers's division)",
		   	   "The division [a] / [b] result is 1. Remember when X's coefficient is 1, isn't necessary to explicit it in the equation; thus you can write just X",
		   	   "The next equation’s step (the whole line) is [STEP]"];
//ax/b pode ser simplificada
hints_enGB["ax/b=c"][1] = ["The fraction [a] / [b] must be simplified (even when [a] is multiplying X, the simplification rules are the same)",
			   "To simplify a fraction you must divide numerator and denominator by the same number",
			   "Remember the divisibility criteria to choose the next number that can divide the numerator and denominator at the same time. <a href='https://brasilescola.uol.com/matematica/criterios-divisibilidade.htm'>Click</a> to confer those criteria.",
			   "Divide the numerator and denominator by the number [NUMBER]",
			   "The next equation’s step (the whole line) is [STEP]"];
//ax/b nao pode ser simplificada
hints_enGB["ax/b=c"][2] = ["In that equation, X and its coefficient are in a fraction and simplification isn't possible. What do we have to do to remove the fraction's denominator so X and its coefficient be isolated in equation's left side?",
			   "To remove the denominator, you must perform an operation at both equation's sides so the denominator becomes 1. Another way of thinking is that the denominator should be passed to equation's other side, so X and its coefficient be isolated",
			   "The operation that should be performed at both equation's sides is the denominator's inverse operation. In other words you must pass the denominator to equation's other side, reversing its operation. Considering  the denominator is dividing X and its coefficient, what's the division's inverse operation?",
			   "Division's inverse operation is multiplication. Thus, multiply equation's two sides by denominator value [b]",
			   "The next equation’s step (the whole line) is [STEP]"];

hints_enGB["-ax/b=c"] = [new Array(), new Array(), new Array()];
hints_enGB["-ax/b=c"][0] = ["There are two numbers that can be divided, try to find out which are they",
		   		"Although [a] is multiplying X, you still can divide [a] by [b]",
		   		"Notice the number that's multiplying X owns the same absolute value than fraction's denominator(in other words, numbers are identical except for the sign). What is the division's result of a positive number by a negative with same absolute value?(in that case it would be [a] / [b], but remember that result is always the same in identical numbers's division)",
		   		"The division [a] / [b] result is -1. Remember when X's coefficient is -1, isn't necessary to explicit it in the equation; thus you can write just -X",
		   		"The next equation’s step (the whole line) is [STEP]"];
hints_enGB["-ax/b=c"][1] = hints_enGB["ax/b=c"][1];
hints_enGB["-ax/b=c"][2] = hints_enGB["ax/b=c"][2];

hints_enGB["many terms with multiplication positive"] = ["There are terms that can be multiplied, try to find out which are they",
											 "The terms [TERMS] must be multiplied",
											 "What is product's [TERMS MULTIPLICATION] result?",
											 "The product [TERMS MULTIPLICATION] result is [RESULT]",
											 "The next equation’s step (the whole line) is [STEP]"];
hints_enGB["many terms with multiplication negative"] = ["There are terms that can be multiplied, try to find out which are they",
											 "The terms [TERMS] must be multiplied",
											 "What is product [TERMS MULTIPLICATION] result? Remember the multiplication of two negative numbers results in a positive number",
											 "The product [TERMS MULTIPLICATION] result is [RESULT]",
											 "The next equation’s step (the whole line) is [STEP]"];
hints_enGB["many terms with multiplication positive"] = ["There are terms that can be multiplied, try to find out which are they",
											 "The terms [TERMS] must be multiplied",
											 "What is product [TERMS MULTIPLICATION] result? Remember the multiplication of a positive number with a negative numbers results in a negative number",
											 "The product [TERMS MULTIPLICATION] result is [RESULT]",
											 "The next equation’s step (the whole line) is [STEP]"];



hints_enGB["numbers on different sides (left is positive)"] = ["There are two terms that can be operated, however they are in different equation sides. What do you have to do in that case?",
											 	   "Terms [a] and [b] must be operated, but they are in different equation sides. To solve this problem, you must perform an operation at both equation's sides so that [a] becomes zero. Another way of thinking is term [a] should be passed to equation's other side. Besides, remind that goal to let the numbers in the equation's right side",
											 	   "The operation that must be performed at both equation's sides is the inverse of [a] term. In other words, you have to pass [a] to equation's other side, reversing its operation. Considering [a] is summing X, what's the addition's inverse operation?",
											 	   "Addition's inverse operation is subtraction. Thus, subtract equation's two sides by number [a]",
											 	   "The next equation’s step (the whole line) is [STEP]"];

hints_enGB["numbers on different sides (left is negative)"] = ["There are two terms that can be operated, however they are in different equation sides. What do you have to do in that case?",
												   "Terms [a] and [b] must be operated, but they are in different equation sides. To solve this problem, you must perform an operation at both equation's sides so that [a] becomes zero. Another way of thinking is term [a] should be passed to equation's other side. Besides, remind that goal to let the numbers in the equation's right side",
												   "The operation that must be performed at both equation's sides is the inverse of [a] term. In other words, you have to pass [a] to equation's other side, reversing its operation. Considering [a] is subtracting X, what's the subtraction's inverse operation?",
												   "Subtraction's inverse operation is addition. Thus, sum equation's two sides by number [a]",
												   "The next equation’s step (the whole line) is [STEP]"];

hints_enGB["sum of terms with x on the same side"] = ["There are two similar terms that can be summed, try to find out which are they",
										  "Terms [a] and [b] must be summed",
										  "To perform a two similar terms's summing, simply sum the numbers and maintain x. For example, the sum 2x + 5x result is 7x",
										  "The sum [a] + [b] result is [RESULT]",
										  "The next equation’s step (the whole line) is [STEP]"];

hints_enGB["subtraction of terms with x on the same side"] = ["There are two similar terms that can be subtracted, try to find out which are they",
												  "Terms [a] and [b] must be subtracted",
												  "To perform a two similar terms's subtraction, simply subtract the numbers and maintain x. For example, the difference 7x - 5x result is 2x",
												  "The subtraction [a] - [b] result is [RESULT]",
												  "The next equation’s step (the whole line) is [STEP]"];

hints_enGB["terms with X on different sides (right is positive)"] = ["There are two terms that can be operated, however they are in different equation sides. What do you have to do in that case?",
														 "Terms [a] and [b] must be operated, but they are in different equation sides. To solve this problem, you must perform an operation at both equation's sides so that [a] becomes zero. Another way of thinking is term [b] should be passed to equation's other side. Besides, remind that goal to let the terms with X on equation's left side",
														 "The operation that must be performed at both equation's sides is the inverse of [b] term. In other words, you have to pass [b] to equation's other side, reversing its operation. Considering [b] has plus sign, what's the sum's inverse operation?",
														 "Addition's inverse operation is subtraction. Thus, subtract equation's two sides by term [b]",
														 "The next equation's step (the whole line) is [STEP]"];

hints_enGB["terms with X on different sides (right is negative)"] = ["There are two terms that can be operated, however they are in different equation sides. What do you have to do in that case?",
														 "Terms [a] and [b] must be operated, but they are in different equation sides. To solve this problem, you must perform an operation at both equation's sides so that [a] becomes zero. Another way of thinking is term [b] should be passed to equation's other side. Besides, remind that goal to let the terms with X on equation's left side",
														 "The operation that must be performed at both equation's sides is the inverse of [b] term. In other words, you have to pass [b] to equation's other side, reversing its operation. Considering [b] has minus sign, what's the subtraction's inverse operation?",
														 "subtraction's inverse operation is addition. Thus, sum equation's two sides by term [b]",
														 "The next equation's step (the whole line) is [STEP]"];

hints_enGB["parentheses with plus sign"] = ["That equation has terms in parentheses, try to find out some way to remove those parentheses",
								"Notice there's a plus sign in front of parentheses [PARENTHESES]. Do you remember the set of signs?",
								"When there's a plus sign in front of parentheses, you can remove those parentheses without alterating signs of the terms inside them",
								"For the next step, you just have to write the equation removing parentheses and the sign in front of them in the expression +[PARENTHESES]",
								"The next equation's step (the whole line) is [STEP]"];

hints_enGB["parentheses with minus sign"] = ["That equation has terms in parentheses, try to find out some way to remove those parentheses",
								 "Notice there's a minus sign in front of parentheses [PARENTHESES]. Do you remember the set of signs?",
								 "When there's a minus sign in front of parentheses, you must invert signs of the terms inside them (plus signs turn to minus signs, and minus signs turn to plus signs)",
								 "For the next step, you just have to write the equation removing parentheses of expression [PARENTHESES] and inverting the signs of its terms. They will end up this way: [RESULT]",
								 "The next equation's step (the whole line) is [STEP]"];

hints_enGB["distributive property"] = ["That equation has distributive property, that is, one number multiplying terms in parentheses. What do you have to do in that case?",
						   "To solve a distributive property, you have to multiply the number outside parentheses by the first term, then by the second term. Attention with the set of signs",
						   "Observe an exemple of a distributive property resolution: 4(2-5x) = 4*2-4*5x = 8-20x",
						   "The distributive property [DISTRIBUTIVE PROPERTY] result is [RESULT]",
						   "The next equation's step (the whole line) is [STEP]"];

hints_enGB["reason and proportion"] = ["That equation is composed by a ratio and proportion, that is, an equality of fractions. To solve it, you can use the same rules of Rule of Three",
						   "There are three ways to solve a ratio and proportion. The first one is using Least Common Multiple (LCM), but there are simplests ways. The second one is the following: <br>1. Multiply the denominator from left side by the numerator from right side<br>2. Multiply the denominator from right side by the numerator from left side",
						   "The other way of solving a ratio and proportion is based on the inverse operation principle: which terms would be interesting to pass to equation's opposite side?",
						   "For the next step, perform the folliwing procedures: <br>1. Pass the equation's left side denominator ([LEFT DENOMINATOR]) to the right side multiplying all its terms<br>2. Next, pass the right side denominator ([RIGHT DENOMINATOR]) to the left side, multiplying all its terms.<br><a href=# onclick=reasonAndProportionHelp()>Click here to confer the formula that represents these instructions</a>",
						   "The next equation's step (the whole line) is [STEP]"];

hints_enGB["fractions with simple numerator"] = ["That equation is composed by fractions with different denominators. What do you have to do in that case?",
									 "Your goal will be to remove the fractions from equation. The first step is to calculate denominators's Least Common Multiple (LCM). Next, try to find out which are the fractions's terms in you should apply division and multiplication operations",
									 "After calculating LCM, for every equation's term you should divide the obtained value by its denominator (if the term doesn't have one, consider it as 1), and multiply by its numerator. Notice that after this procedure, you'll have removed all denominators",
									 "The LCM value of all denominators is [LCM]. Check below how calculation is done:",
									 "The next equation's step (the whole line) is [STEP]"];

hints_enGB["fractions with compound numerator"] = ["That equation is composed by fractions with different denominators. What do you have to do in that case?",
									   "Your goal will be to remove the fractions from equation. The first step is to calculate denominators's Least Common Multiple (LCM). Next, try to find out which are the fractions's terms in you should apply division and multiplication operations",
									   "After calculating LCM, for every equation's term you should divide the obtained value by its denominator (if the term doesn't has one, consider it as 1), and multiply by its numerator (attention with fractions that have an expression in numerator's place. The multiplication must be performed in each element of this expression, composing then a distributive property). Notice that after this procedure, you'll have removed all denominators",
									   "The LCM value of all denominators is [LCM]. Check below how calculation is done:",
									   "The next equation's step (the whole line) is [STEP]"];

hints_enGB["fractions with distributive property"] = ["That equation is composed by fractions with different denominators. What do you have to do in that case?",
										  "Your goal will be to remove the fractions from equation. If you want to, you can calculate distributive property before advancing step to calculating denominators's Least Common Multiple(LCM). Next, try to find out which are the fractions's terms in you should apply division and multiplication operations",
										  "After calculating LCM, for every equation's term you should divide the obtained value by its denominator (if the term doesn't has one, consider it as 1), and multiply by its numerator (attention with fractions that have an expression or distributive property in numerator's place). Notice that after this procedure, you'll have removed all denominators",
										  "The LCM value of all denominators is [LCM]. Check below how calculation is done:",
										  "The next equation's step (the whole line) is [STEP]"];

hints_enGB["fractions with multiplication"] = [new Array(), new Array()];
//O numero que esta multiplicando eh igual ao denominador da fracao
hints_enGB["fractions with multiplication"][0] = ["There are two numbers that can be multiplied, try to find out which are they",
									  "Terms [b] and [c] must be multiplied. Remember you can apply cancellation rule in that case",
									  'Cancellation rule consists in "cutting off" two equal terms in an equation, respecting the following rules: <br>1. One of the terms must be in numerator (or multiplying the fraction) and the other in denominator<br>2. Numerator should be composed by a number or a multiplication to apply the rule, unless a extern number is already multiplying the fraction (in that case you can cut this number with denominator). <br><a href=# onclick=cancellationRuleHelp()>Click here to confer some examples</a>',
									  "Applying cancellation rule, we remove the fraction's denominator and the number that's multiplying it, resulting the own fraction numerator: [RESULT]",
									  "The next equation's step (the whole line) is [STEP]"];

//O numero que esta multiplicando eh diferente do denominador da fracao
hints_enGB["fractions with multiplication"][1] = ["There are two numbers that can be multiplied, try to find out which are they",
									  "Terms [b] and [c] must be multiplied. Do you remember how to multiply a fraction by an integer?",
									  "You just have to multiply the fraction numerator by the number and place the result in numerator (denominator remains the same)",
									  "The product [b] * [c] result is [RESULT]",
									  "The next equation's step (the whole line) is [STEP]"];