//TODO: Fazer o registro no banco de dados conforme descrevi no OneNote
function hint() {
	if (levelHint < 6) {
		if (isLoadEquation)
			isLoadEquation = false;
		
	    if (!selectedEquation.isComplete) {
	    	var currentStep;
	
	    	if (selectedEquation.lastStep !== null) {
	    		currentStep = selectedEquation.lastStep.step;
	    	}
	    	
	    	else {
	    		currentStep = selectedEquation.equation;
	    	}
	    	
	    	var formatEquation = checkFormat(currentStep);
	    	var text = selectHint(formatEquation, currentStep);
	        showHint(text);      
	        levelHint++;
	        
	    } else {
	    	$("#hintText").html(indexTXT[60]);
	        $("#hintText").show('blind', 500);
	        $(".verticalTape").show('fold', 500);
	    }
	}
}

function selectHint(formatEquation, currentStep) {
	var textHint;
	var typeHint = typesHint[formatEquation];
	
	if (typeHint.length === 1) {
		textHint = window[typeHint[0]](currentStep);
	}
	
	else if (typeHint.length === 2) {
		textHint = window[typeHint[0]](currentStep, typeHint[1]);
	}
	
	else if (typeHint.length === 4) {
		textHint = window[typeHint[0]](currentStep, typeHint[1], typeHint[2], typeHint[3], formatEquation);
	}
	
	else if (typeHint.length === 3) {
		textHint = window[typeHint[0]](currentStep, typeHint[1], typeHint[2], formatEquation);
	}
	
	else {
		textHint = window[typeHint[0]](currentStep, typeHint[1], typeHint[2], typeHint[3], typeHint[4]);
	}
	
	registerHint(currentStep);
	
	return textHint;
}
// Dicionario que salva os nomes das funcoes e os parametros necessarios
var typesHint = {};

typesHint["-x=c"] = ["type1"];
typesHint["-x=-c"] = ["type1"];

typesHint["x=b+c"] = ["type2", "+", "", ""];
typesHint["-x=b+c"] = ["type2", "+", "", ""];
typesHint["ax=b+c"] = ["type2", "+", "", ""];
typesHint["-ax=b+c"] = ["type2", "+", "", ""];

typesHint["x=b-c"] = ["type2", "-", "", ""];
typesHint["-x=b-c"] = ["type2", "-", "", ""];
typesHint["ax=b-c"] = ["type2", "-", "", ""];
typesHint["-ax=b-c"] = ["type2", "-", "", ""];

typesHint["x=b*c"] = ["type2", "*", "", ""];
typesHint["-x=b*c"] = ["type2", "*", "", ""];
typesHint["ax=b*c"] = ["type2", "*", "", ""];
typesHint["-ax=b*c"] = ["type2", "*", "", ""];

typesHint["x=b/c"] = ["type2Fraction", "", ""];
typesHint["-x=b/c"] = ["type2Fraction", "", ""];
typesHint["ax=b/c"] = ["type2Fraction", "", ""];
typesHint["-ax=b/c"] = ["type2Fraction", "", ""];

typesHint["x=-b+c"] = ["type2", "+", "-", ""];
typesHint["-x=-b+c"] = ["type2", "+", "-", ""];
typesHint["ax=-b+c"] = ["type2", "+", "-", ""];
typesHint["-ax=-b+c"] = ["type2", "+", "-", ""];

typesHint["x=-b-c"] = ["type2", "-", "-", ""];
typesHint["-x=-b-c"] = ["type2", "-", "-", ""];
typesHint["ax=-b-c"] = ["type2", "-", "-", ""];
typesHint["-ax=-b-c"] = ["type2", "-", "-", ""];

typesHint["x=-b*c"] = ["type2", "*", "-", ""];
typesHint["x=b*-c"] = ["type2", "*", "", "-"];
typesHint["-x=-b*c"] = ["type2", "*", "-", ""];
typesHint["-x=b*-c"] = ["type2", "*", "", "-"];
typesHint["ax=-b*c"] = ["type2", "*", "-", ""];
typesHint["ax=b*-c"] = ["type2", "*", "", "-"];
typesHint["-ax=-b*c"] = ["type2", "*", "-", ""];
typesHint["-ax=b*-c"] = ["type2", "*", "", "-"];	

typesHint["x=-b*-c"] = ["type2", "*", "-", "-"];
typesHint["-x=-b*-c"] = ["type2", "*", "-", "-"];
typesHint["ax=-b*-c"] = ["type2", "*", "-", "-"];
typesHint["-ax=-b*-c"] = ["type2", "*", "-", "-"];

typesHint["x=-b/c"] = ["type2Fraction", "-", ""];
typesHint["x=b/-c"] = ["type2Fraction", "", "-"];
typesHint["-x=-b/c"] = ["type2Fraction", "-", ""];
typesHint["-x=b/-c"] = ["type2Fraction", "", "-"];
typesHint["ax=-b/c"] = ["type2Fraction", "-", ""];
typesHint["ax=b/-c"] = ["type2Fraction", "", "-"];
typesHint["-ax=-b/c"] = ["type2Fraction", "-", ""];
typesHint["-ax=b/-c"] = ["type2Fraction", "", "-"];	

typesHint["x=-b/-c"] = ["type2Fraction", "-", "-"];
typesHint["-x=-b/-c"] = ["type2Fraction", "-", "-"];
typesHint["ax=-b/-c"] = ["type2Fraction", "-", "-"];
typesHint["-ax=-b/-c"] = ["type2Fraction", "-", "-"];

typesHint["x=numerical expression"] = ["type2NumericalExpression"];
typesHint["-x=numerical expression"] = ["type2NumericalExpression"];
typesHint["ax=numerical expression"] = ["type2NumericalExpression"];
typesHint["-ax=numerical expression"] = ["type2NumericalExpression"];

typesHint["x+b=c"] = ["type3", "+", ""];
typesHint["x+b=-c"] = ["type3", "+", "-"];
typesHint["-x+b=c"] = ["type3", "+", ""];
typesHint["-x+b=-c"] = ["type3", "+", "-"];
typesHint["ax+b=c"] = ["type3", "+", ""];
typesHint["ax+b=-c"] = ["type3", "+", "-"];
typesHint["-ax+b=c"] = ["type3", "+", ""];
typesHint[-"ax+b=-c"] = ["type3", "+", "-"];

typesHint["x-b=c"] = ["type3", "-", ""];
typesHint["x-b=-c"] = ["type3", "-", "-"];
typesHint["-x-b=c"] = ["type3", "-", ""];
typesHint["-x-b=-c"] = ["type3", "-", "-"];
typesHint["ax-b=c"] = ["type3", "-", ""];
typesHint["ax-b=-c"] = ["type3", "-", "-"];
typesHint["-ax-b=c"] = ["type3", "-", ""];
typesHint[-"ax-b=-c"] = ["type3", "-", "-"];

typesHint["ax=c"] = ["type4", "", ""];
typesHint["ax=-c"] = ["type4", "", "-"];
typesHint["-ax=c"] = ["type4", "-", ""];
typesHint["-ax=-c"] = ["type4", "-", "-"];

typesHint["x/b=c"] = ["type5", "", ""];
typesHint["x/b=-c"] = ["type5", "", "-"];
typesHint["-x/b=c"] = ["type5", "-", ""];
typesHint["-x/b=-c"] = ["type5", "-", "-"];

typesHint["ax/b=c"] = ["type5WithCoefficient", "", ""];
typesHint["ax/b=-c"] = ["type5WithCoefficient", "", "-"];
typesHint["-ax/b=c"] = ["type5WithCoefficient", "-", ""];
typesHint["-ax/b=-c"] = ["type5WithCoefficient", "-", "-"];

typesHint["many terms"] = ["type6"];

typesHint["parentheses with plus sign"] = ["type7PlusSign"];
typesHint["parentheses with minus sign"] = ["type7MinusSign"];

typesHint["distributive property"] = ["type8"];

typesHint["reason and proportion"] = ["type9"];

typesHint["fractions with simple numerator"] = ["type10", "fractions with simple numerator"];
typesHint["fractions with compound numerator"] = ["type10", "fractions with compound numerator"];
typesHint["fractions with distributive property"] = ["type10", "fractions with distributive property"];

typesHint["c=x"] = ["type11"];
typesHint["c=-x"] = ["type11"];
typesHint["c=ax"] = ["type11"];
typesHint["c=-ax"] = ["type11"];
typesHint["-c=x"] = ["type11"];
typesHint["-c=-x"] = ["type11"];
typesHint["-c=ax"] = ["type11"];
typesHint["-c=-ax"] = ["type11"];

//Para equacoes do tipo -x=c
function type1(currentStep) {
	var text = hintsTXT["-x=c"][levelHint-1];
	
	if (levelHint === 5) {
		var split = currentStep.split("=");
		var step = split[0] + "*(-1)=" + split[1] + "*(-1)";
			
		text = text.replace("[STEP]", step);
	}
	
	return text;	
}

//Para equacoes do tipo +-ax=b[operador]c com resultado inteiro
function type2(currentStep, operator, signB, signC, formatEquation) {
//	var formatEquation = "x=" + signB + "b" + operator + signC + "c";
	var text;
	
	if (operator !== "/") {
		text = hintsTXT[formatEquation][levelHint-1];
	}
	
	else {
		text = hintsTXT[formatEquation][0][levelHint-1];
	}
	
	if (levelHint > 1) {
		var split = currentStep.split("=");
		
		if (levelHint <= 4) {
			var numbers = split[1].split(operator);
			var i;
			
			if (numbers.length === 2) {
				i = 0;
			}
			
			else {
				i = 1;
				numbers[1] = "-" + numbers[1];
			}
			
			var b = numbers[i];
			var c = numbers[i+1];
			
			if (signC === "-" && operator === "*" && levelHint > 2 && c[0] !== "(") {
				c = "(" + c + ")";
			}
			
			text = replaceAll(text, "[b]", b);
			text = replaceAll(text, "[c]", c);
			
			if (levelHint === 3) {
				text = text.replace("- -", "- ");
			}
			
			if (levelHint === 4) {
				var result = "" + eval(split[1]);
				text = text.replace("[RESULT]", result);
			}
		}
		
		else {
			var step = split[0] + "=" + eval(split[1]);
			text = text.replace("[STEP]", step);
		}
	}
	
	return text;	
}

//Para equacoes do tipo +-ax=+-b/+-c
function type2Fraction(currentStep, signB, signC, formatEquation) {
	currentStep = replaceAll(currentStep, "(", "");
	currentStep = replaceAll(currentStep, ")", "");
	
	var split = currentStep.split("=");
	var result = eval(split[1]);
	
	//Resultado inteiro
	if (result % 1 === 0) {
		return type2(currentStep, "/", "", "", formatEquation);
	}
	
	//Resultado fracionario
	else {
		var text = hintsTXT[formatEquation][1][levelHint-1];
		
		if (levelHint === 1 || levelHint >= 4) {
			if (levelHint === 1) {
				var numbers = split[1].split("/");		
				var b = numbers[0];
				var c = numbers[1];
				
				text = text.replace("[b]", b);
				
				if (levelHint > 1 || signC !== "-") {
					text = text.replace("[c]", c);
				}
				
				else {
					text = text.replace("[c]", "(" + c + ")");
				}
			}
			
			else {
				var stepSimplification = simplifyOneStep(split[1]);
				
				if (levelHint === 4) {				
					var number = "" + stepSimplification.divisor;				
					text = text.replace("[NUMBER]", number);
				}
				
				else {
					var step = split[0] + "=" + stepSimplification.newNumerator + "/" + stepSimplification.newDenominator;
					text = text.replace("[STEP]", step);
				}
			}
		}
		
		return text;
	}
}

//Para equacoes do tipo +-ax=expressao numerica
function type2NumericalExpression(currentStep) {
	var text = findParcialNumericExpression(currentStep);
	
	if (levelHint > 1) {
		var terms = findTermsOnSideFromStart(currentStep, false, "right", 2);	
		var b = terms[0].toString;
		var c = terms[1].toString.substring(1);
			
		text = text.replace("[b]", b);
		text = text.replace("[c]", c);
		
		if (levelHint === 4) {
			var parcialExpression = terms[0].toString + terms[1].toString;
			var result = "" + eval(parcialExpression);
			text = text.replace("[RESULT]", result);
		}
	}
	
	return text;	
}

//Para equacoes do tipo +-ax[operador]b=+-c
function type3(currentStep, operator, signC, formatEquation) {
//	var formatEquation = "x" + operator + "b" + "=" + signC + "c";
	var text = hintsTXT[formatEquation][levelHint-1];
	
	if (levelHint > 1) {
		var split = currentStep.split("=");
		
		if (currentStep[0] === "-") {
			split[0] = split[0].substring(1);
		}
		
		var leftSide = split[0].split(operator);
		
		if (currentStep[0] === "-") {
			leftSide[0] = "-" + leftSide[0];
		}
		
		if (levelHint < 4 && operator === "-") {
			leftSide[1] = "-" + leftSide[1];
		}
		if (levelHint === 5) {
			var step;
			
			if (operator === "+") {
				step = split[0] + "-" + leftSide[1] + "=" + split[1] + "-" + leftSide[1];
			}
			
			else {
				step = split[0] + "+" + leftSide[1] + "=" + split[1] + "+" + leftSide[1];
			}
			
			
			text = text.replace("[STEP]", step);
		}
		
		else {			
			text = replaceAll(text, "[b]", leftSide[1]);
			
			if (levelHint === 2) {		
				text = text.replace("[c]", split[1]);
			}
		}
	}
	
	return text;
}

//Para equacoes do tipo +-ax=+-c
function type4(currentStep, signA, signC, formatEquation) {
	//var formatEquation = signA + "ax=" + signC + "c";
	var text = hintsTXT[formatEquation][levelHint-1];
	
	if (levelHint >= 4) {
		var split = currentStep.split("x=");
		
		if (levelHint === 4) {
			text = text.replace("[a]", split[0]);
		}
		
		else {
			var division;
			
			if (signA !== "-") {
				division = "/" + split[0];
			}
			
			else {
				division = "/(" + split[0] + ")";
			}
			
			var step = split[0] + "x" + division + "=" + split[1] + division;
			text = text.replace("[STEP]", step);
		}
	}
	
	return text;
}

//Para equacoes do tipo +-x/b=+-c
function type5(currentStep, signX, signC, formatEquation) {
	//var formatEquation = signX + "x/b=" + signC + "c";
	var text = hintsTXT[formatEquation][levelHint-1];
	
	if (levelHint >= 4) {
		var equation = replaceAll(currentStep, "(", "");
		equation = replaceAll(equation, ")", "");
		
		var split = equation.split("=");
		var fraction = split[0].split("/")
		
		if (levelHint === 4) {
			text = replaceAll(text, "[b]", fraction[1]);
		}
		
		else {
			var step = fraction[0] + "=" + split[1] + "*" + fraction[1];
			text = text.replace("[STEP]", step);
		}
	}
	
	return text;
}

//Para equacoes do tipo +-ax/b=+-c
function type5WithCoefficient(currentStep, signA, signC, formatEquation) {
	//var formatEquation = signA + "ax/b=" + signC + "c";
	var text;
	var equation = replaceAll(currentStep, "(", "");
	equation = replaceAll(equation, ")", "");
	
	var split = equation.split("=");
	var fraction = split[0].split("/");
	
	var a = fraction[0].replace("x", "");
	var b = fraction[1];
	var c = split[1];
	
	if (Math.abs(a) === Math.abs(b)) {
		text = hintsTXT[formatEquation][0][levelHint-1];
		
		if (levelHint > 1) {
			if (levelHint < 5) {
				text = replaceAll(text, "[a]", a);
				text = replaceAll(text, "[b]", b);
			}
			
			else {
				var step = "x=" + c;
				
				if (signA === "-") {
					step = "-" + step;
				}
				
				text = text.replace("[STEP]", step);
			}
		}
	}
	
	else if (!isIrreducible(parseInt(a), parseInt(b))) {
		if (levelHint === 1 || levelHint >= 4) {
			if (levelHint === 1) {		
				text = replaceAll(text, "[a]", a);
				text = text.replace("[b]", b);
			}
			
			else {
				var stepSimplification = simplifyOneStep(split[1]);
				
				if (levelHint === 4) {				
					var number = "" + stepSimplification.divisor;				
					text = text.replace("[NUMBER]", number);
				}
				
				else {
					var step = split[0] + "=" + stepSimplification.newNumerator + "/" + stepSimplification.newDenominator;
					text = text.replace("[STEP]", step);
				}
			}
		}
	}
	
	else {
		if (levelHint >= 4) {
			if (levelHint === 4) {
				text = text.replace("[b]", b);
			}
			
			else {
				var step = split[0] + "*" + b + "=" + split[1] + "*" + b;
				text = text.replace("[STEP]", step);
			}
		}
	}
	
	return text;
}

//Varios termos
function type6(currentStep) {
	var multiplication = findMultiplicationFromStart(currentStep);
	
	if (multiplication !== null) {
		return type6Multiplication(currentStep, multiplication);	
	}
	
	var hasX = [false, true];
	var sides = ["left", "right"];	
	var text = null;
		
	for (var i = 0; text === null && i < hasX.length; i++) {
		for (var j = 0; text === null && j < sides.length; j++) {
			text = findParcialExpressionOnSide(currentStep, hasX[i], sides[j]);
		}
	}
		
	//Caso em que os termos semelhantes estao em diferentes lados da equacao
	if (text === null) {
		text = findSimilarTermsOnDifferentSides(currentStep);
	}
	
	return text;
}

//Caso em que ha uma conta de multiplicacao
function type6Multiplication(currentStep, multiplication) {
	var text;
	
	if (multiplication.toString.indexOf("-") === -1) {
		text = hintsTXT["many terms with multiplication positive"][levelHint-1];
	}
	
	else if (multiplication.toString.split("-").length === 3) {
		text = hintsTXT["many terms with multiplication negative"][levelHint-1];
	}
	
	else {
		text = hintsTXT["many terms with multiplication positive and negative"][levelHint-1];
	}
	
	if (levelHint > 1) {
		var terms = multiplication.terms;	
		
		if (levelHint === 2) {
			var termsString = terms[0].toString + " e " + terms[1].toString;			
			text = text.replace("[TERMS]", termsString);
		}
		
		else if (levelHint < 5) {
			text = text.replace("[TERMS MULTIPLICATION]", multiplication.toString);
			
			if (levelHint === 4) {
				var result = "" + multiplyTerms(terms);
				text = text.replace("[RESULT]", result);
			}
		}
		
		else {
			var result = "" + multiplyTerms(terms);
			var step = currentStep.replace(multiplication.toString, result);
			step = step.replace("+-", "-");
			step = step.replace("--", "+");
			text = text.replace("[STEP]", step);	
		}
	}
	
	return text;
}


//Parenteses com sinal de + na frente
function type7PlusSign(currentStep) {
	if (levelHint < 5) {
		return type7Aux(currentStep, "parentheses with plus sign", "+");
	}
	
	else {
		var text = hintsTXT["parentheses with plus sign"][levelHint-1];
		var expression = findExpressionInParenthesesFromStart(currentStep, "+");
		var step = currentStep.replace(expression.toString, expression.termInParentheses.toString);
		step = step.replace("+-", "-");
		text = text.replace("[STEP]", step);
		
		return text;
	}
}

//Parenteses com sinal de - na frente
function type7MinusSign(currentStep) {
	if (levelHint < 4) {
		return type7Aux(currentStep, "parentheses with minus sign", "-");
	}
	
	else {
		var text;
		var expression = findExpressionInParenthesesFromStart(currentStep, "-");
		var result = replaceAll(expression.termInParentheses.toString, "+", "#");
		
		result = replaceAll(result, "-", "+");
		result = replaceAll(result, "#", "-");
		
		if (result[0] !== "+") {
			result = "-" + result;
		}
		
		if (levelHint === 4) {
			text = type7Aux(currentStep, "parentheses with minus sign", "-").replace("[RESULT]", result);					
		}
		
		else {
			text = hintsTXT["parentheses with minus sign"][levelHint-1];
			var parentheses = expression.toString.substring(1);
			var step = currentStep.replace("-" + parentheses, result);
			step = step.replace("=+", "=");
			text = text.replace("[STEP]", step);
		}
		
		return text;
	}
}

function type7Aux(currentStep, formatEquation, operator) {
	var text = hintsTXT[formatEquation][levelHint-1];
	
	if (levelHint === 2 || levelHint === 4) {
		var expression = findExpressionInParenthesesFromStart(currentStep, operator);
		var parentheses = expression.toString.substring(1);		
		text = text.replace("[PARENTHESES]", parentheses);
	}
	
	return text;
}

//Propriedade Distributiva
function type8(currentStep) {
	var multiplication = findMultiplicationFromStart(currentStep);
	
	if (multiplication !== null) {
		return type6Multiplication(currentStep, multiplication);	
	}
	
	var text = hintsTXT["distributive property"][levelHint-1];
	
	if (levelHint > 3) {
		var term = findTermWithDistributivePropertyFromStart(currentStep);
		var distributiveProperty = term.toString;
		var multiplier = term.multiplier;
		var termsInParentheses = term.termInParentheses.terms;
		var operator = distributiveProperty[0];
		
		if (isNumber(operator)) {
			operator = "+";
		}
		
		var result;
		var multiplication2 = multiplyTwoTerms(multiplier, termsInParentheses[1]);
		
		if (multiplication2[0] !== "-") {
			if (operator === "+") {
				result = multiplyTwoTerms(multiplier, termsInParentheses[0]) + operator + multiplication2;
			}
			
			else {
				result = multiplyTwoTerms(multiplier, termsInParentheses[0]) + "+" + multiplication2;
			}
		}
		
		else {
			result = multiplyTwoTerms(multiplier, termsInParentheses[0]) + multiplication2;	
		}
		
		if (levelHint === 4) {
			text = text.replace("[DISTRIBUTIVE PROPERTY]", distributiveProperty);
			text = text.replace("[RESULT]", result);
		}
		
		else {
			var step = currentStep.replace(distributiveProperty, result);
			text = text.replace("[STEP]", step);
		}
	}
	
	return text;
}

//Razao e Proporcao
function type9(currentStep) {
	var fractions = currentStep.split("=");
	var leftFractionIsIrreducible = isIrreducibleString(fractions[0]);
	var fraction;
	var side;
	var text;
	
	if (!leftFractionIsIrreducible || !isIrreducibleString(fractions[1])) {
		if (!leftFractionIsIrreducible) {
			fraction = replaceAll(fractions[0], "(", "");
			side = "left";
		}
		
		else {
			fraction = replaceAll(fractions[1], "(", "");
			side = "right";
		}
		
		fraction = replaceAll(fraction, ")", "");
		text = type9Aux(currentStep, fraction, side);
	}
	
	else {	
		text = hintsTXT["reason and proportion"][levelHint-1];
		
		if (levelHint > 3) {	
			var leftFraction = findFractionalTermOnSideFromStart(currentStep, true, "left");
			var rightFraction = findFractionalTermOnSideFromStart(currentStep, true, "right");
			
			if (leftFraction === null) {
				leftFraction = findFractionalTermOnSideFromStart(currentStep, false, "left");
			}
			
			//Else if porque um dos lados obrigatoriamente tem que ter o X. Assim, se o esquerdo não tiver, o direito terá
			else if (rightFraction === null) {
				rightFraction = findFractionalTermOnSideFromStart(currentStep, false, "right");
			}
			
			var leftDenominator = leftFraction.denominator.toString;
			var rightDenominator = rightFraction.denominator.toString;
			
			if (levelHint === 4) {
				text = text.replace("[LEFT DENOMINATOR]", leftDenominator);
				text = text.replace("[RIGHT DENOMINATOR]", rightDenominator);
			}
			
			else {
				var leftNumerator = leftFraction.numerator.toString;
				var rightNumerator = rightFraction.numerator.toString;
				
				if (leftFraction.numerator instanceof CompoundTerm) {
					leftNumerator = "(" + leftNumerator + ")";
				}
				
				if (leftFraction.denominator instanceof CompoundTerm) {
					leftDenominator = "(" + leftDenominator + ")";
				}
				
				if (rightFraction.numerator instanceof CompoundTerm) {
					rightNumerator = "(" + rightNumerator + ")";
				}
				
				if (rightFraction.denominator instanceof CompoundTerm) {
					rightDenominator = "(" + rightDenominator + ")";
				}
				
				var leftSide = rightDenominator;
				
				if (leftSide[0] !== "(" && leftSide[leftSide.length-1] !== ")" && leftSide.indexOf("x") === -1) {
					leftSide += "*" + leftNumerator
				}
				
				else {
					leftSide = leftNumerator + "*" + leftSide;
				}
				
				var rightSide = leftDenominator;
				
				if (rightSide[0] !== "(" && rightSide[rightSide.length-1] !== ")" && rightSide.indexOf("x") === -1) {
					rightSide += "*" + rightNumerator; 
				}
				
				else {
					rightSide = rightNumerator + "*" + rightSide;
				}
				
				var step = leftSide + "=" + rightSide;
				text = text.replace("[STEP]", step);
			}
		}
	}
	
	return text;
}

function type9Aux(currentStep, fraction, side) {
	var numbers = fraction.split("/");
	var a = numbers[0].replace("x", "");
	var b = numbers[1].replace("x", "");
	var text;
	
	if (Math.abs(a) === Math.abs(b)) {
		if (a === b) {
			text = hintsTXT["ax/b=c"][0][levelHint-1];
		}
		
		else {
			text = hintsTXT["-ax/b=c"][0][levelHint-1];
		}
		
		if (levelHint > 1) {
			if (levelHint < 5) {			
				text = replaceAll(text, "[a]", a);
				text = text.replace("[b]", b);
			}
			
			else {
				var split = currentStep.split("=");
				var step;
				
				if (side === "left") {
					step = evalFraction(fraction) + "=" + split[1];
				}
				
				else {
					step = split[0] + "=" + evalFraction(fraction);
				}
				
				text = text.replace("[STEP]", step);
			}
		}
	}
	
	else {
		text = hintsTXT["ax/b=c"][1][levelHint-1];
		
		if (levelHint === 1 || levelHint >= 4) {
			if (levelHint === 1) {		
				text = replaceAll(text, "[a]", a);
				text = text.replace("[b]", b);
			}
			
			else {
				var stepSimplification = simplifyOneStep(fraction);
				
				if (levelHint === 4) {				
					var number = "" + stepSimplification.divisor;				
					text = text.replace("[NUMBER]", number);
				}
				
				else {
					var split = currentStep.split("=");
					var step;
					
					if (side === "left") {
						step = stepSimplification.newNumerator + "/" + stepSimplification.newDenominator + "=" + split[1];
					}
					
					else {
						step = split[0] + "=" + stepSimplification.newNumerator + "/" + stepSimplification.newDenominator;
					}
					
					text = text.replace("[STEP]", step);
				}
			}
		}
	}
	
	return text;
}

//Fracoes
function type10(currentStep, type) {
	var multiplication = findMultiplicationFromStart(currentStep);
	
	if (multiplication !== null && multiplication.toString.indexOf("/") !== -1) {
		return type10Multiplication(currentStep, multiplication);	
	}
	
	var text = hintsTXT[type][levelHint-1];
	
	if (levelHint > 3) {
		var fractions = findAllFractionalTerms(currentStep);	
		var result = "";
		var denominators = new Array();
		
		for (var i = 0; i < fractions.length; i++) {
			denominators.push(fractions[i].denominator.coefficient);
		}
		
		var mmc = calculateMMC(denominators);
		var terms = [findAllTermsOnSide(currentStep, "left"), findAllTermsOnSide(currentStep, "right")];
		
		if (levelHint === 4) {
			var mmcString = "" + mmc;
			text = text.replace("[MMC]", mmcString);	
			var cont = 1;
			
			for (var i = 0; i < terms.length; i++) {
				for (var j = 0; j < terms[i].length; j++) {
					if (terms[i][j].type === "Term") {
						var toString = terms[i][j].term.toString;
						
						if (isNumber(toString[0])) {
							text += "<br>" + cont + "º termo: " + mmc + "*" + toString;
						}
						
						else if (toString[0] === "+") {
							text += "<br>" + cont + "º termo: " + mmc + "*" + toString.substring(1);
						}
						
						else {
							text += "<br>" + cont + "º termo: " + mmc + "*(-" + toString.substring(1) + ")";
						}
					}
					
					else {
						var numerator = terms[i][j].term.numerator.toString;
						var denominator = terms[i][j].term.denominator.coefficient;
						
						if ($.isNumeric(numerator) || $.isNumeric(numerator.replace("x", "")) || numerator.indexOf("*(") !== -1) {
							text += "<br>" + cont + "º termo: " + (mmc / denominator) + "*" + numerator;
						}
						
						else {
							text += "<br>" + cont + "º termo: " + (mmc / denominator) + "*(" + numerator + ")";
						}
					}
					
					cont++;
				}
			}
		}
		
		else {
			var step = "";
			
			for (var i = 0; i < terms.length; i++) {
				for (var j = 0; j < terms[i].length; j++) {
					var term = terms[i][j].term.toString;
					var expression;
					
					if (terms[i][j].type === "Term") {
						if (isNumber(term[0])) {
							expression = mmc + "*" + term;
						}
						
						else if (term[0] === "+") {
							expression = "+" + mmc + "*" + term.substring(1);
						}
						
						else {
							expression = "+" + mmc + "*(" + term + ")";
						}			
					}
					
					else {
						var numerator = terms[i][j].term.numerator.toString;
						var denominator = terms[i][j].term.denominator.coefficient;
						
						if ($.isNumeric(numerator) || $.isNumeric(numerator.replace("x", "")) || numerator.indexOf("*(") !== -1) {
							expression = (mmc / denominator) + "*" + numerator;
						}
						
						else {
							expression = (mmc / denominator) + "*(" + numerator + ")";
						}
						
						if (term[0] === "-") {
							expression = "-" + expression;
						}
						
						else if (j > 0) {
							expression = "+" + expression;
						}	
					}
					
					step += expression;
				}
				
				if (i === 0) {
					step += "=";
				}
			}
			
			if (step[0] === "+") {
				step = step.substring(1);
			}
			
			text = text.replace("[STEP]", step);
		}
		
		text = replaceAll(text, "*-", "*");
	}
	
	return text;
}

//Caso em que ha uma conta de multiplicacao envolvendo uma fracao
function type10Multiplication(currentStep, multiplication) {
	var fraction = findFractionalTermFromStart(multiplication.toString, true);
	var terms = multiplication.toString.split("*");
	var term2;
	var denominatorEqualsMultiplier;
	
	if (terms[1].indexOf("/") === -1 || (terms[0].indexOf("/") !== -1 && terms[1].indexOf("/") !== -1)) {
		term2 = terms[1];
		denominatorEqualsMultiplier = true;
	}
	
	else {
		term2 = terms[0];
		denominatorEqualsMultiplier = false;
	}
	
	var text;
	
	if (fraction.denominator.coefficient === parseInt(term2)) {
		text = hintsTXT["fractions with multiplication"][0][levelHint-1];
	}
	
	else {
		text = hintsTXT["fractions with multiplication"][1][levelHint-1];
	}
	
	if (levelHint > 1 && levelHint !== 3) {
		var terms = multiplication.terms;
		var numerator = terms[0].numerator;
		var denominator = terms[0].denominator;
		var multiplier = terms[1];
		
		if (text.indexOf("[b]") !== -1) {
			text = text.replace("[b]", terms[0].toString);
			text = text.replace("[c]", multiplier.toString);
		}
			
		if (levelHint >= 4) {
			var result;
			
			if (denominatorEqualsMultiplier) {
				result = numerator.toString;
			}
			
			else {
				result = multiplyTwoTerms(numerator, multiplier) + "/" + denominator.toString;
			}
			
			if (levelHint === 4) {
				text = text.replace("[RESULT]", result);
			}
			
			else {
				var step = currentStep.replace(multiplication.toString, result);
				text = text.replace("[STEP]", step);
			}
		}
	}
	
	return text;
}

//Quando o numero esta no lado esquerdo e x no lado direito
function type11(currentStep) {
	text = hintsTXT["c=x"][levelHint-1];
	
	if (levelHint > 2) {
		var terms = currentStep.split("=");
		
		if (levelHint < 5) {
			text = text.replace("[c]", terms[0]);
			text = text.replace("[x]", terms[1]);
		}
		
		else {
			var step = terms[1] + "=" + terms[0];
			text = text.replace("[STEP]", step);
		}
	}
	
	return text;
}