errorFeedbacks_ptBR["-x=c"] = {};
errorFeedbacks_ptBR["-x=c"]["x=c"] = "Observe que X é negativo, assim você precisará inverter o sinal da resposta para torná-lo positivo";

errorFeedbacks_ptBR["x=b+c"] = "Você errou o resultado da soma [b] + [c]. Revise os seu cálculos e tente novamente";
errorFeedbacks_ptBR["-x=b+c"] = errorFeedbacks_ptBR["x=b+c"];
errorFeedbacks_ptBR["ax=b+c"] = errorFeedbacks_ptBR["x=b+c"];
errorFeedbacks_ptBR["-ax=b+c"] = errorFeedbacks_ptBR["x=b+c"];

errorFeedbacks_ptBR["x=b-c"] = "Você errou o resultado da diferença [b] - [c]. Revise os seus cálculos e tente novamente";
errorFeedbacks_ptBR["-x=b-c"] = errorFeedbacks_ptBR["x=b-c"];
errorFeedbacks_ptBR["ax=b-c"] = errorFeedbacks_ptBR["x=b-c"];
errorFeedbacks_ptBR["-ax=b-c"] = errorFeedbacks_ptBR["x=b-c"];

errorFeedbacks_ptBR["x=b*c"] = "Você errou o resultado do produto [b] * [c]. Revise os seus cálculos e tente novamente";
errorFeedbacks_ptBR["-x=b*c"] = errorFeedbacks_ptBR["x=b*c"];
errorFeedbacks_ptBR["ax=b*c"] = errorFeedbacks_ptBR["x=b*c"];
errorFeedbacks_ptBR["-ax=b*c"] = errorFeedbacks_ptBR["x=b*c"];

errorFeedbacks_ptBR["x=b/c"] = new Array();
//Resultado inteiro
errorFeedbacks_ptBR["x=b/c"][0] = "Você errou o resultado da divisão [b] / [c]. Revise os seus cálculos e tente novamente";
//Errou numerador
errorFeedbacks_ptBR["x=b/c"][1] = "Você errou o cálculo da divisão no numerador da fração";
//Errou denominador
errorFeedbacks_ptBR["x=b/c"][2] = "Você errou o cálculo da divisão no denominador da fração";
//Errou ambos
errorFeedbacks_ptBR["x=b/c"][3] = "Você errou o cálculo da divisão no numerador e no denominador da fração";
//Utilizou divisores diferentes
errorFeedbacks_ptBR["x=b/c"][4] = "Você utilizou divisores diferentes para o numerador e o denominador da fração. Lembre-se que o número divisor deve ser o mesmo para ambos";
errorFeedbacks_ptBR["-x=b/c"] = errorFeedbacks_ptBR["x=b/c"];
errorFeedbacks_ptBR["ax=b/c"] = errorFeedbacks_ptBR["x=b/c"];
errorFeedbacks_ptBR["-ax=b/c"] = errorFeedbacks_ptBR["x=b/c"];

errorFeedbacks_ptBR["x=-b+c"] = new Array();
//Errou o sinal do resultado
errorFeedbacks_ptBR["x=-b+c"][0] = "Você acertou o valor absoluto da soma [b] + [c], mas errou o sinal. Observe que o valor absoluto de [b] é maior do que [c], assim o resultado deverá ser negativo";
//Fez a soma como se b fosse positivo
errorFeedbacks_ptBR["x=-b+c"][1] = "Você calculou a soma [b] + [c] como se o número [b] fosse positivo. Refaça o cálculo considerando o sinal de menos do número [b] (lembre-se que uma outra maneira de resolver é a partir da diferença [c] - [b]";
//Errou o sinal do resultado
errorFeedbacks_ptBR["x=-b+c"][2] = "Você acertou o valor absoluto da soma [b] + [c], mas errou o sinal. Observe que o valor absoluto de [b] é menor do que [c], assim o resultado deverá ser positivo";
//Errou todo o calculo
errorFeedbacks_ptBR["x=-b+c"][3] = "Você errou o resultado da soma [b] + [c]. Revise os seu cálculos e tente novamente";
errorFeedbacks_ptBR["-x=-b+c"] = errorFeedbacks_ptBR["x=-b+c"];
errorFeedbacks_ptBR["ax=-b+c"] = errorFeedbacks_ptBR["x=-b+c"];
errorFeedbacks_ptBR["-ax=-b+c"] = errorFeedbacks_ptBR["x=-b+c"];

errorFeedbacks_ptBR["x=-b-c"] = new Array();
//Errou o sinal do resultado
errorFeedbacks_ptBR["x=-b-c"][0] = "Você acertou o valor absoluto da diferença [b] - [c], mas errou o sinal. Observe que [b] e [c] são negativos, assim o resultado deverá ser negativo também";
//Fez a subtracao como se b fosse positivo
errorFeedbacks_ptBR["x=-b-c"][1] = "Você calculou a diferença [b] - [c] como se o número [b] fosse positivo. Refaça o cálculo considerando o sinal de menos do número [b] (você pode considerar essa expressão como uma soma de dívidas)";
//Errou todo o calculo
errorFeedbacks_ptBR["x=-b-c"][2] = "Você errou o resultado da diferença [b] - [c]. Revise os seu cálculos e tente novamente (você pode considerar essa expressão como uma soma de dívidas)";
errorFeedbacks_ptBR["-x=-b-c"] = errorFeedbacks_ptBR["x=-b-c"];
errorFeedbacks_ptBR["ax=-b-c"] = errorFeedbacks_ptBR["x=-b-c"];
errorFeedbacks_ptBR["-ax=-b-c"] = errorFeedbacks_ptBR["x=-b-c"];

errorFeedbacks_ptBR["x=-b*c"] = new Array();
//Errou o sinal do resultado
errorFeedbacks_ptBR["x=-b*c"][0] = "Você acertou o valor absoluto do produto [b] * [c], mas errou o sinal. Lembre-se que a multiplicação de um número positivo por um negativo (ou vice-versa) resulta em um número negativo";
//Errou todo o calculo
errorFeedbacks_ptBR["x=-b*c"][1] = "Você errou o resultado do produto [b] * [c]. Revise os seu cálculos e tente novamente. Lembre-se que a multiplicação de um número positivo por um negativo (ou vice-versa) resulta em um número negativo";
errorFeedbacks_ptBR["-x=-b*c"] = errorFeedbacks_ptBR["x=-b*c"];
errorFeedbacks_ptBR["ax=-b*c"] = errorFeedbacks_ptBR["x=-b*c"];
errorFeedbacks_ptBR["-ax=-b*c"] = errorFeedbacks_ptBR["x=-b*c"];

errorFeedbacks_ptBR["x=b*-c"] = new Array();
//Errou o sinal do resultado
errorFeedbacks_ptBR["x=b*-c"][0] = "Você acertou o valor absoluto do produto [b] * ([c]), mas errou o sinal. Lembre-se que a multiplicação de um número positivo por um negativo (ou vice-versa) resulta em um número negativo";
//Errou todo o calculo
errorFeedbacks_ptBR["x=b*-c"][1] = "Você errou o resultado do produto [b] * ([c]). Revise os seu cálculos e tente novamente. Lembre-se que a multiplicação de um número positivo por um negativo (ou vice-versa) resulta em um número negativo";
errorFeedbacks_ptBR["-x=b*-c"] = errorFeedbacks_ptBR["x=b*-c"];
errorFeedbacks_ptBR["ax=b*-c"] = errorFeedbacks_ptBR["x=b*-c"];
errorFeedbacks_ptBR["-ax=b*-c"] = errorFeedbacks_ptBR["x=b*-c"];

errorFeedbacks_ptBR["x=-b*-c"] = new Array();
errorFeedbacks_ptBR["x=-b*-c"][0] = "Você acertou o valor absoluto do produto [b] * ([c]), mas errou o sinal. Lembre-se que a multiplicação de dois números negativos resulta em um número positivo";
errorFeedbacks_ptBR["x=-b*-c"][1] = "Você errou o resultado do produto [b] * ([c]). Revise os seu cálculos e tente novamente. Lembre-se que a multiplicação de dois números negativos resulta em um número positivo";
errorFeedbacks_ptBR["-x=-b*-c"] = errorFeedbacks_ptBR["x=-b*-c"];
errorFeedbacks_ptBR["ax=-b*-c"] = errorFeedbacks_ptBR["x=-b*-c"];
errorFeedbacks_ptBR["-ax=-b*-c"] = errorFeedbacks_ptBR["x=-b*-c"];

errorFeedbacks_ptBR["x=-b/c"] = new Array();
//Errou o sinal do resultado
errorFeedbacks_ptBR["x=-b/c"][0] = "Você acertou o valor absoluto da divisão [b] / [c], mas errou o sinal. Lembre-se que a divisão de um número positivo por um negativo (ou vice-versa) resulta em um número negativo";
//Errou todo o calculo
errorFeedbacks_ptBR["x=-b/c"][1] = "Você errou o resultado da divisão [b] / [c]. Revise os seu cálculos e tente novamente. Lembre-se que a divisão de um número positivo por um negativo (ou vice-versa) resulta em um número negativo";
errorFeedbacks_ptBR["-x=-b/c"] = errorFeedbacks_ptBR["x=-b/c"];
errorFeedbacks_ptBR["ax=-b/c"] = errorFeedbacks_ptBR["x=-b/c"];
errorFeedbacks_ptBR["-ax=-b/c"] = errorFeedbacks_ptBR["x=-b/c"];

errorFeedbacks_ptBR["x=b/-c"] = new Array();
//Errou o sinal do resultado
errorFeedbacks_ptBR["x=b/-c"][0] = "Você acertou o valor absoluto da divisão [b] / ([c]), mas errou o sinal. Lembre-se que a divisão de um número positivo por um negativo (ou vice-versa) resulta em um número negativo";
//Errou todo o calculo
errorFeedbacks_ptBR["x=b/-c"][1] = "Você errou o resultado da divisão [b] / ([c]). Revise os seu cálculos e tente novamente. Lembre-se que a divisão de um número positivo por um negativo (ou vice-versa) resulta em um número negativo";
errorFeedbacks_ptBR["-x=b/-c"] = errorFeedbacks_ptBR["x=b/-c"];
errorFeedbacks_ptBR["ax=b/-c"] = errorFeedbacks_ptBR["x=b/-c"];
errorFeedbacks_ptBR["-ax=b/-c"] = errorFeedbacks_ptBR["x=b/-c"];

errorFeedbacks_ptBR["x=-b/-c"] = new Array();
//Errou o sinal do resultado
errorFeedbacks_ptBR["x=-b/-c"][0] = "Você acertou o valor absoluto da divisão [b] / ([c]), mas errou o sinal. Lembre-se que a divisão de dois números negativos resulta em um número positivo";
//Errou todo o calculo
errorFeedbacks_ptBR["x=-b/-c"][1] = "Você errou o resultado da divisão [b] / ([c]). Revise os seu cálculos e tente novamente. Lembre-se que a divisão de dois números negativos resulta em um número positivo";
errorFeedbacks_ptBR["-x=-b/-c"] = errorFeedbacks_ptBR["x=-b/-c"];
errorFeedbacks_ptBR["ax=-b/-c"] = errorFeedbacks_ptBR["x=-b/-c"];
errorFeedbacks_ptBR["-ax=-b/-c"] = errorFeedbacks_ptBR["x=-b/-c"];

errorFeedbacks_ptBR["x+b=c"] = {};
errorFeedbacks_ptBR["x+b=c"]["x=c+b"] = "Você esqueceu de inverter a operação ao passar o termo [b] para o outro lado da equação. Como ele está somando X no lado esquerdo, ele deverá subtrair de [c] no lado direito";
errorFeedbacks_ptBR["x+b=c"]["-x=c+b"] = errorFeedbacks_ptBR["x+b=c"]["x=c+b"];
errorFeedbacks_ptBR["-x+b=c"] = errorFeedbacks_ptBR["x+b=c"];
errorFeedbacks_ptBR["ax+b=c"] = errorFeedbacks_ptBR["x+b=c"];
errorFeedbacks_ptBR["-ax+b=c"] = errorFeedbacks_ptBR["x+b=c"];
errorFeedbacks_ptBR["ax+b=c"]["ax=c+b"] = errorFeedbacks_ptBR["x+b=c"]["x=c+b"];
errorFeedbacks_ptBR["-ax+b=c"]["-ax=c+b"] = errorFeedbacks_ptBR["x+b=c"]["x=c+b"];

errorFeedbacks_ptBR["x-b=c"] = {};
errorFeedbacks_ptBR["x-b=c"]["x=c-b"] = "Você esqueceu de inverter a operação ao passar o termo [b] para o outro lado da equação. Como ele está subtraindo de X no lado esquerdo, ele deverá fazer uma soma com [c] no lado direito";
errorFeedbacks_ptBR["x-b=c"]["-x=c-b"] = errorFeedbacks_ptBR["x-b=c"]["x=c-b"];
errorFeedbacks_ptBR["-x-b=c"] = errorFeedbacks_ptBR["x-b=c"];
errorFeedbacks_ptBR["ax-b=c"] = errorFeedbacks_ptBR["x-b=c"];
errorFeedbacks_ptBR["-ax-b=c"] = errorFeedbacks_ptBR["x-b=c"];
errorFeedbacks_ptBR["ax-b=c"]["ax=c-b"] = errorFeedbacks_ptBR["x-b=c"]["x=c-b"];
errorFeedbacks_ptBR["-ax-b=c"]["-ax=c-b"] = errorFeedbacks_ptBR["x-b=c"]["x=c-b"];

errorFeedbacks_ptBR["ax=c"] = {};
errorFeedbacks_ptBR["-ax=c"] = {};
errorFeedbacks_ptBR["ax=c"]["x=c*a"] = "Você esqueceu de inverter a operação ao passar o termo [a] para o outro lado da equação. Como ele está multiplicando X no lado esquerdo, ele deverá dividir [c] no lado direito";
errorFeedbacks_ptBR["-ax=c"]["x=c*-a"] = "Você esqueceu de inverter a operação ao passar o termo [a] para o outro lado da equação. Como ele está multiplicando X no lado esquerdo, ele deverá dividir [c] no lado direito";
errorFeedbacks_ptBR["ax=c"]["x=c*-a"] = "Nesse caso você não deve inverter o sinal do termo [a] ao passá-lo para o outro lado da equação, mas sim inverter a operação. Como ele está multiplicando X no lado esquerdo, deverá dividir [c] no lado direito";
errorFeedbacks_ptBR["-ax=c"]["x=c*a"] = "Nesse caso você não deve inverter o sinal do termo [a] ao passá-lo para o outro lado da equação, mas sim inverter a operação. Como ele está multiplicando X no lado esquerdo, deverá dividir [c] no lado direito";
errorFeedbacks_ptBR["ax=c"]["x=c/-a"] = "Nesse caso você não deve inverter o sinal do termo [a] ao passá-lo para o outro lado da equação. Você só deve inverter o sinal se o termo estiver somando ou subtraindo (pense que somente a operação deve ser invertida)";
errorFeedbacks_ptBR["-ax=c"]["x=c/a"] = "Nesse caso você não deve inverter o sinal do termo [a] ao passá-lo para o outro lado da equação. Você só deve inverter o sinal se o termo estiver somando ou subtraindo (pense que somente a operação deve ser invertida)";

errorFeedbacks_ptBR["x/b=c"] = {};
errorFeedbacks_ptBR["-x/b=c"] = {};
errorFeedbacks_ptBR["x/b=c"]["x=c/b"] = "Você esqueceu de inverter a operação ao passar o termo [b] para o outro lado da equação. Como ele está dividindo X no lado esquerdo, ele deverá multiplicar [c] no lado direito";
errorFeedbacks_ptBR["-x/b=c"]["x=c/-b"] = "Você esqueceu de inverter a operação ao passar o termo [b] para o outro lado da equação. Como ele está dividindo X no lado esquerdo, ele deverá multiplicar [c] no lado direito";
errorFeedbacks_ptBR["-x/b=c"]["-x=c/b"] = "Você esqueceu de inverter a operação ao passar o termo [b] para o outro lado da equação. Como ele está dividindo X no lado esquerdo, ele deverá multiplicar [c] no lado direito";
errorFeedbacks_ptBR["x/b=c"]["x=c/-b"] = "Nesse caso você não deve inverter o sinal do termo [b] ao passá-lo para o outro lado da equação, mas sim inverter a operação. Como ele está dividindo X no lado esquerdo, deverá multiplicar [c] no lado direito";
errorFeedbacks_ptBR["-x/b=c"]["x=c/b"] = "Nesse caso você não deve inverter o sinal do termo [b] ao passá-lo para o outro lado da equação, mas sim inverter a operação. Como ele está dividindo X no lado esquerdo, deverá multiplicar [c] no lado direito";
errorFeedbacks_ptBR["x/b=c"]["x=c*-b"] = "Nesse caso você não deve inverter o sinal do termo [b] ao passá-lo para o outro lado da equação. Você só deve inverter o sinal se o termo estiver somando ou subtraindo (pense que somente a operação deve ser invertida)";
errorFeedbacks_ptBR["-x/b=c"]["x=c*b"] = "Nesse caso você não deve inverter o sinal do termo [b] ao passá-lo para o outro lado da equação. Você só deve inverter o sinal se o termo estiver somando ou subtraindo (pense que somente a operação deve ser invertida)";
errorFeedbacks_ptBR["-x/b=c"]["-x=c*-b"] = "Nesse caso você não deve inverter o sinal do termo [b] ao passá-lo para o outro lado da equação. Você só deve inverter o sinal se o termo estiver somando ou subtraindo (pense que somente a operação deve ser invertida)";
errorFeedbacks_ptBR["ax/b=c"] = errorFeedbacks_ptBR["x/b=c"];
errorFeedbacks_ptBR["-ax/b=c"] = errorFeedbacks_ptBR["-x/b=c"];
errorFeedbacks_ptBR["ax/b=c"]["ax=c/b"] = errorFeedbacks_ptBR["x/b=c"]["x=c/b"];
errorFeedbacks_ptBR["-ax/b=c"]["ax=c/-b"] = errorFeedbacks_ptBR["-x/b=c"]["x=c/-b"];
errorFeedbacks_ptBR["-ax/b=c"]["-ax=c/b"] = errorFeedbacks_ptBR["-x/b=c"]["-x=c/b"];
errorFeedbacks_ptBR["ax/b=c"]["ax=c/-b"] = errorFeedbacks_ptBR["x/b=c"]["x=c/-b"];
errorFeedbacks_ptBR["-ax/b=c"]["ax=c/b"] = errorFeedbacks_ptBR["x/b=c"]["x=c/-b"];
errorFeedbacks_ptBR["-ax/b=c"]["-ax=c/-b"] = errorFeedbacks_ptBR["x/b=c"]["x=c/-b"];
errorFeedbacks_ptBR["ax/b=c"]["ax=c*-b"] = errorFeedbacks_ptBR["x/b=c"]["x=c*-b"];
errorFeedbacks_ptBR["-ax/b=c"]["ax=c*b"] = errorFeedbacks_ptBR["-x/b=c"]["x=c*b"];
errorFeedbacks_ptBR["-ax/b=c"]["-ax=c*-b"] = errorFeedbacks_ptBR["x/b=c"]["x=c*-b"];

errorFeedbacks_ptBR["many terms"] = "Você não pode operar os termos [a] e [b] porque eles não são semelhantes. Dois termos são semelhantes se forem somente números ou se ambos acompanham X";
errorFeedbacks_ptBR["parentheses with plus sign"] = "Você errou um ou mais sinais dos termos ao remover os parênteses. Se o sinal que está na frente deles for positivo, os termos que estão dentro dos parênteses permanecem com o mesmo sinal; do contrário, você deverá inverter os sinais dos termos que estão dentro dos parênteses";
errorFeedbacks_ptBR["parentheses with minus sign"] = "Você errou um ou mais sinais dos termos ao remover os parênteses. Se o sinal que está á na frente deles for positivo, os termos que estão dentro dos parênteses permanecem com o mesmo sinal; do contrário, você deverá inverter os sinais dos termos que estão dentro dos parênteses";

errorFeedbacks_ptBR["distributive property"] = "Você não efetuou corretamente os cálculos da propriedade distributiva. Observe um exemplo de resolução: 4(2-5x) = 4*2-4*5x = 8-20x";

errorFeedbacks_ptBR["reason and proportion"] = 'Você não efetuou corretamente os cálculos da razão e proporção. Observe um exemplo de resolução:<br><span class="math-box"><span class="strut"></span><span class="vstack"><div class="denominator">2</div><div class="numerator">x</div><div class="frac-line-aux"><span class="frac-line"></span></div><span class="baseline-fix"></span></span></span>&nbsp&nbsp=&nbsp&nbsp<span class="math-box"><span class="strut"></span><span class="vstack"><div class="denominator">25</div><div class="numerator">42</div><div class="frac-line-aux"><span class="frac-line"></span></div><span class="baseline-fix"></span></span></span><br>25*x=42*2';

errorFeedbacks_ptBR["fractions"] = "Você não efetuou corretamente os cálculos nas frações. Lembre-se que o primeiro passo é calcular o MMC de todos os denominadores. Em seguida, você deverá dividir o valor obtido pelo seu denominador (se o termo não tiver denominador, considere-o como 1), e multiplicar pelo seu numerador. Observe que após esse procedimento você terá removido todos os denominadores";

errorFeedbacks_ptBR["general"] = "O passo informado está incorreto. Revise e tente novamente";