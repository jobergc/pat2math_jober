function errorFeedback(currentStep) {
	var text = selectErrorFeedback(currentStep);
	
	showFeedbackError(text);
	
	if (levelGamification !== "without") {
    	var lostPoints = -5;
    	
//    	if (levelGamification === "full") {      			
//    		if (freeErrors[planoAtual] > 0) {
//    			freeErrors[planoAtual]--;
//    			lostPoints = 0;
//    			
//    			var contentCookie = freeErrors[planoAtual] + "," + (planoAtual);
//    			
//    			setCookieDays("freeErrors", contentCookie, 1);
//    			
//    			verifyFreeErrors();
//    		}
//    	}
    	
//    	if (lostPoints < 0) {
    		callbackAddPoints(lostPoints);
//    	}
    }
}

function selectErrorFeedback(currentStep) {
	var previousStep;

	if (selectedEquation.lastStep !== null) {
		previousStep = selectedEquation.lastStep.step;
	}
	
	else {
		previousStep = selectedEquation.equation;
	}
	
	var formatEquation = checkFormat(previousStep);
	var text;
	
	if (levelError === 1) {
		var typeError = typesError[formatEquation];	
		
		if (typeError !== undefined) {	
			if (formatEquation === "-x=c" || formatEquation === "-x=-c") {
				text = type1E();
			}
			
			else if (typeError.length === 1) {
				text = window[typeError[0]](currentStep, previousStep);
			}
			
			else if (typeError.length === 4) {
				text = window[typeError[0]](currentStep, previousStep, typeError[1], typeError[2], typeError[3]);
			}
			
			else if (typeError.length === 5) {
				text = window[typeError[0]](currentStep, previousStep, typeError[1], typeError[2], typeError[3], typeError[4]);
			}
			
			else if (typeError.length === 3) {
				text = window[typeError[0]](currentStep, previousStep, typeError[1], typeError[2]);
			}
			
			else {
				text = window[typeError[0]](currentStep, previousStep, typeError[1]);
			}
		}
		
		else {
			text = typeGeneral();
		}
	}
	
	else {
		text = feedbackWithHint(formatEquation, previousStep);
	}
	
	registerError(previousStep, currentStep);
	
	if (previousFeedbackError === text) {
		text = feedbackWithHint(formatEquation, previousStep);
	}
	
	previousFeedbackError = text;
	
	return text;
}

var typesError = {};

typesError["-x=c"] = ["type1E"];
typesError["-x=-c"] = ["type1E"];

typesError["x=b+c"] = ["type2E", "+", "", "", "x=b+c"];
typesError["-x=b+c"] = ["type2E", "+", "", "", "-x=b+c"];
typesError["ax=b+c"] = ["type2E", "+", "", "", "ax=b+c"];
typesError["-ax=b+c"] = ["type2E", "+", "", "", "-ax=b+c"];

typesError["x=b-c"] = ["type2E", "-", "", "", "x=b-c"];
typesError["-x=b-c"] = ["type2E", "-", "", "", "-x=b-c"];
typesError["ax=b-c"] = ["type2E", "-", "", "", "ax=b-c"];
typesError["-ax=b-c"] = ["type2E", "-", "", "", "-ax=b-c"];

typesError["x=b*c"] = ["type2E", "*", "", "", "x=b*c"];
typesError["-x=b*c"] = ["type2E", "*", "", "", "-x=b*c"];
typesError["ax=b*c"] = ["type2E", "*", "", "", "ax=b*c"];
typesError["-ax=b*c"] = ["type2E", "*", "", "", "-ax=b*c"];

typesError["x=b/c"] = ["type2FractionE", "", "", "x=b/c"];
typesError["-x=b/c"] = ["type2FractionE", "", "", "-x=b/c"];
typesError["ax=b/c"] = ["type2FractionE", "", "", "ax=b/c"];
typesError["-ax=b/c"] = ["type2FractionE", "", "", "-ax=b/c"];

typesError["x=-b+c"] = ["type2E", "+", "-", "", "x=-b+c"];
typesError["-x=-b+c"] = ["type2E", "+", "-", "", "-x=-b+c"];
typesError["ax=-b+c"] = ["type2E", "+", "-", "", "ax=-b+c"];
typesError["-ax=-b+c"] = ["type2E", "+", "-", "", "-ax=-b+c"];

typesError["x=-b-c"] = ["type2E", "-", "-", "", "x=-b-c"];
typesError["-x=-b-c"] = ["type2E", "-", "-", "", "-x=-b-c"];
typesError["ax=-b-c"] = ["type2E", "-", "-", "", "ax=-b-c"];
typesError["-ax=-b-c"] = ["type2E", "-", "-", "", "-ax=-b-c"];

typesError["x=-b*c"] = ["type2E", "*", "-", "", "x=-b*c"];
typesError["x=b*-c"] = ["type2E", "*", "", "-", "x=b*-c"];
typesError["-x=-b*c"] = ["type2E", "*", "-", "", "-x=-b*c"];
typesError["-x=b*-c"] = ["type2E", "*", "", "-", "-x=b*-c"];
typesError["ax=-b*c"] = ["type2E", "*", "-", "", "ax=-b*c"];
typesError["ax=b*-c"] = ["type2E", "*", "", "-", "ax=b*-c"];
typesError["-ax=-b*c"] = ["type2E", "*", "-", "", "-ax=-b*c"];
typesError["-ax=b*-c"] = ["type2E", "*", "", "-", "-ax=b*-c"];	

typesError["x=-b*-c"] = ["type2E", "*", "-", "-", "x=-b*-c"];
typesError["-x=-b*-c"] = ["type2E", "*", "-", "-", "-x=-b*-c"];
typesError["ax=-b*-c"] = ["type2E", "*", "-", "-", "ax=-b*-c"];
typesError["-ax=-b*-c"] = ["type2E", "*", "-", "-", "-ax=-b*-c"];

typesError["x=-b/c"] = ["type2FractionE", "-", "", "x=-b/c"];
typesError["x=b/-c"] = ["type2FractionE", "", "-", "x=b/-c"];
typesError["-x=-b/c"] = ["type2FractionE", "-", "", "-x=-b/c"];
typesError["-x=b/-c"] = ["type2FractionE", "", "-", "x=b/-c"];
typesError["ax=-b/c"] = ["type2FractionE", "-", "", "ax=-b/c"];
typesError["ax=b/-c"] = ["type2FractionE", "", "-", "x=b/-c"];
typesError["-ax=-b/c"] = ["type2FractionE", "-", "", "-ax=-b/c"];
typesError["-ax=b/-c"] = ["type2FractionE", "", "-", "x=b/-c"];	

typesError["x=-b/-c"] = ["type2FractionE", "-", "-", "x=-b/-c"];
typesError["-x=-b/-c"] = ["type2FractionE", "-", "-", "-x=-b/-c"];
typesError["ax=-b/-c"] = ["type2FractionE", "-", "-", "ax=-b/-c"];
typesError["-ax=-b/-c"] = ["type2FractionE", "-", "-", "-ax=-b/-c"];

typesError["x+b=c"] = ["type3E", "+"];
typesError["x+b=-c"] = ["type3E", "+"];
typesError["-x+b=c"] = ["type3E", "+"];
typesError["-x+b=-c"] = ["type3E", "+"];
typesError["ax+b=c"] = ["type3E", "+"];
typesError["ax+b=-c"] = ["type3E", "+"];
typesError["-ax+b=c"] = ["type3E", "+"];
typesError[-"ax+b=-c"] = ["type3E", "+"];

typesError["x-b=c"] = ["type3E", "-"];
typesError["x-b=-c"] = ["type3E", "-"];
typesError["-x-b=c"] = ["type3E", "-"];
typesError["-x-b=-c"] = ["type3E", "-"];
typesError["ax-b=c"] = ["type3E", "-"];
typesError["ax-b=-c"] = ["type3E", "-"];
typesError["-ax-b=c"] = ["type3E", "-"];
typesError[-"ax-b=-c"] = ["type3E", "-"];

typesError["ax=c"] = ["type4E"];
typesError["ax=-c"] = ["type4E"];
typesError["-ax=c"] = ["type4E"];
typesError["-ax=-c"] = ["type4E"];

typesError["x/b=c"] = ["type5E"];
typesError["x/b=-c"] = ["type5E"];
typesError["-x/b=c"] = ["type5E"];
typesError["-x/b=-c"] = ["type5E"];
typesError["ax/b=c"] = ["type5E"];
typesError["ax/b=-c"] = ["type5E"];
typesError["-ax/b=c"] = ["type5E"];
typesError["-ax/b=-c"] = ["type5E"];

typesError["many terms"] = ["type6E"];

typesError["parentheses with plus sign"] = ["type7E", "+(", "parentheses with plus sign"];
typesError["parentheses with minus sign"] = ["type7E", "-(", "parentheses with minus sign"];

typesError["distributive property"] = ["type7E", "*(", "distributive property"];

typesError["reason and proportion"] = ["type7E", "/", "reason and proportion"];

typesError["fractions with simple numerator"] = ["type7E", "/", "fractions"];
typesError["fractions with compound numerator"] = ["type7E", "/", "fractions"];
typesError["fractions with distributive property"] = ["type7E", "/", "fractions"];

//Para equacoes do tipo -x=c
function type1E() {
	return errorFeedbacksTXT["-x=c"]["x=c"];
}

//Para equacoes do tipo +-ax=b[operador]c com resultado inteiro
function type2E(currentStep, previousStep, operator, signB, signC, formatEquation) {
	var text;
	var expression = previousStep.split("=")[1];
	var correctAnswer = eval(expression);
	var split = expression.split(operator);
	var b;
	var c;
	
	//b nao eh negativo
	if (split.length === 2) {
		b = split[0];
		c = split[1];
	}
	
	//b eh negativo
	else {
		b = "-" + split[1];
		c = split[2];
	}
	
	if (signB === "" && signC === "") {
		text = errorFeedbacksTXT[formatEquation];
		checkErrorLevel(1);
		previousTypeError = 1;
	}
	
	else {
		var answer = parseInt(currentStep.split("=")[1]);
		
		if (Math.abs(answer) === Math.abs(correctAnswer)) {
			if (operator === "+" && correctAnswer > 0) {
				text = errorFeedbacksTXT[formatEquation][2];
				checkErrorLevel(2);
				previousTypeError = 2;
			}
			
			else {
				text = errorFeedbacksTXT[formatEquation][0];
				checkErrorLevel(3);
				previousTypeError = 3;
				
				//Adaptacao no texto quando fala que os dois termos sao negativos
				if (operator === "-" && signB === "-") {
					var textSplit = text.split(".");
					text = textSplit[0] + "." + textSplit[1].replace("-", "");
				}
			}
		}
		
		else if (operator !== "*" && answer === eval(expression.substring(1))) {
			text = errorFeedbacksTXT[formatEquation][1];		
			checkErrorLevel(4);
			previousTypeError = 4;
		}
		
		else if (operator === "+") {
			text = errorFeedbacksTXT[formatEquation][3];
			checkErrorLevel(5);
			previousTypeError = 5;
		}
		
		else if (operator === "-") {
			text = errorFeedbacksTXT[formatEquation][2];
			checkErrorLevel(6);
			previousTypeError = 6;
		}
		
		else {
			text = errorFeedbacksTXT[formatEquation][1];
			checkErrorLevel(7);
			previousTypeError = 7;
		}
	}
	
	text = replaceAll(text, "[b]", b);
	text = replaceAll(text, "[c]", c);
	
	
	return text;
}

//Para equacoes do tipo +-ax=+-b/+-c
function type2FractionE(currentStep, previousStep, signB, signC, formatEquation) {
	var text;
	var answer = currentStep.split("=")[1];
	var isInteger = $.isNumeric(answer);
	
	if ((signB === "" && signC === "") || !isInteger) {
		if (isInteger) {
			text = errorFeedbacksTXT["x=b/c"][0];
			checkErrorLevel(8);
			previousTypeError = 8;
		}
		
		else {
			var splitCurrent = currentStep.split("/");
			var splitPrevious = previousStep.split("/");
			
			var numerator = parseInt(splitPrevious[0]);
			var denominator = parseInt(splitPrevious[1]);
			var numeratorAnswer = parseInt(splitCurrent[0]);
			var denominatorAnswer = parseInt(splitCurrent[1]);
			
			var divisorNumerator = numerator / numeratorAnswer;		
			var divisorDenominator = denominator / denominatorAnswer;
			
			if (divisorNumerator !== divisorDenominator) {
				text = errorFeedbacksTXT["x=b/c"][4];
				checkErrorLevel(9);
				previousTypeError = 9;
			}
					
			else if (divisorNumerator % 1 !== 0 && divisorDenominator % 1 !== 0) {
				text = errorFeedbacksTXT["x=b/c"][3];
				checkErrorLevel(10);
				previousTypeError = 10;
			}

			else if (divisorDenominator % 1 !== 0) {
				text = errorFeedbacksTXT["x=b/c"][2];
				checkErrorLevel(11);
				previousTypeError = 11;
			}

			else {
				text = errorFeedbacksTXT["x=b/c"][1];
				checkErrorLevel(12);
				previousTypeError = 12;
			}
		}
	}
	
	else {
		var expression = previousStep.split("=")[1];
		var correctAnswer = eval(expression);
		var split = expression.split("/");
		var b = split[0];
		var c = split[1];
		
		if (Math.abs(answer) === Math.abs(correctAnswer)) {
			text = errorFeedbacksTXT[formatEquation][0];
			checkErrorLevel(13);
			previousTypeError = 13;
		}
		
		else {
			text = errorFeedbacksTXT[formatEquation][1];
			checkErrorLevel(14);
			previousTypeError = 14;
		}
		
		text = replaceAll(text, "[b]", b);
		text = replaceAll(text, "[c]", c);
	}
	
	return text;
}

//Para equacoes do tipo +-ax+-b=+-c
function type3E(currentStep, previousStep, operator) {
	var text;
	var bLeftSide = findTermFromStart(previousStep, false).toString;
	var bRightSide;
	
	if (bLeftSide[0] === "+") {
		bRightSide = bLeftSide.replace("+", "-");
	}
	
	else {
		bRightSide = bLeftSide.replace("-", "+");
	}
	
	var correctAnswer = previousStep.replace(bLeftSide, "") + bRightSide;
	
	//Troca o termo errado do aluno pelo termo correto para verificar se foi somente isso que ele errou
	if (replace(currentStep, bLeftSide, bRightSide, currentStep.indexOf("=") + 1) === correctAnswer) {
		text = errorFeedbacksTXT["x" + operator + "b=c"]["x=c" + operator + "b"];
		text = text.replace("[b]", bLeftSide);
		text = text.replace("[c]", previousStep.split("=")[1]);
		checkErrorLevel(15);
		previousTypeError = 15;
	}
	
	else {
		text = typeGeneral();
	}
	
	return text;
}

//Para equacoes do tipo +-ax=+-c
function type4E(currentStep, previousStep) {
	var text;
	var split = previousStep.split("=");
	var a = split[0].replace("x", "");
	
	var correctAnswer = previousStep.replace(a, "") + "/" + a; 
	
	currentStep = replaceAll(currentStep, "(", "");
	currentStep = replaceAll(currentStep, ")", "");

	if (currentStep.indexOf("*") !== -1) {
		if (currentStep.replace("*", "/") === correctAnswer) {
			text = errorFeedbacksTXT["ax=c"]["x=c*a"];
			checkErrorLevel(16);
			previousTypeError = 16;
		}
		
		else {
			var checkStep;
			
			if (a[0] === "-") {
				checkStep = currentStep.replace("*", "/-");
			}
			
			else {
				checkStep = currentStep.replace("*-", "/");
			}
			
			if (checkStep === correctAnswer) {
				text = errorFeedbacksTXT["ax=c"]["x=c*-a"];
				checkErrorLevel(17);
				previousTypeError = 17;
			}
			
			else {
				text = typeGeneral();
			}
		}
	}
	
	else {
		var checkStep;
		
		if (a[0] === "-") {
			checkStep = currentStep.replace("/", "/-");
		}
		
		else {
			checkStep = replace(currentStep, "-", "", currentStep.indexOf("=") + 1);
		}
		
		if (checkStep === correctAnswer) {
			text = errorFeedbacksTXT["ax=c"]["x=c/-a"];
			checkErrorLevel(18);
			previousTypeError = 18;
		}
		
		else {
			text = typeGeneral();
		}
	}
	
	if (text.indexOf("[a]") !== -1) {
		text = replaceAll(text, "[a]", a);
		text = replaceAll(text, "[c]", split[1]);
	}
	
	return text;
}

//Para equacoes do tipo +-ax/b=+-c
function type5E(currentStep, previousStep) {
	var text;
	
	previousStep = replaceAll(previousStep, "(", "");
	previousStep = replaceAll(previousStep, ")", "");
	currentStep = replaceAll(currentStep, "(", "");
	currentStep = replaceAll(currentStep, ")", "");
	
	var split = previousStep.split("=");
	var b = split[0].split("/")[1];
	var correctAnswer = previousStep.replace("/" + b, "") + "*";
	var formatEquation;
	
	if (previousStep[0] !== "-") {
		correctAnswer += b; 
		formatEquation = "x/b=c";
	}
	
	else {
		correctAnswer += "-" + b;
		formatEquation = "-x/b=c";
	}
	
	if (currentStep.indexOf("/") !== -1) {
		if (formatEquation[0] !== "-") {
			if (currentStep.replace("/", "*") === correctAnswer) {
				text = errorFeedbacksTXT[formatEquation]["x=c/b"];
				checkErrorLevel(19);
				previousTypeError = 19;
			}
			
			else if (currentStep.replace("/-", "*") === correctAnswer) {
				text = errorFeedbacksTXT[formatEquation]["x=c/-b"];
				checkErrorLevel(20);
				previousTypeError = 20;
			}
				
			else {
				text = typeGeneral();
			}	
		}
		
		else {
			var checkStep = currentStep.replace("/", "*");
			var checkAnswer = correctAnswer.replace("-", "");
		
			if ((currentStep[0] !== "-" && checkStep === correctAnswer) || (currentStep[0] === "-" && checkStep === checkAnswer)) {
				text = errorFeedbacksTXT[formatEquation]["x=c/-b"];
				checkErrorLevel(21);
				previousTypeError = 21;
			}
			
			else if ((currentStep.replace("/-", "*") === correctAnswer) || (currentStep[0] === "-" && checkStep === checkAnswer)) {
				text = errorFeedbacksTXT[formatEquation]["x=c/b"];
				checkErrorLevel(22);
				previousTypeError = 22;
			}
				
			else {
				text = typeGeneral();
			}	
		}
	}
	
	else {
		var checkStep;
		
		if (formatEquation[0] === "-") {
			checkStep = currentStep.replace("*", "*-");
		}
		
		else {
			checkStep = replace(currentStep, "-", "", currentStep.indexOf("=") + 1);
		}
		
		if (checkStep === correctAnswer) {
			text = errorFeedbacks_ptBR["x/b=c"]["x=c*-b"];
			checkErrorLevel(23);
			previousTypeError = 23;
		}
		
		else {
			text = typeGeneral();
		}
	}
	
	if (text.indexOf("[b]") !== -1) {
		text = replaceAll(text, "[b]", b);
		text = replaceAll(text, "[c]", split[1]);
	}
	return text;
}

//Varios termos
function type6E(currentStep, previousStep) {
	var text;
	var termsWithX = findTermsFromStart(previousStep, true, 42);
	var termsWithoutX = findTermsFromStart(previousStep, false, 42);
	var splitP = previousStep.split("=");
	var splitC = currentStep.split("=");
	
	for (var i = 0; i < termsWithX.length; i++) {
		var withX = termsWithX[i].toString;
		
		for (var j = 0; j < termsWithoutX.length; j++) {	
			var withoutX = termsWithoutX[j].toString;		
			var result = eval(withX.replace("x", "") + withoutX);

			if ((splitP[0].indexOf(withX) !== -1 && splitP[0].indexOf(withoutX) !== -1 && (splitC[0].indexOf(result) !== -1 || splitC[0].indexOf(result + "x") !== -1)) ||
				(splitP[1].indexOf(withX) !== -1 && splitP[1].indexOf(withoutX) !== -1 && (splitC[1].indexOf(result) !== -1 || splitC[1].indexOf(result + "x") !== -1))) {
				text = errorFeedbacksTXT["many terms"];
				text = text.replace("[a]", withX);
				text = text.replace("[b]", withoutX);
				checkErrorLevel(24);
				previousTypeError = 24;
				
				//Para sair do for
				i = termsWithX.length;
				j = termsWithoutX.length;
			}
		}
	}
	
	if (text === undefined) {
		text = typeGeneral();
	}
	
	return text;
}

//Parenteses, propriedade distributiva, razao e proporcao ou fracoes
function type7E(currentStep, previousStep, paramIndexOf, paramErrorFeedback) {
	var text;
	
	if (previousStep.indexOf(paramIndexOf) !== -1 && currentStep.indexOf(paramIndexOf) === -1) {
		text = errorFeedbacksTXT[paramErrorFeedback];
		
		if (paramErrorFeedback.indexOf("parentheses")) {
			checkErrorLevel(25);
			previousTypeError = 25;
		}
		
		else if (paramErrorFeedback === "distributive property") {
			checkErrorLevel(26);
			previousTypeError = 26;
		}
		
		else if (paramErrorFeedback === "reason and proportion") {
			checkErrorLevel(27);
			previousTypeError = 27;
		}
		
		else {
			checkErrorLevel(28);
			previousTypeError = 28;
		}
	}
	
	else {
		text = typeGeneral();
	}
	
	return text;
}

//Se nao se enquadra em nenhum dos tipos anteriores
function typeGeneral() {
	checkErrorLevel(29);
	previousTypeError = 29;
	
	return errorFeedbacksTXT["general"];
}

function feedbackWithHint(formatEquation, currentStep) {
	checkErrorLevel(30);
	levelHint = levelError + 2;
	return selectHint(formatEquation, currentStep);
}

//O nivel do erro so deve ser aumentado se o aluno cometeu o mesmo erro duas vezes seguidas. Por exemplo, na equacao x+7=10, o aluno poderia escrever duas vezes seguidas
//x=10+7
function checkErrorLevel(currentTypeError) {
	if (levelError < 3 && previousTypeError === currentTypeError) {
		levelError++;
	}
}

////Parenteses 
//function type7(currentStep, previousStep, symbolSign, nameSign) {
//	var text;
//	
//	if (previousStep.indexOf(symbolSign + "(") !== -1 && currentStep.indexOf(symbolSign + "(") === -1) {
//		text = errorFeedbacksTXT["parentheses with " + nameSign];
//	}
//	
//	else {
//		text = typeGeneral();
//	}
//	
//	return text;
//}
//
////Propriedade Distributiva
//function type8(currentStep, previousStep) {
//	var text;
//	
//	if (previousStep.indexOf("*(") !== -1 && currentStep.indexOf("*(") === -1) {
//		text = errorFeedbacksTXT["distributive property"];
//	}
//	
//	else {
//		text = typeGeneral();
//	}
//	
//	return text;
//}
//
////Fracoes
//function type9(currentStep, previousStep) {
//	var text;
//	
//	if (previousStep.indexOf("/") !== -1 && currentStep.indexOf("/") === -1) {
//		text = errorFeedbacksTXT["fractions"];
//	}
//	
//	else {
//		text = typeGeneral();
//	}
//	
//	return text;
//}