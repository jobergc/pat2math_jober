//Só falta tratar equações com mais de uma fração, e também tratar os casos em que a equação é mais
//complexa que ax + b = c e tem frações e equações que possuam parênteses dentro de parênteses
//Cuidar que a resposta final da equação deverá ser igual à gerada por este método, que será um String no caso de
//fração
function solveEquation(equation) {
	var eq = equation;
	var format = checkFormat(eq);
	
	if (format.indexOf("x") !== -1) {
		var sides = equation.split("=");
		
		if (format.indexOf("/") === -1 && (format[0] === "x" || format.indexOf("-x").indexOf !== -1 || format.indexOf("ax"))) {
			var a, b, c;
			
			var terms = sides[0].split("x");
			
			if (terms[0] === "") {
				a = 1;
			}
			
			else if (terms[0] === "-") {
				a = -1;
			}
			
			else {
				a = parseInt(terms[0]);
			}
			
			if (terms[1] === "") {
				b = 0;
			}
			
			else {
				b = parseInt(terms[1]);
			}
			
			c = parseInt(sides[1]);
			
			var answer = c - b;
			
			if (a % 1 === 0) {
				answer /= a;
			}
			
			else {
				answer = simplify(answer, a);
			}
			
			return answer;
		}
	
		else {
			var denominator = sides[0].split(")/(");
			denominator = denominator[1];
			denominator = denominator.replace(")", "");
			denominator = parseInt(denominator);
			
			if (equation[0] === "-") {
				denominator *= -1;
			}
			
			var numberRightSide = parseInt(sides[1]);
			var answer = numberRightSide * denominator;
			
			return answer;
		}
	}
	
	else {
		if (format === "reason and proportion") {
			eq = checkReasonAndProportion(equation);
			eq = checkParentheses(eq);
		}
		
		else if (format.indexOf("parentheses") !== -1 || format === "distributive property") {
			eq = checkParentheses(equation);
		}
		
		else if (format.indexOf("fraction") !== -1) {
			eq = checkFractionsForSolve(equation);
			eq = checkParentheses(eq);
		}
			
		eq = checkXWithoutExplicitCoefficient(eq);
		eq = splitTermsXAndNumbers(eq);
		
		eq[0] = replaceAll(eq[0], "x", "");
		eq[0] = replaceAll(eq[0], "++", "+1+");
		eq[0] = replaceAll(eq[0], "--", "-1-");
		
		var leftSide = eval(eq[0]);
		var rightSide = eval(eq[1]);
		var answer = rightSide / leftSide;
		
		if (answer % 1 !== 0) {
			answer = simplify(rightSide, leftSide);
		}
		
		return answer;	
	}
}