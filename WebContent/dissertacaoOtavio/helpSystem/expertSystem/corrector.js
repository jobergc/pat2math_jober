function correctStep(step) {
	step = verifyMultiplicationsInX(step);
	
	var finalAnswer = finalAnswerCurrentEquation;
	
	if (typeof finalAnswer === "string") {
		finalAnswer = eval(finalAnswer);
	}
	
	var verifyEquality = replaceAll(step, "x", finalAnswer);
	var ve = verifyEquality.split("=");
	
	var left = replaceAll(ve[0], "--", "+");
	left = eval(left);
	var right = replaceAll(ve[1], "--", "+"); 
	right = eval(right);
	
	return equalsNumbers(left, right);
}

function checkEquation() {
	if (isLoadEquation)
		isLoadEquation = false;

		$(selectedSheet + " .canMove li input").blur();

		var currentStep = naturalToText(selectedEquation.currentStep);
		currentStep = replaceAllMultiplications(currentStep);
		
		var validation = validateInput(currentStep);
		

	if (validation !== null) {
		showFeedbackError(validation);
	}

	else {
		if (currentStep === "")
			currentStep = " ";

		else if (currentStep.indexOf(".") !== -1 || currentStep.indexOf(",") !== -1)
			alert(indexTXT[61]);

		var previousStep = selectedEquation.lastStep;

		if (previousStep !== null) {
			previousStep = previousStep.step;
		} else {
			previousStep = selectedEquation.initialEquation;
		}

		if (currentStep.indexOf("d") !== -1 && previousStep.indexOf("d") === -1
				|| previousStep === "" || previousStep === null) {
			previousStep = selectedEquation.initialEquation;
		}

		var isCorrect = correctStep(currentStep);
		var split = currentStep.split("=");
		var isFinalAnswer = isCorrect && split[0] === "x" && !isExpression(split[1]);
		
		if (isFinalAnswer) {
			var isFraction = split[1].indexOf("/") !== -1;
			
			if (isFraction) {
				var fraction = replaceAll(split[1], "(", "");
				fraction = replaceAll(fraction, ")", "");

				var splitFraction = fraction.split("/");
				var numerator = parseInt(splitFraction[0]);
				var denominator = parseInt(splitFraction[1]);

				isFinalAnswer = numerator !== 0 && isIrreducible(numerator, denominator);
			}

			else {
				isFinalAnswer = $.isNumeric(split[1]);
			}
		}
		
		if (!isPAT2Exam) {
			if (isCorrect) {
				levelHint = 1;
				levelError = 1;
				registerHit();
			}
			
			if (!isCorrect) {
				errorFeedback(currentStep);
			}
	
			if (window.location.href.indexOf("knowledgeTest") === -1) {
				saveEquationProgress(previousStep, currentStep, isCorrect, isFinalAnswer, $(selectedSheet + " #button"));
			}
	
			else {
				requestServerKnowledgeTest('e', previousStep, currentStep, "OG", $(selectedSheet + " #button"));
			}
		}
		
		else {
			checkEquationExam(currentStep, isCorrect, isFinalAnswer, previousStep, $(selectedSheet + " #button"));
		}
	}
}

function validateInput(input) {
    if (usingHandwritten) {
    	replaceInputForHandWritten();
    }
    
    if (input === "" || input === null) {
    	validadeInputAux();
    	document.getElementById('inputMobile').style.border = "1px solid red";
    	return typingErrorsTXT[0];
    }
    
    else if (input.indexOf("x") === -1) {
    	validadeInputAux();
    	document.getElementById('inputMobile').style.border = "1px solid red";
    	return typingErrorsTXT[1];
    }
    
    else if (input.indexOf("=") === -1) {
    	validadeInputAux();
    	document.getElementById('inputMobile').style.border = "1px solid red";
    	return typingErrorsTXT[2];
    }
    
    else if (input.indexOf(".") !== -1 || input.indexOf(",") !== -1) {
    	validadeInputAux();
    	document.getElementById('inputMobile').style.border = "1px solid red";
    	return typingErrorsTXT[3];
    }
    
    else if (selectedEquation.equation === input || searchStep(input) !== -1) {    
    	validadeInputAux();
    	document.getElementById('inputMobile').style.border = "1px solid red";
    	
		if (selectedEquation.equation === input) {
			return typingErrorsTXT[4];
		}
		
		else {
			return typingErrorsTXT[5];
		}
	}
    
    else {
    	var textWithoutX = replaceAll(input, "x", "1");
    	var split = textWithoutX.split("=");
    	
    	if (split.length > 2) {
    		validadeInputAux();
    		document.getElementById('inputMobile').style.border = "1px solid red";
    		return typingErrorsTXT[6];
    	}
    	
    	else {
    		var sides = ["esquerdo", "direito"]
    		for (var i = 0; i < split.length; i++) {
    			if (split[i] === "") {
    				validadeInputAux();
    				document.getElementById('inputMobile').style.border = "1px solid red";
    				return typingErrorsTXT[6+i];
    			}
    			
    			else {
	    			try {
	    				eval(split[i]);
	    			} catch (e) {
	    				validadeInputAux();
	    				document.getElementById('inputMobile').style.border = "1px solid red";
	    				return typingErrorsTXT[9];
	    			}
    			}
    		}
    		
    		return null;
    	}
    }
}

function validadeInputAux() {
	clearLine();
    $(selectedSheet + " .labelDefault input").attr("value", selectedEquation.currentStep);
    $(selectedSheet + " .labelDefault input").css("width", (selectedEquation.currentStep.length + 1) * 16 + "px");
    centralizeCanMoveAndButton();
    focus();
}