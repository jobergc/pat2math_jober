function simplify(numerator, denominator) {
    if (isIrreducible(numerator, denominator))
        return numerator + "/" + denominator;
    
    var n = numerator;
    var d = denominator;
    var currentPrimeNumber = 2;
    var i = 1;
    
    do {
        if (n % currentPrimeNumber === 0 && d % currentPrimeNumber === 0) {
            n /= currentPrimeNumber;
            d /= currentPrimeNumber;
        }
        
        else {
            currentPrimeNumber = primeNumbers[i];
            i++;
        }
    } while (currentPrimeNumber <= n && currentPrimeNumber <= d);
    
    return n + "/" + d;
}

function isIrreducible(numerator, denominator) {
    for (var i = 0; i < primeNumbers.length; i++) {
        if (Math.abs(numerator) < primeNumbers[i] || Math.abs(denominator) < primeNumbers[i])
            return true;
        
        else if (numerator % primeNumbers[i] === 0 && denominator % primeNumbers[i] === 0)
            return false;
    }
    
    return true;
}

function isSimpleFraction(term) {
	var split = term.split(")/(");
	
	if (split.length === 1) {
		return false;
	}
	
	else {
		split[0] = split[0].replace("(", "");
		split[0] = split[0].replace("x", "");
		
		if (split[0] === "") {
			split[0] = "1";
		}
		
		else if (split[0] === "-") {
			split[0] = "-1";
		}
		
		split[1] = split[1].replace(")", "");
		split[1] = split[1].replace("x", "");
		
		return $.isNumeric(split[0]) && $.isNumeric(split[1]);
	}
}