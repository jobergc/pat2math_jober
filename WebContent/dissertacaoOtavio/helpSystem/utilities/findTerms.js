function findTermWithX(expression, start) {
	if (start >= expression.length) {
		return null;
	}
	
	var posEnd = expression.indexOf("x", start);
	
	if (posEnd !== -1) {
		var posStart = posEnd;
		var term = "x";
		
		//Caso especial em que a equacao comeca com "x"
		if (posEnd === 0) {
			var position = new Position(posStart, posEnd);
			var result = new Term(term, position);
				
			return result;
		}
		
		else {
			for (var i = posEnd - 1; i >= 0; i--) {
				if (expression[i] === "(") {
					var posParentheses = expression.indexOf(")", i);
					
					return findTermWithX(expression, posParentheses + 1);
				}
				
				if (expression[i] !== "*" && expression[i] !== "=") {
					posStart--;
					term = expression[i] + term;	
				}
					
				if (isOperator(expression[i]) || i === 0) {						
					if (i > 0 && expression[i-1] === "(") {
						var posParentheses = expression.indexOf(")", i);
						var termInParentheses = expression.substring(i + 1, posParentheses);
						var temp = termInParentheses.replace("x", "");
						
						if ($.isNumeric(temp)) {
							var position = new Position(posStart - 1, posEnd + 1);
							term = "(" + term + ")";
							var result = new Term(term, position);
						}
					}
					
					var closedParenthesis = expression.indexOf(")", posEnd);
					var openParenthesis = expression.indexOf("(", posEnd);
						
					if (openParenthesis > closedParenthesis) {
							return findTermWithX(expression, posEnd + 1);
					}
					
					var position = new Position(posStart, posEnd);	
					var result = new Term(term, position);
					
					return result;
				}
			}
		}
	}
	
	else {
		//Se chegou ate aqui, eh porque nao encontrou o termo com os parametros passados
		return null;
	}
}

//So falta tratar o caso de termos entre parenteses tipo (-1) nesta funcao, a que inclui o X ja tratei
function findTermWithoutX(expression, start) {
	if (start >= expression.length) {
		return null;
	}
	
	else if (start === expression.length - 1 && expression[start] !== "x") {
		var position = new Position(start, start);
		var result = new Term(expression[start], position);
		
		return result;
	}
	
	var term;
	
	if (expression[start] !== "*") {
		term = expression[start];
	}
	
	else {
		term = expression[++start];
	}
	
	var posStart = start;
	var posEnd = posStart;
		
	var i = null;
	
	//Caso especial em que a equacao comeca com "x"
	if (term === "x") {
		if (start === expression.length - 1) {
			return null;
		}
		
		if (expression[start+1] !== "=") {
			term = expression[start+1];
			i = start + 2;
		}
		
		else {
			term = expression[start+2];
			i = start + 3
			
			if (i >= expression.length) {
				var position = new Position(start, start);
				var result = new Term(expression[start], position);
			
				return result;
			}
		}
		
		posStart = i - 1;
		posEnd = posStart;
	}
	
	if (start === expression.length - 1) {
		var position = new Position(posStart, posEnd);
		var result = new Term(term, position);
		
		return result;
	}
	
	
	if (term !== "(" && i === null) {
		i = start + 1
	}
	
	else if (i === null) {
		var posFinalParenthesis = expression.indexOf(")/", posEnd);
		
		//Eh uma fracao
		if (posFinalParenthesis !== -1) {
			posFinalParenthesis = expression.indexOf(")", posFinalParenthesis + 3);
			posStart = posFinalParenthesis + 1;
			posEnd = posStart;
			i = posEnd;
			term = expression[i];
		}
		
		else {
			posFinalParenthesis = expression.indexOf(")", start);
			var termInParentheses = expression.substring(start + 1, posFinalParenthesis);
			
			if ($.isNumeric(termInParentheses)) {
				term = "(" + termInParentheses + ")";
				var position = new Position(start, posFinalParenthesis);
				var result = new Term(term, position);
				
				return result;
			}
			
			else {
				posStart = posFinalParenthesis + 1;
				posEnd = posStart;
				i = posEnd;
				term = expression[i];
			}
		}
	}
	
	for (; i < expression.length; i++) {
		if ((isOperator(expression[i]) && expression[i] !== "=") || expression[i] === "x") {
			if (expression[i] === "x") {
				if (i < expression.length - 1) {
					term = "";
					posStart = i + 1;
					posEnd = posStart;
				}
				
				else {
					return null;
				}
			}
			
			else if (term !== "" && !isOperator(term) && expression[i+1] !== "(") {
				var position = new Position(posStart, posEnd);
				var result = new Term(term, position);
					
				return result;
			}
			
			else if (isOperator(term)) {
				posEnd++;
			}
			
			else if (expression[i] !== "*"){
				term += expression[i];				
			}
		}
		
		else if (expression[i] === "(") {
			var posFinalParenthesis = expression.indexOf(")/", i + 1);
			
			//Eh uma fracao
			if (posFinalParenthesis !== -1) {
				posFinalParenthesis = expression.indexOf(")", posFinalParenthesis + 3);
				posStart = posFinalParenthesis + 1;
				posEnd = posStart;
				i = posEnd;
				term = expression[i];
			}
			
			else {
				posFinalParenthesis = expression.indexOf(")", i + 1);
				var termInParentheses = expression.substring(i + 1, posFinalParenthesis);
				
				if ($.isNumeric(termInParentheses)) {
					term = "(" + termInParentheses + ")";
					var position = new Position(i, posFinalParenthesis);
					var result = new Term(term, position);
					
					return result;
				}
				
				else {
					posStart = posFinalParenthesis + 1;
					posEnd = posStart;
					i = posEnd;
					term = expression[i];
				}
			}
		}
		
		else if (expression[i] === "=") {
			var position = new Position(posStart, posEnd);
			var result = new Term(term, position);
				
			return result;
		}
		
		else {
			term += expression[i];
			posEnd++;
			
			if (i === expression.length - 1) {
				var position = new Position(posStart, posEnd);
				var result = new Term(term, position);
				
				return result;
			}
		}
	}
	
	return null;
}

//equation eh a representacao em String da equacao
//hasX eh um boolean que indica se o termo eh em funcao de X
//start eh a posicao em que a pesquisa iniciara
//Retorna um objeto do tipo Term (nao pode ser fracao ou propriedade distributiva)
function findTerm(expression, hasX, start) {
	if (start >= expression.length) {
		return null;
	}
	
	else if (hasX) {
		return findTermWithX(expression, start);
	}
		
	else {
		return findTermWithoutX(expression, start);
	}
}

//Sobrecarga que comeca a pesquisa sempre desde o inicio
function findTermFromStart(expression, hasX) {
	return findTerm(expression, hasX, 0);
}

//Permite indicar o lado da equacao em um String
//Para referenciar o lado direito, escrever "right", "r", "direito" ou "d"
//Para referenciar o lado esquerdo, escrecer "left", "l", "esquerdo" ou "e"
function findTermOnSide(expression, hasX, start, side) {
	var sides = expression.split("=");
	
	if (side === "right" || side === "r" || side === "direito" || side === "d") {
		var result = findTerm(sides[1], hasX, start);
		
		if (result !== null) {
			//Ajustes ns posicao para considerar os indices do lado esquerdo tambem
			result.position.start += sides[0].length + 1;
			result.position.end += sides[0].length + 1;
		}
		
		return result;
	}
	
	else {
		return findTerm(sides[0], hasX, start);
	}
}

//Sobrecarga que comeca a pesquisa sempre desde o inicio
function findTermOnSideFromStart(expression, hasX, side) {
	return findTermOnSide(expression, hasX, 0, side);
}

function findFractionalTerm(expression, hasX, start) {
	if (start >= expression.length) {
		return null;
	}
	
	var med = expression.indexOf("/", start);
	
	if (med === -1) {
		//Se chegou aqui nao ha fracoes
		return null;
	}
	
	var endNumerator = med - 1;
	var startDenominator = med + 1;
	var endDenominator = expression.indexOf(")", startDenominator);
	
	var openedParentheses = 1;
	var i = endNumerator - 1;
	
	for (; openedParentheses > 0; i--) {
		if (expression[i] === "(") {
			openedParentheses--;
		}
		
		else if (expression[i] === ")") {
			openedParentheses++;
		}
	}
	
	var startNumerator = i + 1;
	
	if (expression[startNumerator-1] === "-") {
		startNumerator--;
	}
	
	var fraction = expression.substring(startNumerator, endDenominator + 1);
	var fractionHasX = fraction.indexOf("x") !== -1;
	
	if (hasX === fractionHasX) {
		var position = new Position(startNumerator, endDenominator);
		var result = new FractionalTerm(fraction, position);
		
		return result;
	}
	
	else {
		return findFractionalTerm(expression, hasX, endDenominator + 1);
	}
	
}

//Sobrecarga que comeca a pesquisa sempre desde o inicio
function findFractionalTermFromStart(expression, hasX) {
	return findFractionalTerm(expression, hasX, 0);
}

function findFractionalTermOnSide(expression, hasX, start, side) {
	var sides = expression.split("=");
	
	if (side === "right" || side === "r" || side === "direito" || side === "d") {
		var result = findFractionalTerm(sides[1], hasX, start);
		
		if (result !== null) {
			//Ajustes ns posicao para considerar os indices do lado esquerdo tambem
			result.position.start += sides[0].length + 1;
			result.position.end += sides[0].length + 1;
		}
		
		return result;
	}
	
	else {
		return findFractionalTerm(sides[0], hasX, start);
	}
}

//Sobrecarga que comeca a pesquisa sempre desde o inicio
function findFractionalTermOnSideFromStart(expression, hasX, side) {
	return findFractionalTermOnSide(expression, hasX, 0, side);
}

function findTermWithDistributiveProperty(expression, start) {
	if (start >= expression.length) {
		return null;
	}
	
	var fractions = [findFractionalTerm(expression, true, start), findFractionalTerm(expression, false, start)];
	
	var posStart = expression.indexOf("*(", start);
	
	if (posStart === -1) {
		return null;
	}
	
	else {
		var posEnd = expression.indexOf(")", posStart);
		var insideWithParentheses = expression.substring(posStart + 1, posEnd + 1);

		var i = posStart - 1;
		
		var multiplier = expression[i];
		i--;
		
		for (; i >= 0; i--) {
			if (isNumber(expression[i])) {
				multiplier = expression[i] + multiplier;
			}
			
			else {
				if (expression[i] === "-") {
					multiplier = "-" + multiplier;
				}
				
				posStart = i + 1;
				break;
			}
		}
		
		var distributiveProperty = multiplier + "*";
		
		distributiveProperty += insideWithParentheses;
		
		var position = new Position(posStart, posEnd);
		var term = new TermWithDistributiveProperty(distributiveProperty, position);
		
		var isFractionTerm = false;
		var endFraction;
		
		for (var i = 0; i < fractions.length; i++) {
			if (fractions[i] !== null) {
				if (term.position.start > fractions[i].position.start && term.position.end < fractions[i].position.end) {
					isFractionTerm = true;
					endFraction = fractions[i].position.end;
					break;
				}
			}
		}
		
		if (!isFractionTerm) {
			return term;
		}
		
		else {
			return findTermWithDistributiveProperty(expression, endFraction + 1);
		}
	}
}

//Sobrecarga que comeca a pesquisa sempre desde o inicio
function findTermWithDistributivePropertyFromStart(expression) {
	return findTermWithDistributiveProperty(expression, 0);
}

function findTermWithDistributivePropertyOnSide(expression, start, side) {
	var sides = expression.split("=");
	
	if (side === "right" || side === "r" || side === "direito" || side === "d") {
		var result = findTermWithDistributivePropertyOnSide(sides[1], hasX, start);
		
		if (result !== null) {
			//Ajustes ns posicao para considerar os indices do lado esquerdo tambem
			result.position.start += sides[0].length + 1;
			result.position.end += sides[0].length + 1;
		}
		
		return result;
	}
	
	else {
		return findTermWithDistributivePropertyOnSide(sides[0], hasX, start);
	}
}

//Sobrecarga que comeca a pesquisa sempre desde o inicio
function findTermWithDistributivePropertyOnSideFromStart(expression, side) {
	return findTermWithDistributivePropertyOnSide(expression, side, 0);
}

//Retorna uma expressao entre parenteses
function findExpressionInParentheses(expression, operator, start) {
	if (start >= expression.length) {
		return null;
	}
	
	var fractions = [findFractionalTerm(expression, true, start), findFractionalTerm(expression, false, start)];
	
	var posStart = expression.indexOf(operator + "(", start);
	
	if (posStart === -1) {
		return null;
	}
	
	else {
		var posEnd = expression.indexOf(")", posStart);
		var inside = expression.substring(posStart+2, posEnd);
		
		if (inside === "x" || inside === "-x" || $.isNumeric(inside.replace("x", ""))) {
			return null;
		}
		
		var insideWithParentheses = "(" + inside + ")";
		var expressionInParentheses = operator + insideWithParentheses;
		
		var position = new Position(posStart, posEnd);
		var term = new ExpressionInParentheses(expressionInParentheses, position);
		
		var isFractionTerm = false;
		var endFraction;
		
		for (var i = 0; i < fractions.length; i++) {
			if (fractions[i] !== null) {
				if (term.position.start > fractions[i].position.start && term.position.end < fractions[i].position.end) {
					isFractionTerm = true;
					endFraction = fractions[i].position.end;
					break;
				}
			}
		}
		
		if (!isFractionTerm) {
			return term;
		}
		
		else {
			return findExpressionInParentheses(expression, endFraction + 1);
		}
	}
}

function findExpressionInParenthesesFromStart(expression, operator) {
	return findExpressionInParentheses(expression, operator, 0);
}

function findExpressionInParenthesesOnSide(expression, operator, start, side) {
	var sides = expression.split("=");
	
	if (side === "right" || side === "r" || side === "direito" || side === "d") {
		var result = findExpressionInParentheses(sides[1], operator, start);
		
		if (result !== null) {
			//Ajustes ns posicao para considerar os indices do lado esquerdo tambem
			result.position.start += sides[0].length + 1;
			result.position.end += sides[0].length + 1;
		}
		
		return result;
	}
	
	else {
		return findExpressionInParentheses(sides[0], operator, start);
	}
}

function findExpressionInParenthesesOnSideFromStart(expression, operator, side) {
	return findExpressionInParenthesesOnSide(expression, operator, 0, side);
}

function findMultiplication(expression, start) {
	var posOperator = expression.indexOf("*", start);
	
	if (posOperator === -1) {
		return null;
	}
	
	else {
		var multiplication = findLeftTermInMultiplication(expression, posOperator);
		multiplication += "*" + findRightTermInMultiplication(expression, posOperator);
		
		if (multiplication.indexOf("distributive property") !== -1) {
			var split = multiplication.split("*");
			
			if (split.length > 2) {
				multiplication = multiplication.replace("*distributive property", "");
			}
			
			else {
				return null;
			}
				
		}
		
		var posStart = expression.indexOf(multiplication, start);
		var posEnd = expression.indexOf(multiplication[multiplication.length-1], posStart);
		var position = new Position(posStart, posEnd);
			
		var result = new CompoundTerm(multiplication, position);
			
		return result;
	}
}

function findLeftTermInMultiplication(expression, posOperator) {
	var term = expression[posOperator-1];
	
	if (term !== ")") {	
		for (var i = posOperator - 2; i >= 0; i--) {
			if (isNumberOrIncognita(expression[i]) || expression[i] === "-") {
				term = expression[i] + term;
				
				if (expression[i] === "-") {
					break;
				}
			}
			
			else {
				break;
			}
		}
	}
	
	else {
		var posParenthesisDenominator = expression.lastIndexOf("(", posOperator - 2);
		var posParenthesisNumerator = expression.lastIndexOf("(", posParenthesisDenominator - 3);
		term = expression.substring(posParenthesisNumerator, posOperator);
	}
	
	return term;
}

function findRightTermInMultiplication(expression, posOperator) {
	var term = expression[posOperator+1];
	var i; 
	
	if (term !== "(") {
		i = posOperator + 2
		
		for (; i < expression.length; i++) {
			if (isNumberOrIncognita(expression[i]) || expression[i] === ")") {
				term += expression[i];
				
				if (expression[i] === ")") {
					var temp = term.replace("(", "");
					temp = temp.replace(")", "");
					temp = temp.replace("x", "");
					
					if (!$.isNumeric(temp)) {
						term = "distributive property";
					}
				}
			}
			
			else {
				break;
			}
		}
	}
	
	else {
		if (expression.indexOf("/") !== -1) {
			var posParenthesisNumerator = expression.indexOf(")", posOperator + 2);
			var posParenthesisDenominator = expression.lastIndexOf(")", posParenthesisNumerator + 3);
			i = posParenthesisDenominator + 1;
			term = expression.substring(posOperator + 1, i);
		}
		
		else {
			var posParentheses = expression.indexOf(")", posOperator + 2);
			term = expression.substring(posOperator + 2, posParentheses);
			
			if (!$.isNumeric(term)) {
				term = "distributive property";
			}
		}
	}
	
	return term;
}

function findMultiplicationFromStart(expression) {
	return findMultiplication(expression, 0);
}

function findMultiplicationOnSide(expression, start, side) {
	var sides = expression.split("=");
	
	if (side === "right" || side === "r" || side === "direito" || side === "d") {
		var result = findMultiplication(sides[1], start);
		
		if (result !== null) {
			//Ajustes ns posicao para considerar os indices do lado esquerdo tambem
			result.position.start += sides[0].length + 1;
			result.position.end += sides[0].length + 1;
		}
		
		return result;
	}
	
	else {
		return findMultiplication(sides[0], start);
	}
}

function findMultiplicationOnSideFromStart(expression, side) {
	return findMultiplicationOnSide(expression, 0, side);
}

//Retorna um array de termos com o tamanho maximo de numTerms
function findTerms(expression, hasX, start, numTerms) {
	var result = new Array();
	var term = findTerm(expression, hasX, start);
	var newStart;
	
	if (term !== null) {
		newStart = term.position.end + 1;
	}
	
	while (term !== null && result.length < numTerms) {
		result.push(term);
		term = findTerm(expression, hasX, newStart);
		
		if (term !== null) {
			newStart = term.position.end + 1;
		}
	}
	
	return result;
}

function findTermsFromStart(expression, hasX, numTerms) {
	return findTerms(expression, hasX, 0, numTerms);
}

//Permite indicar o lado da equacao em um String
//Para referenciar o lado direito, escrever "right", "r", "direito" ou "d"
//Para referenciar o lado esquerdo, escrecer "left", "l", "esquerdo" ou "e"
function findTermsOnSide(expression, hasX, start, side, numTerms) {
	var sides = expression.split("=");
	
	if (side === "right" || side === "r" || side === "direito" || side === "d") {
		var result = findTerms(sides[1], hasX, start, numTerms);
		
		if (result !== null) {
			//Ajustes nas posicao para considerar os indices do lado esquerdo tambem
			for (var i = 0; i < result.length; i++) {
				result[i].position.start += sides[0].length + 1;
				result[i].position.end += sides[0].length + 1;
			}
		}
		
		return result;
	}
	
	else {
		return findTerms(sides[0], hasX, start, numTerms);
	}
}

//Sobrecarga que comeca a pesquisa sempre desde o inicio
function findTermsOnSideFromStart(expression, hasX, side, numTerms) {
	return findTermsOnSide(expression, hasX, 0, side, numTerms);
}

function findFractionalTerms(expression, hasX, start, numTerms) {
	var result = new Array();
	var term = findFractionalTerm(expression, hasX, start);
	var newStart;
	
	if (term !== null) {
		newStart = term.position.end + 1;
	}
	
	while (term !== null && result.length <= numTerms) {
		result.push(term);
		term = findFractionalTerm(expression, hasX, newStart);
		
		if (term !== null) {
			newStart = term.position.end + 1;
		}
	}
	
	return result;
}

//Sobrecarga que comeca a pesquisa sempre desde o inicio
function findFractionalTermsFromStart(expression, hasX, numTerms) {
	return findFractionalTerms(expression, hasX, 0, numTerms);
}

function findFractionalTermsOnSide(expression, hasX, start, side, numTerms) {
	var sides = expression.split("=");
	
	if (side === "right" || side === "r" || side === "direito" || side === "d") {
		var result = findFractionalTerms(sides[1], hasX, start, numTerms);
		
		if (result !== null) {
			for (var i = 0; i < result.length; i++) {
				result[i].position.start += sides[0].length + 1;
				result[i].position.end += sides[0].length + 1;
			}
		}
		
		return result;
	}
	
	else {
		return findFractionalTerms(sides[0], hasX, start, mumTerms);
	}
}

//Sobrecarga que comeca a pesquisa sempre desde o inicio
function findFractionalTermsOnSideFromStart(expression, hasX, side, numTerms) {
	return findFractionalTermsOnSide(expression, hasX, 0, side, numTerms);
}

function findAllFractionalTerms(expression) {
	var med = expression.indexOf("/");
	var fractions = new Array();
	
	while (med !== -1) {
		var endNumerator = med - 1;
		var startDenominator = med + 1;
		var endDenominator = expression.indexOf(")", startDenominator);
		
		var openedParentheses = 1;
		var i = endNumerator - 1;
		
		for (; openedParentheses > 0; i--) {
			if (expression[i] === "(") {
				openedParentheses--;
			}
			
			else if (expression[i] === ")") {
				openedParentheses++;
			}
		}
		
		var startNumerator;
		
		if (expression[i] !== "-") {
			startNumerator = i + 1;
		}
		
		else {
			startNumerator = i;
		}
		
		var fraction = expression.substring(startNumerator, endDenominator + 1);
		var position = new Position(startNumerator, endDenominator);
		var term = new FractionalTerm(fraction, position);
		fractions.push(term);
		
		med = expression.indexOf("/", endDenominator + 1);
	}
	
	return fractions;
}

function findExpressionsInParentheses(expression, operator, start, numTerms) {
	var result = new Array();
	var term = findExpressionInParentheses(expression, operator, start);
	var newStart;
	
	if (term !== null) {
		newStart = term.position.end + 1;
	}
	
	while (term !== null && result.length < numTerms) {
		result.push(term);
		term = findExpressionInParentheses(expression, operator, newStart);
		
		if (term !== null) {
			newStart = term.position.end + 1;
		}
	}
	
	return result;
}

function findExpressionsInParenthesesFromStart(expression, operator, numTerms) {
	return findExpressionsInParentheses(expression, operator, 0, numTerms);
}

function findExpressionsInParenthesesOnSide(expression, operator, start, side, numTerms) {
	var sides = expression.split("=");
	
	if (side === "right" || side === "r" || side === "direito" || side === "d") {
		var result = findExpressionsInParentheses(sides[1], operator, start, numTerms);
		
		if (result !== null) {
			//Ajustes nas posicao para considerar os indices do lado esquerdo tambem
			for (var i = 0; i < result.length; i++) {
				result[i].position.start += sides[0].length + 1;
				result[i].position.end += sides[0].length + 1;
			}
		}
		
		return result;
	}
	
	else {
		return findExpressionsInParentheses(sides[0], operator, start, numTerms);
	}
}

function findExpressionsInParenthesesOnSideFromStart(expression, operator, side, numTerms) {
	return findExpressionsInParenthesesOnSide(expression, operator, 0, side, numTerms);
}

function findMultiplications(expression, start, numTerms) {
	var result = new Array();
	var term = findMultiplication(expression, start);
	var newStart;
	
	if (term !== null) {
		newStart = term.position.end + 1;
	}
	
	while (term !== null && result.length < numTerms) {
		result.push(term);
		term = findMultiplication(expression, operator, newStart);
		
		if (term !== null) {
			newStart = term.position.end + 1;
		}
	}
	
	return result;
}

function findMultiplicationsFromStart(expression, numTerms) {
	return findMultiplications(expression, 0, numTerms);
}

function findMultiplicationsOnSide(expression, start, side, numTerms) {
	var sides = expression.split("=");
	
	if (side === "right" || side === "r" || side === "direito" || side === "d") {
		var result = findMultiplications(sides[1], start, numTerms);
		
		if (result !== null) {
			//Ajustes nas posicao para considerar os indices do lado esquerdo tambem
			for (var i = 0; i < result.length; i++) {
				result[i].position.start += sides[0].length + 1;
				result[i].position.end += sides[0].length + 1;
			}
		}
		
		return result;
	}
	
	else {
		return findMultiplications(sides[0], start, numTerms);
	}
}

function findMultiplicationsOnSideFromStart(expression, side, numTerms) {
	return findMultiplicationsOnSide(expression, 0, side, numTerms)
}

function findAllTerms(expression) {
	var result = new Array();
	
	var hasX = [true, false];
	
	for (var i = 0; i < hasX.length; i++) {
		var term = findTermFromStart(expression, hasX[i]);
		var newStart;
		
		if (term !== null) {
			newStart = term.position.end + 1;
		}
		
		while (term !== null) {
			term = new TermGeneral(term);
			insertSorted(result, term);
			term = findTerm(expression, hasX[i], newStart);
			
			if (term !== null) {
				newStart = term.position.end + 1;
			}
		}
	}
	
	var fractions = findAllFractionalTerms(expression);
	
	for (var i = 0; i < fractions.length; i++) {
		var term = new TermGeneral(fractions[i]);
		insertSorted(result, term);
	}
	
	var term = findExpressionInParenthesesFromStart(expression, "+");
	var newStart;
	
	if (term !== null) {
		newStart = term.position.end + 1;
	}
	
	while (term !== null) {
		term = new TermGeneral(term);
		insertSorted(result, term);
		term = findExpressionInParentheses(expression, newStart);
		
		if (term !== null) {
			newStart = term.position.end + 1;
		}
	}
	
	term = findExpressionInParenthesesFromStart(expression, "-");
	
	if (term !== null) {
		newStart = term.position.end + 1;
	}
	
	while (term !== null) {
		term = new TermGeneral(term);
		insertSorted(result, term);
		term = findExpressionInParentheses(expression, newStart);
		
		if (term !== null) {
			newStart = term.position.end + 1;
		}
	}
	
	term = findTermWithDistributivePropertyFromStart(expression);
	
	if (term !== null) {
		newStart = term.position.end + 1;
	}
	
	while (term !== null) {
		term = new TermGeneral(term);
		insertSorted(result, term);
		term = findTermWithDistributiveProperty(expression, newStart);
		
		if (term !== null) {
			newStart = term.position.end + 1;
		}
	}
	
	return result;
}

function findAllTermsOnSide(expression, side) {
	var sides = expression.split("=");
	
	if (side === "right" || side === "r" || side === "direito" || side === "d") {
		var result = findAllTerms(sides[1]);
		
		if (result !== null) {
			for (var i = 0; i < result.length; i++) {
				result[i].position.start += sides[0].length + 1;
				result[i].position.end += sides[0].length + 1;
			}
		}
		
		return result;
	}
	
	else {
		return findAllTerms(sides[0]);
	}
}

//Caso em que ha uma expressao de numeros com operadores de adicao e subtracao, especifico para o type2NumericalExpression
function findParcialNumericExpression(currentStep) {
	var split = currentStep.split("=");
	var terms = findAllTermsOnSide(currentStep, "right");
	var parcialExpression;
	
	if (currentStep[terms[1].position.start-1] !== "*") { 
		parcialExpression = split[0] + "=" + terms[0].term.toString + terms[1].term.toString;
	}
	
	else {
		parcialExpression = split[0] + "=" + terms[0].term.toString + "*" + terms[1].term.toString;
	}
	var formatEquation = checkFormat(parcialExpression);

	if (levelHint < 5) {
		return hintsTXT[formatEquation][levelHint-1];
	}

	else {
		var result = hintsTXT[formatEquation].slice();
		var completeStep = parcialExpression;

		for (var i = 2; i < terms.length; i++) {
			completeStep += terms[i].toString;
		}

		result[4] = result[4].replace("[STEP]", completeStep);

		return result;
	}
}

//Caso em que ha uma expressao de termos podendo ou nao envolver o X
function findParcialExpressionOnSide(currentStep, hasX, side) {
	var terms = findTermsOnSideFromStart(currentStep, hasX, side, 42);
	
	if (terms.length < 2) {
		return null
	}
	
	else {
		var hasZero = false;
		var i = 0;
		var limit = terms.length - 1;
		
		for (; i < limit; i++) {
			var arrayTerms = [terms[i], terms[i+1]];
			
			if (sumSimilarTerms(arrayTerms) === "0") {
				hasZero = true;
				break;
			}
		}
		
		if (!hasZero) {
			i = 0;
		}
		
		if (hasX) {
			return findParcialExpressionWithXOnSide(currentStep, terms[i], terms[i+1]);
		}
			
		else {
			return findParcialExpressionWithoutXOnSide(currentStep, terms[i], terms[i+1]);
		}
	}
}

function findParcialExpressionWithXOnSide(currentStep, a, b) {
	var text;
	
	if (b.toString[0] === "+") {
		text = hintsTXT["sum of terms with x on the same side"][levelHint-1];
	}
	
	else {			
		text = hintsTXT["subtraction of terms with x on the same side"][levelHint-1];
	}

	if (levelHint !== 1 && levelHint !== 3) {
		if (levelHint !== 5) {
			text = text.replace("[a]", a.toString);
			text = text.replace("[b]", b.toString.replace("-", ""));
		}
		
		if (levelHint >= 4) {
			var terms = [a, b];
			var result = sumSimilarTerms(terms);
			
			if (levelHint === 4) {
				result = "" + result;
				text = text.replace("[RESULT]", result);
				text = text.replace("+ +", "+ ");
				text = text.replace("- -", "- ");
			}
			
			else {
				if (result === "0") {
					result = "";
				}
				
				else if (result[0] !== "-" && a.position.start > 0 && currentStep[a.position.start-1] !== "=") {
					result = "+" + result;
				}
				
				var step = replace(currentStep, b.toString, result, b.position.start);
				step = replace(step, a.toString, "", a.position.start);
				step = step.replace("=+", "=");
				step = step.replace("++", "+");
				step = step.replace("--", "+");
				step = step.replace("+-", "-");
				step = step.replace("-+", "-");
				
				var finalPos = step.length - 1;
				
				if (step[finalPos] === "+" || step[finalPos] === "-" || step[finalPos] === "*") {
					step = step.substring(0, finalPos);
				}
				
				text = text.replace("[STEP]", step);
			}
		}
	}
	
	return text;
}

function findParcialExpressionWithoutXOnSide(currentStep, b, c) {
	var operatorB;
	
	if (b.toString[0] !== "-") {
		operatorB = "";
	}
	
	else {
		operatorB = "-";
	}
	
	//Os textos das dicas sao os mesmos das equacoes no formato x=+-a[operador]+-b
	var formatEquation = "x=" + operatorB + "b" + c.toString[0] + "c";
	var text = hintsTXT[formatEquation][levelHint-1];
	
	if (levelHint > 1) {
		if (levelHint <= 4) {
			text = replaceAll(text, "[b]", b.toString);
			text = replaceAll(text, "[c]", "" + Math.abs(c.toString));
			
			if (levelHint === 3) {
				text = text.replace("+ +", "+ ");
				text = text.replace("- -", "- ");
			}
			
			if (levelHint === 4) {
				var result = "" + eval(b.toString + c.toString);
				text = text.replace("[RESULT]", result);
			}
		}
		
		else {
			var result = "" + eval(b.toString + c.toString);
			
			if (result === "0") {
				result = "";
			}
			
			else if (result[0] !== "-" && b.position.start > 0 && currentStep[c.position.start-1] !== "=") {
				result = "+" + result;
			}
			
			if (b.toString[0] === "+") {
				b.toString = b.toString.substring(1);
			}
			
			var step = replace(currentStep, c.toString, result, c.position.start);
			step = replace(step, b.toString, "", b.position.start);
			step = step.replace("=+", "=");
			step = step.replace("++", "+");
			step = step.replace("--", "+");
			step = step.replace("+-", "-");
			step = step.replace("-+", "-");
			
			var finalPos = step.length - 1;
			
			if (step[finalPos] === "+" || step[finalPos] === "-" || step[finalPos] === "*") {
				step = step.substring(0, finalPos);
			}
			
			text = text.replace("[STEP]", step);
		}
	}
	
	return text;
}

function findSimilarTermsOnDifferentSides(currentStep) {
	var hasX = [false, true];
	var sides = ["left", "right"];
	var terms = new Array();
	var text;
	
	for (var i = 0; terms.length < 2 && i < hasX.length; i++) {
		for (var j = 0; j < sides.length; j++) {
			var term = findTermOnSideFromStart(currentStep, hasX[i], sides[j]);
			
			if (term !== null) {
				terms.push(term);
			}
			
			else {
				terms = new Array();
				break;
			}
		}
	}
	
	if (terms[0].hasX) {
		if (terms[1].isPositive) {
			text = hintsTXT["terms with X on different sides (right is positive)"][levelHint-1];
		}
		
		else {
			text = hintsTXT["terms with X on different sides (right is negative)"][levelHint-1];
		}
	}
	
	else {
		if (terms[0].isPositive) {
			text = hintsTXT["numbers on different sides (left is positive)"][levelHint-1];
		}
		
		else {
			text = hintsTXT["numbers on different sides (left is negative)"][levelHint-1];
		}
	}
	
	if (levelHint > 1) {
		if (levelHint < 4) {
			text = replaceAll(text, "[a]", terms[0].toString);
			text = replaceAll(text, "[b]", terms[1].toString);
		}
		
		else {
			var i;
			
			if (terms[0].hasX) {
				i = 1;
			}
			
			else {
				i = 0;
			}
			
			if (levelHint === 4) {
				var termsReplace = ["[a]", "[b]"];
						
				if (!isNumber(terms[i].toString[0])) {
					var newTerm = terms[i].toString.substring(1);
					text = text.replace(termsReplace[i], newTerm);
				}
			}
			
			else {
				var split = currentStep.split("=");			
				
				if (terms[i].isPositive) {
					split[0] += "-" + terms[i].coefficient;
					split[1] += "-" + terms[i].coefficient;
				}
				
				else {
					split[0] += terms[i].toString.replace("-", "+");
					split[1] += terms[i].toString.replace("-", "+");
				}
					
				var step = split[0] + "=" + split[1];
				text = text.replace("[STEP]", step);
			}
		}
	}

	return text;
}
