function checkParentheses(equation) {
	var start = equation.indexOf("(");
	
	if (start === -1) {
		equation = replaceAll(equation, "+-", "-");
		return equation;
	}
	
	else {
		var end = equation.indexOf(")");
		var insideWithParentheses = equation.substring(start, end+1);
		var inside = equation.substring(start+1, end);
		
		var operator = equation[start-1]
		
		if (operator === "+" || operator === "/")
			return checkParentheses(equation.replace(insideWithParentheses, inside));
		
		else {
			if (isNumber(inside[0]))
				inside = "+" + inside;
			
			if (operator === "-") {
				var expression = replaceAll(inside, "+", "#");
				expression = replaceAll(expression, "-", "+");
				expression = replaceAll(expression, "#", "-");
				expression = replaceAll(expression, "+-", "-");
				expression = replaceAll(expression, "--", "+");
				expression = replaceAll(expression, "-+", "-");
				expression = replaceAll(expression, "++", "+");
				
				var result = equation.replace(insideWithParentheses, expression);
				
				if (inside[0] === "+")
					result = result.replace("--", "-");

				else
					result = result.replace("-+", "+");
				

				return checkParentheses(result);
			}
			
			else if (operator === "*" || isNumber(operator)) {
				var i = start - 1;
				
				if (operator === "*")
					i--;
				
				var multiplier = equation[i];
				i--;
				
				for (; i >= 0; i--) {
					if (isNumber(equation[i]) || equation[i] === "-") {
						multiplier = equation[i] + multiplier;
						
						if (equation[i] === "-") {
							break;
						}
					}
					
					else {
						break;
					}
				}
				
				var distributiveProperty = multiplier;
				
				if (operator === "*")
					distributiveProperty += "*";
				
				distributiveProperty += insideWithParentheses;
				
				multiplier = parseInt(multiplier);
				
				var result = "";
				var current = inside[0];
				
				for (var j = 1; j < inside.length; j++) {
					if (isNumberOrIncognita(inside[j])) {
						current += inside[j];
					}
					
					else {
						if (current === "x") {
							current = "1x";
						}
						
						else if (current === "-x") {
							current = "-1x";
						}
						
						var parcial = multiplier * parseInt(current);
						
						if (parcial >= 0) {
							parcial = "+" + parcial; 
						}
						
						result += parcial;
						
						if (current.indexOf("x") !== -1) {
							result += "x"
						}
						
						current = inside[j];
					}
				}
				
				if (current === "x") {
					current = "1x";
				}
				
				else if (current === "-x") {
					current = "-1x";
				}
				
				var parcial = multiplier * parseInt(current);
				
				if (parcial >= 0) {
					parcial = "+" + parcial; 
				}
				
				result += parcial;
				
				if (current.indexOf("x") !== -1) {
					result += "x"
				}
				
				result = result.replace("+-", "-");
				
				var resultReturn = equation.replace(distributiveProperty, result);
				
				resultReturn = resultReturn.replace("++", "+");
				resultReturn = resultReturn.replace("+-", "-");		
				resultReturn = resultReturn.replace("-+", "-");
				resultReturn = resultReturn.replace("--", "+");
				
				return checkParentheses(resultReturn);
			}
		}
	}
}

function checkReasonAndProportion(equation) {
	var sides = equation.split("=");
	var leftSide = findAllTermsOnSide(equation, "left");
	var rightSide = findAllTermsOnSide(equation, "right");
	
	if (leftSide.length === 1 && rightSide.length === 1 && leftSide[0].type === "FractionalTerm" && rightSide[0].type === "FractionalTerm" ) {
		var fractionLeft = leftSide[0].term;
		var fractionRight = rightSide[0].term;
		
		return multiplyTwoTerms(fractionLeft.numerator, fractionRight.denominator) + "=" + multiplyTwoTerms(fractionLeft.denominator, fractionRight.numerator);
	}
	
	else {
		return equation;
	}
}

//Exceto nas equacoes com razao e proporcao, os denominadores sempre sao termos simples
function checkFractionsForSolve(equation) {
	var fractionsWithX = findFractionalTermsFromStart(equation, true, 42); //Esse numero eh para pegar todas as fracoes
	var fractionsWithoutX = findFractionalTermsFromStart(equation, false, 42);
	
	if (fractionsWithX.length > 0 || fractionsWithoutX.length > 0) {
		var result = "";
		var denominators = new Array();
		
		for (var i = 0; i < fractionsWithX.length; i++) {
			denominators.push(fractionsWithX[i].denominator.coefficient);
		}
		
		for (var i = 0; i < fractionsWithoutX.length; i++) {
			denominators.push(fractionsWithoutX[i].denominator.coefficient);
		}
		
		var mmc = calculateMMC(denominators);
		var terms = [findAllTermsOnSide(equation, "left"), findAllTermsOnSide(equation, "right")];
		
		
		for (var i = 0; i < terms.length; i++) {
			for (var j = 0; j < terms[i].length; j++) {
				var newTermToString = "";
				
				if (terms[i][j].type === "Term") {
					newTermToString = "" + terms[i][j].term.coefficient * mmc;		
					
					if (terms[i][j].hasX) {
						newTermToString += "x";
					}
					
					if (!hasOperatorFromStart(newTermToString[0])) {
						newTermToString = "+" + newTermToString;
					}
				}
				
				else if (terms[i][j].type === "FractionalTerm") {
					var numerator = terms[i][j].term.numerator;
					var denominator = terms[i][j].term.denominator;
					var newValue = mmc / denominator.coefficient;
						
					if (numerator instanceof Term) {
						newValue *= numerator.coefficient;
						
						newTermToString = "" + newValue;
						
						if (!hasOperatorFromStart(newTermToString[0])) {
							newTermToString = "+" + newTermToString;
						}
						
						if (terms[i][j].term.hasX) {
							newTermToString += "x";
						}	
						
	//					var posStart = terms[i][j].term.position.start;
	//					var posEnd = posStart + newTermToString.length - 1;
	//					var position = new Position(posStart, posEnd);
	//					
	//					var newTerm = new Term(newTermToString, position);
	//					
	//					terms[i][j] = new TermGeneral(newTerm);
					}
					
					else {				
	//					var newTermToString;
						
						if (numerator instanceof TermWithDistributiveProperty) {
							newTermToString = numerator.multiplier.coefficient * newValue + "*(" + numerator.termInParentheses.toString + ")";
						}
						
						else { //Termo composto
							newTermToString = newValue + "*(" + numerator.toString + ")";
						}
						
						if (!hasOperatorFromStart(newTermToString[0])) {
							newTermToString = "+" + newTermToString;
						}					
	//					var posStart = terms[i][j].term.position.start;
	//					var posEnd = posStart + newTermToString.length - 1;
	//					var position = new Position(posStart, posEnd);
	//					
	//					var newTerm = new TermWithDistributiveProperty(newTermToString, position);
	//					
	//					terms[i][j] = new TermGeneral(newTerm);
					}
				}
				
				else { //Propriedade Distributiva
					newTermToString = terms[i][j].term.multiplier.coefficient * mmc + "*(" + terms[i][j].term.termInParentheses.toString + ")";
				}
				
				result += newTermToString;
			}
			
			if (i === 0) {
				result += "=";
			}
		}
		
		return result;
	}
	
	else {
		return equation;
	}
}

function checkXWithoutExplicitCoefficient(equation) {
	var result = equation;
	var posX = result.indexOf("x");
	
	while (posX !== -1) {
		if (!isNumber(result[posX-1])) {
			result = replace(result, "x", "1x", posX);
			posX = result.indexOf("x", posX + 2);
		}
		
		else {
			posX = result.indexOf("x", posX + 1);
		}
	}
	
	return result;
}

function splitTermsXAndNumbers(equation) {
	var sides = equation.split("=");
	
	if (isNumberOrIncognita(sides[0][0]))
		sides[0] = "+" + sides[0];
	
	if (isNumberOrIncognita(sides[1][0]))
		sides[1] = "+" + sides[1];
	
	
	var leftSide = sides[0];
	var rightSide = sides[1];
	var term = "";
	var toRight = "";
	var toLeft = "";
	
	for (var i = leftSide.length - 1; i >= 0; i--) {
		term = leftSide[i] + term;
		
		if (!isNumberOrIncognita(leftSide[i]) && leftSide[i] !== "*") {
			if (term[term.length-1] !== "x") {
				sides[0] = replace(sides[0], term, "", i);
				var temp = term.substring(1);
				
				if (term[0] === "-")
					temp = "+" + temp;
				
				else
					temp = "-" + temp;
				
				toRight += temp;
			}
			
			term = "";	
		}
	}
	
	for (var i = rightSide.length - 1; i >= 0; i--) {
		term = rightSide[i] + term;
		
		if (!isNumberOrIncognita(rightSide[i]) && rightSide[i] !== "*") {
			if (term[term.length-1] === "x") {
				sides[1] = sides[1].replace(term, "");
				var temp = term.substring(1);
				
				if (term[0] === "-")
					temp = "+" + temp;
				
				else
					temp = "-" + temp;
				
				toLeft += temp;
			}
			
			term = "";	
		}
	}
	
	sides[0] += toLeft;
	sides[1] += toRight;
	
	return sides;
}

function verifyMultiplicationsInX(expression) {
	expression = expression.toLowerCase();
	var pos = expression.indexOf("x");
	
	while (pos !== -1) {
		if (isNumber(expression[pos-1])) {
			var array = expression.split("");
			insertInArray(array, "*", pos);
			var toString = array.toString();
			expression = replaceAll(toString, ",", "");
			
			pos += 2;
		}
		
		else
			pos++;

		pos = expression.indexOf("x", pos);
	}
	
	return expression;
}
