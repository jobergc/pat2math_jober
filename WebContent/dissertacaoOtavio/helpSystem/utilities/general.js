

//Indica se a expressao passada por parametro possui um ou mais operadores
function hasOperatorFromStart(expression) {
	return expression.indexOf("+") !== -1 || expression.indexOf("-") !== -1 ||
           expression.indexOf("*") !== -1 || expression.indexOf("/") !== -1;
}

//Sobrecarga que permite especificar a posicao em que a pesquisa devera ser iniciada
function hasOperator(expression, start) {
	return expression.indexOf("+", start) !== -1 || expression.indexOf("-", start) !== -1 || 
	       expression.indexOf("*", start) !== -1 || expression.indexOf("/", start) !== -1;
}

//Retorna um array com os operadores da expressao passada por parametro
function findOperators(expression) {
	var operators = new Array();
	
	//Se houver um + ou - na primeira posicao, indica que o primeiro numero eh positivo ou negativo, e nao eh um operador
	for (var i = 1; i < expression.length; i++) {
		if (hasOperatorFromStart(expression[i]) || expression[i] === "(" || expression[i] === ")") {
			operators.push(expression[i]);
		}
	}
	
	return operators;
}


//Insere um termo em um array de maneira que fiquem ordenados pela posicao
function insertSorted(array, term) {
	if (array.length === 0 || term.position.end > array[array.length-1].position.end) {
		array.push(term);
	}
	
	else {
		var size = array.length;
		for (var i = 0; i < size; i++) {
			if (term.position.end < array[i].position.end) {
				for (var j = size - 1; j >= i; j--) {
					array[j+1] = array[j]
				}
				
				array[i] = term;
			}
		}
	}
}

//Procura o passo passado por parametro no array de passos da equacao atual
function searchStep(step) {
	for (var i = 0; i < selectedEquation.steps.length; i++) {
		if (selectedEquation.steps[i].step === step) {
			return i;
		}
	}
	
	return -1;
}

function replace(string, character, newCharacter, start) {
	if (start === 0) {
		return string.replace(character, newCharacter);
	}
	
	else {
		var part1 = string.substring(0, start);
		var part2 = string.substring(start);
		part2 = part2.replace(character, newCharacter);
		
		var result = part1 + part2;
		
		return result;
	}
}

function reasonAndProportionHelp() {
	//img src=https://i.imgur.com/BHHdYgP.png
	//<img src=/pat2math/dissertacaoOtavio/img/rp.png></img>
	$("#reasonAndProportion-box").html("<div style='margin-top: 15px;'><img src=/pat2math/dissertacaoOtavio/img/rp.png></div><button type='button' style='margin-left: 190px;' onclick=$('#reasonAndProportion-box').fadeOut(700)>" + buttonHintTXT + "</button>");
	$("#reasonAndProportion-box").fadeIn(700);
}

function cancellationRuleHelp() {
	//<img src=https://i.imgur.com/nFVmdZW.png>
	//<img src=/pat2math/dissertacaoOtavio/img/rc.png></img>
	$("#cancellationRule-box").html("<div style='margin-top: 15px;'><img src=/pat2math/dissertacaoOtavio/img/rc.png></img></div><button type='button' onclick=$('#cancellationRule-box').fadeOut(700)>" + buttonHintTXT + "</button>");
	$("#cancellationRule-box").fadeIn(700);
}

function registerHit() {
	$.ajax({
		type : "GET",
		url : "newPatequation/addHit",
		data : {

		},
		success : function(data) {
			console.log(data);
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("Ocorreu um erro inesperado");
		}
	});
}

function registerHint(step) {
	$.ajax({
		type : "GET",
		url : "newPatequation/addHint",
		data : {
			"level" : levelHint,
			"step" : step
		},
		success : function(data) {
			console.log(data);
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("Ocorreu um erro inesperado");
		}
	});
}

function registerError(previousStep, wrongStep) {
	$.ajax({
		type : "GET",
		url : "newPatequation/addError",
		data : {
			"level" : levelError,
			"previousStep" : previousStep,
			"wrongStep" : wrongStep
		},
		success : function(data) {
			console.log(data);
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("Ocorreu um erro inesperado");
		}
	});
}