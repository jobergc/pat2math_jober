var primeNumbers = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 
	89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 
	229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 
	379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 
	541, 547, 557, 563, 569, 571, 577, 587, 593, 599, 601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673, 677, 683, 
	691, 701, 709, 719, 727, 733, 739, 743, 751, 757, 761, 769, 773, 787, 797, 809, 811, 821, 823, 827, 829, 839, 853, 857, 859, 
	863, 877, 881, 883, 887, 907, 911, 919, 929, 937, 941, 947, 953, 967, 971, 977, 983, 991, 997]; 

//Retorna a soma dos termos no array "terms" de objetos do tipo Term
function sumSimilarTerms(terms) {
	var sum = terms[0].coefficient;
	var hasX = terms[0].hasX;
	
	for (var i = 1; i < terms.length; i++) {
		sum += terms[i].coefficient;
		
		if (!hasX) {
			hasX = terms[i].hasX;
		}
	}
	
	if (hasX && sum !== 0) {
		sum += "x";
		sum = sum.replace("1x", "x");
	}
	
	else {
		sum = "" + sum;
	}
	
	return sum;
}

function multiplyTerms(terms) {
	var result = terms[0].coefficient;
	var hasX = terms[0].hasX;
	
	for (var i = 1; i < terms.length; i++) {
		result *= terms[i].coefficient;
		
		if (!hasX) {
			hasX = terms[i].hasX;
		}
	}
	
	if (hasX) {
		result += "x";
		result = result.replace("1x", "x");
	}
	
	else {
		result = "" + result;
	}
	
	return result;
}

function evalFraction(fraction) {
	fraction = replaceAll(fraction, "(", "");
	fraction = replaceAll(fraction, ")", "");
	
	var terms = fraction.split("/");
	var result = parseInt(terms[0]) / parseInt(terms[1]);
	
	if (fraction.indexOf("x") !== -1) {
		if (Math.abs(result) !== 1) {
			result += "x";
		}
		
		else if (result === 1) {
			result = "x";
		}
		
		//result === -1
		else {
			result = "-x";
		}
	}
	
	else {
		result = "" + result;
	}
	
	return result;
}

function multiplyTwoTerms(term1, term2) {
	if (term1 instanceof Term && term2 instanceof Term) {
		var terms = [term1, term2];
		return multiplyTerms(terms);
	}
	
	else {
		var term = "";
		var termWithDistributiveProperty = "";
		var compoundTerm = "";
		
		if (term1 instanceof Term) {
			term = term1;
			
			if (term2 instanceof TermWithDistributiveProperty) {
				termWithDistributiveProperty = term2;
			}
			
			else {
				compoundTerm = term2;
			}
		}
		
		else {
			term = term2;
			
			if (term1 instanceof TermWithDistributiveProperty) {
				termWithDistributiveProperty = term1;
			}
			
			else {
				compoundTerm = term1;
			}
		}
		
		if (termWithDistributiveProperty !== "") {
			return term.coefficient * termWithDistributiveProperty.multiplier.toString + "*(" + termWithDistributiveProperty.termInParentheses.toString + ")";
		}
		
		else {
			return term.coefficient + "*(" + compoundTerm.toString + ")";
		}
	}
}

function calculateMMC(numbers) {
	var n = numbers.slice();
	var result = 1;
	var continueMultiplying = true; //Variavel controladora para multiplicar o novo numero do MMC na variavel result
	var repeat = true; //Variavel controladora para repetir o mesmo numero primo e verificar se um dos numeros ainda eh divisivel
	
	for (var i = 0; i < primeNumbers.length; i++) {
		while (repeat) {
			for (var j = 0; j < n.length; j++) {
				if (n[j] % primeNumbers[i] === 0) {
					n[j] /= primeNumbers[i];
					
					if (continueMultiplying) {
						result *= primeNumbers[i];
						continueMultiplying = false;
					}
				}
			}
			
			if (continueMultiplying) {
				repeat = false;
			}
			
			else {
				continueMultiplying = true;
			}
		}
		
		if (stopMMC(n)) {
			break;
		}
		
		else {
			continueMultiplying = true;
			repeat = true;
		}
	}
	
	return result;
}

function stopMMC(numbers) {
	var result = numbers[0] === 1;
	
	if (result === true) {
		for (var i = 1; i < numbers.length; i++) {
			result = numbers[i] === 1;
			
			if (result === false) {
				break;
			}
		}
	}
	
	return result;
}

//Para referenciar o lado direito, escrever "right", "r", "direito" ou "d"
//Para referenciar o lado esquerdo, escrecer "left", "l", "esquerdo" ou "e"
function moveToOtherSide(expression, term, destinationSide) {
	var operation = term[0];
	var isNumber = isNumber(operation);
	
	if (isNumber) {
		operation = "+";
	}
	
	var newOperation;
	
	if (operation === "+") {
		newOperation = "-";
	}
	
	else if (operation === "-") {
		newOperation = "+"
	}
	
	else if (operation === "*") {
		newOperation = "/";
	}
	
	else {
		newOperation = "*";
	}
	
	var sides = expression.split("=");
	
	if (side === "right" || side === "r" || side === "direito" || side === "d") {
		sides[0] = sides[0].replace(term, "");
		
		if (isNumber) {
			term = newOperation + term;
		}
		
		else {
			term[0] = newOperation;
		}
		
		sides[1] += term;
	}
	
	else {
		sides[1] = sides[1].replace(term, "");
		
		if (isNumber) {
			term = newOperation + term;
		}
		
		else {
			term[0] = newOperation;
		}
		
		sides[0] += term;
	}
}

function equalsNumbers(n1, n2) {
	//Comparacao de inteiros
	if (n1 % 1 === 0 && n2 % 1 === 0) {
		return n1 === n2;
	}
	
	//Comparacao de decimais
	else {
		return n1.toFixed(4) === n2.toFixed(4);
	}
}

function simplifyOneStep(fraction) {
    if (isIrreducibleString(fraction))
        return null;
    
    var split = fraction.split("/")
	var numerator = split[0];
	var denominator = split[1];
    var n = parseInt(split[0].replace("x", ""));
    var d = parseInt(split[1]);
    
    for (var i = 0; i < primeNumbers.length; i++) {
        if (n % primeNumbers[i] === 0 && d % primeNumbers[i] === 0) {
            var nPrev = n;
            var dPrev = d;
            n /= primeNumbers[i];
            d /= primeNumbers[i];
            
            if (numerator.indexOf("x") !== -1) {
            	nPrev += "x";
            	
            	if (Math.abs(n) !== 1) {
        			n += "x";
        		}
        		
        		else if (n === 1) {
        			n = "x";
        		}
        		
        		//n === -1
        		else {
        			n = "-x";
        		}
            }
            
            return new StepSimplification(nPrev, dPrev, n, d, primeNumbers[i]);
        }
    }
}

function isIrreducible(numerator, denominator) {
	numerator = Math.abs(numerator);
	denominator = Math.abs(denominator);
	
    for (var i = 0; i < primeNumbers.length; i++) {
        if (numerator < primeNumbers[i] || denominator < primeNumbers[i])
            return true;
        
        else if (numerator % primeNumbers[i] === 0 && denominator % primeNumbers[i] === 0)
            return false;
    }
}

function isIrreducibleString(fraction) {
	if (fraction.indexOf("+") !== -1 || fraction.indexOf("-") !== -1 || fraction.indexOf("*") !== -1) {
		return true;
	}
	
	fraction = replaceAll(fraction, "(", "");
	fraction = replaceAll(fraction, ")", "");
	fraction = replaceAll(fraction, "x", "");
	
	var terms = fraction.split("/");
	var numerator = parseInt(terms[0]);
	var denominator = parseInt(terms[1]);
	
	return isIrreducible(numerator, denominator);
}