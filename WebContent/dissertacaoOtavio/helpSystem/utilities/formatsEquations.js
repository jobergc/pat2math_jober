//Formatos possiveis:
//-x=c
//x=b+c, x=-b+c, -x=b+c, -x=-b+c,
//x=b-c, x=-b-c, -x=b-c, -x=-b-c, 
//x=b*c, x=-b*c, x=b*(-c), x=-b*(-c), -x=b*c, -x=-b*c, -x=b*(-c), -x=-b*(-c),
//x=b/c, x=-b/c, x=b/(-c), x=-b/(-c), -x=b/c, -x=-b/c, -x=b/(-c), -x=-b/(-c),
//x=numeric expression,
//-x=numeric expression
//c=x, c=-x, c=ax, c=-ax, -c=x, -c=-x, -c=ax, -c=-ax
//ax=b+c, ax=-b+c, -ax=b+c, -ax=-b+c,
//ax=b-c, ax=-b-c, -ax=b-c, -ax=-b-c, 
//ax=b*c, ax=-b*c, ax=b*(-c), ax=-b*(-c), -ax=b*c, -ax=-b*c, -ax=b*(-c), -ax=-b*(-c),
//ax=b/c, ax=-b/c, ax=b/(-c), ax=-b/(-c), -ax=b/c, -ax=-b/c, -ax=b/(-c), -ax=-b/(-c),
//ax=numeric expression,
//-ax=numeric eaxpression
//x/b=c, -x/b=c, x/b=-c, -x/b=-c,
//ax/b=c, -ax/b=c, ax/b=-c, -ax/b=-c,
//x+b=c, x-b=c, x+b=-c, x-b=-c,
//-x+b=c, -x-b=c, -x+b=-c, -x-b=-c,
//ax=c, -ax=c, ax=-c, -ax=-c,
//ax+b=c, ax-b=c, ax+b=-c, ax-b=-c, -ax+b=c, -ax-b=c, -ax+b=-c, -ax-b=-c,
//many terms, parentheses, distributive property, reason and proportion, fractions with simple numerator, 
//fractions with compound numerator, fractions with distributive property
function checkFormat(equation) {
	var sides = equation.split("=");
	var leftSide = sides[0];
	
	if ((sides[0] === "x" || sides[0] === "-x" || sides[0] === "ax" || sides[0] === "-ax") && !$.isNumeric(sides[1]) && sides[1].indexOf("x") === -1) {
		if (sides[0] === "-x" && $.isNumeric(sides[1])) {
			if (parseInt(sides[1]) >= 0) {
				return "-x=c";
			}
			
			else {
				return "-x=-c";
			}
		}
		
		var operator;
		
		//Deve comecar a pesquisa a partir da segunda posicao para nao considerar o sinal do numero, por exemplo: -5+7
		if (sides[1].indexOf("*", 1) !== -1) {
			operator = "*";
		}
		
		else if (sides[1].indexOf("/", 1) !== -1) {
			operator = "/";
		}
		
		else if (sides[1].indexOf("+", 1) !== -1) {
			operator = "+";
		}
		
		else {
			operator = "-";
		}

		if (operator === "*" || operator === "/") {
			sides[1] = replaceAll(sides[1], "(", "");
			sides[1] = replaceAll(sides[1], ")", "");
		}
		
		var split = sides[1].split(operator);

		if ((split.length === 2 && $.isNumeric(split[1])) || (split.length === 3 && split[0] === "" && $.isNumeric(split[2]))) {
			var b = "b";
			var c = "c";
			var i;
			
			if (split.length === 2) {
				i = 0;
			}
			
			else {
				i = 1;
				split[1] = "-" + split[1];
			}
			
			if (parseInt(split[i]) < 0) {
				b = "-b";
			}
			
			if (operator === "*" || operator === "/") {
				if (parseInt(split[i+1]) < 0) {
					c = "-c";
				}
			}
			
			return sides[0] + "=" + b + operator + c;
		}
		
		return sides[0] + "=numerical expression"; //Caso em que existem mais de dois numeros no lado direito
	}
	
	var termsRightSide = findAllTerms(sides[1]);
	
	if ($.isNumeric(sides[0]) && termsRightSide.length === 1 && termsRightSide[0].type === "Term") {
		var c = "c";
		var a = termsRightSide[0].term.toString.replace("x", "");
		
		if (parseInt(sides[0]) < 0) {
			c = "-c";
		}
		
		if ($.isNumeric(a)) {
			if (a[0] !== "-") {
				a = "a";
			}
			
			else {
				a = "-a";
			}
		}
		
		return c + "=" + a + "x";
	}
	
	else if ($.isNumeric(sides[1])) {
		var a, b;
		var x = "x";
		var c = parseInt(sides[1]);
		
		if (isSimpleFraction(sides[0])) {
			a = "";
			
			var split = leftSide.split(")/(");
			split[0] = split[0].replace("(", "");
			split[1] = split[1].replace(")", "");
			
			if (parseInt(split[1]) > 0) {
				b = "b";
			}
			
			else {
				b = "-b";
			}
			
			if (split[0].indexOf("x") === 0 || split[0].indexOf("-x") === 0) {
				a = "";
				
				if (leftSide[0] === "-") {
					x = "-x";
				}
			}
			
			else {
				if (parseInt(split[0]) > 0) {
					a = "a";
				}
				
				else {
					a = "-a";
				}
			}
			
			if (c >= 0) {
				c = "c";
			}
			
			else {
				c = "-c";
			}
			
			return a + x + "/" + b + "=" + c;
		}
		
		var split = leftSide.split("x");
		var hasManyTerms = split.length > 2 || (!$.isNumeric(split[0]) && split[0] !== "" && split[0] !== "-") || (!$.isNumeric(split[1]) && split[1] !== "");
		
		if (!hasManyTerms) {
			if ($.isNumeric(split[0])) {
				a = "a";
			}
			
			else {
				a = split[0];
			}
			
			if ($.isNumeric(split[1])) {
				if (parseInt(split[1]) >= 0) {
					b = "+b";
				}
				
				else {
					b = "-b";
				}
			}
			
			else {
				b = "";
			}
			
			if (parseInt(sides[1]) >= 0) {
				c = "c";
			}
			
			else {
				c = "-c";
			}
			
			return a + "x" + b + "=" + c;
		}
	}
	
	if (equation.indexOf("(") !== -1 && equation.indexOf("/") === -1) {
		if (equation.indexOf("*(") === -1) {
			var operator = equation[equation.indexOf("(")-1];
			
			if (operator === "+") {
				return "parentheses with plus sign";
			}
			
			else {
				return "parentheses with minus sign";
			}
		}
		
		else {
			return "distributive property";
		}
	}
	
	if (equation.indexOf("/") === -1) {
		var openParentheses = equation.indexOf("(");
		
		if (openParentheses === -1) {
			return "many terms";
		}
		
		else {
			var isManyTerms = true;
			var start = 0;
			
			while (openParentheses !== -1 && isManyTerms) {
				var closeParentheses = equation.indexOf(")", start);
				var term = equation.substring(openParentheses + 1, closeParentheses);
				term = term.replace("x", "");
				
				if ($.isNumeric(term)) {
					openParentheses = equation.indexOf("(", closeParentheses + 1);
					start = openParentheses + 1;
				}
				
				else {
					isManyTerms = false;
				}
			}
			
			if (isManyTerms) {
				return "many terms";
			}
		}
	}
	
	else {
		var leftSide = findAllTermsOnSide(equation, "left");
		var rightSide = findAllTermsOnSide(equation, "right");
		
		if (leftSide.length === 1 && rightSide.length === 1 && leftSide[0].type === "FractionalTerm" && rightSide[0].type === "FractionalTerm" ) {
			return "reason and proportion";
		}
		
		else {
			if (equation.indexOf("*(") !== -1) {
				return "fractions with distributive property";
			}
			
			else {
				var hasCompoundNumerator = false;
				var fractions = findFractionalTermsFromStart(equation, true, 42);
				
				for (var i = 0; i < fractions.length && !hasCompoundNumerator; i++) {
					hasCompoundNumerator = fractions[i].numerator instanceof CompoundTerm;
				}
				
				if (hasCompoundNumerator) {
					return "fractions with compound numerator";
				}
				
				else {
					return "fractions with simple numerator";
				}
			}
		}
	}
}