var listOfContents = {};
//Primeiro elemento: primeiro plano
//Segundo elemento: último plano
//Terceiro elemento: número de equações dos planos
listOfContents["0"] = [1, 5, 5]; //Planos 1 ao 5
listOfContents["1"] = [7, 10, 5]; //Planos 7 ao 10
listOfContents["2"] = [12, 13, 5]; //Planos 12 ao 13
listOfContents["3"] = [14, 15, 5]; //Planos 14 ao 15
listOfContents["4"] = [17, 18, 5]; //Planos 17 ao 18
listOfContents["5"] = [19, 20, 6]; //Planos 19 ao 20
listOfContents["6"] = [22, 22, 10]; //Plano 22
listOfContents["7"] = [23, 23, 10]; //Plano 23
listOfContents["8"] = [24, 24, 10]; //Plano 24
listOfContents["9"] = [26, 26, 10]; //Plano 26
listOfContents["10"] = [27, 27, 10]; //Plano 27
listOfContents["11"] = [28, 28, 10]; //Plano 28
listOfContents["12"] = [30, 30, 10]; //Plano 30
listOfContents["13"] = [31, 31, 10]; //Plano 31
listOfContents["14"] = [32, 32, 10]; //Plano 32

function checkAll(check) {
	var contents = document.getElementsByClassName("testContent");
	
	for (var i = 0; i < contents.length; i++) {
		contents[i].checked = check;
	}
}

function saveData() {
	var numberQuestions = document.getElementById("numberQuestions").value;
	var allContents = document.getElementsByClassName("testContent");
	var contents = "";
	var i = 0;
	
	for (; i < allContents.length; i++) {
		if (allContents[i].checked) {
			contents = allContents[i].value;			
			var numEquations = document.getElementById("" + i).value;
			if (numEquations !== "") {
				contents += ":" + numEquations;
			}
			
			i++;
			break;
		}
	}
	
	for (; i < allContents.length; i++) {
		if (allContents[i].checked) {
			contents += "," + allContents[i].value;
			var numEquations = document.getElementById("" + i).value;
			
			if (numEquations !== "") {
				contents += ":" + numEquations;
			}
		}
	}
	
	$.ajax({
		type : "GET",
		url : "pat2exam/setNumberQuestionsAndContents",
		data : {
			"numberQuestions" : numberQuestions,
			"contents" : contents
		},
		success : function(data) {
			alert(data);
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("Ocorreu um erro inesperado");
		}
	});
}

function getNumberQuestions() {
	$.ajax({
		type : "GET",
		url : "pat2exam/getNumberQuestions",
		data : {
			
		},
		success : function(numberQuestions) {
			numEquations = parseInt(numberQuestions);
			setCookieDays("numberQuestions", numberQuestions, 1);
			getContentTest();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("Ocorreu um erro inesperado");
		}
	});
}

function getContentTest() {
	$.ajax({
		type : "GET",
		url : "pat2exam/getContent",
		data : {
			
		},
		success : function(content) {
			setCookieDays("content", content, 1);
			contentTest = content.split(",");
			startTest();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("Ocorreu um erro inesperado");
		}
	});
}