function loadTasksExam() {
	var html = '<span class="task" onclick="loadExercise(' + idsExam[idsExam.length-1] + ')" id="task' + idsExam[idsExam.length-1] + '">' + equationsExam[equationsExam.length-1] + '</span>' +
			   '<i style="margin-right: 6px" class="icon-pencil  icon-white"></i>' +
			   '<i id="marktask' + idsExam[idsExam.length-1] + '" class="icon-ok" style="visibility: hidden;"></i>';
	
	for (var i = equationsExam.length - 2; i >= 0; i--) {
		html += '<span class="task" onclick="loadExercise(' + idsExam[i] + ')" id="task' + idsExam[i] + '">' + equationsExam[i] + '</span>' +
				'<i style="margin-right: 6px" class="icon-pencil  icon-white"></i>' +
				'<i id="marktask' + idsExam[i] + '" class="icon-ok" style="visibility: hidden;"></i>';
	}
	
	html += '<span id="finalizarTeste" class="task" onclick="finalizaTeste()" style="color: #fffc00; margin-left: -30px; width: 154px">Finalizar teste</span>';
	$("#tasksExam").html(html);
	$("#tasksExam").slideDown(700);
	
	for (var i = 0; i < equacoesResolvidas.length; i++) {
		marcaQuestaoConcluida(equacoesResolvidas[i]);
	}
}

function loadExercise(id) {
	if (getCookie("tourPAT2Exam") === "") {
		isTourInterativo = true;
		tourPAT2Exam();
	}
	
	previousTypeError = undefined;
	var cookieName = "linesHeight" + id;

	if (getCookie(cookieName) !== "") {
		insertLines(true, id);
	}

	blockMenu = false;
	
	if (exercisesElements[id] !== undefined) {
		var elements = exercisesElements[id];
		openExercise(id, elements[0], elements[1], elements[2], false);
	}
	
	else {
		loadExerciseInDataBase(id);
	}
	
	if (stepsDB[id] === undefined) {
		stepsDB[id] = new Array();
	}
	
	$("#topicsAux").show();
}

function openExercise(id, equation, steps, isPerformed) {
	levelHint = 1;
	levelError = 1;

	var equation;

	if (id !== 0)
		equation = new Equation(equation, scoresStages[planoAtual]);

	else
		equation = new Equation(equation, 20);

	equation.id = id;
	idEquation = id;
	var limit = steps.length - 1;
	
	for (var j = 0; j < steps.length - 1; j++) {
		equation.steps[j] = new Step(steps[j], NORMAL_STEP);
	}
	
	if (isPerformed) {
		equation.steps.push(new Step(steps[limit], NORMAL_SOLUTION));
		equation.isComplete = true;

		if (isTourInterativo === false)
			setTimeout(function() {
				$("#topics").fadeIn();
				blockMenu = true;
			}, 2000);
	}
	
	else if (steps.length > 0) {
		equation.steps.push(new Step(steps[limit], NORMAL_STEP));
	}

	newEquations[0] = equation;
	
	reloadPaper(1);
	
	if (levelGamification !== "without") {
		var cookieName = "equationErrorScore" + idEquation;
		var cookieErrorPoints = getCookie(cookieName);

		if (cookieErrorPoints !== "") {
			var errorPoints = parseInt(cookieErrorPoints) * (-1);
			selectedEquation.addPoints(errorPoints);
		}

		calculatePoints(selectedEquation);
	}
	
	if (isTourInterativo) {
		tourPAT2Exam();
	}

	setCookieDays("currentEquation", idEquation, 1);
}

function loadExerciseInDataBase(id) {
	// setCurrentEquation (id);
	loadingShow();
	$.ajax({
		type : 'GET',
		url : appContext + "student/loadExercise",
		data : {
			"exerciseId" : id
		},
		dataType : 'json',
		success : function(data) {
			exercisesElements[id] = new Array();
			var elements = exercisesElements[id];
			elements[0] = data.equation;
			elements[1] = data.steps;
			elements[2] = data.performed;
			openExercise(id, data.equation, data.steps, data.performed);
			loadingHide();
			//Experimentar executar por um siteTimeout de 3 segundos, ou ver um outro local para colocar
			//if (isPAT2Exam && getCookie("tourPAT2Exam") === "") 
				//tourPAT2Exam(); 

			// cookieName = "numLines" + currentPos + idEquation;
			//			
			// if (getCookie (cookieName) === "")
			// setCookieDays (cookieName, "20", 1);

			// stop = true; //Essa variável recebe false em seguida se o usuário
			// clicou no botão de próxima equação

			// var eq = loadEquation(0);
			//			
			// if (currentPlan === -1 || currentEquation === -1)
			// for (var i = 0; i < planos.length; i++)
			// for (var j = 0; j < planos[i].length; j++)
			// if (planos[i][j] === eq) {
			// currentPlan = i;
			// currentEquation = eq;
			// }
			
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("Ocorreu um erro inesperado");
		}
	});
}

function getEquationById(id, pos) {
	$.ajax({
		type : 'GET',
		async : false,
		url : appContext + "student/loadExercise",
		data : {
			"exerciseId" : id
		},
		dataType : 'json',
		success : function(data) {
			if (data != null) {
				equationsExam[pos] = data.equation;
				equationsExamString += equationsExam[pos] + ",";			
			}
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("Ocorreu um erro inesperado");
		}
	});
}

function loadExerciseExam(id) {
	// setCurrentEquation (id);
	loadingShow();

	var cookieName = "linesHeight" + id;

	if (getCookie(cookieName) !== "")
		insertLines(true, id);

	if (isTourInterativo === false)
		blockMenu = false;

	$.ajax({
		type : 'GET',
		url : appContext + "student/loadExerciseExam",
		data : {
			"exerciseId" : id
		},
		dataType : 'json',
		success : function(data) {
			if (data != null) {
				// Verificar neste momento se a primeira equação já está
				// resolvida e atualizar no array de equações (atualizar nos
				// cookies e localmente).
				// Para avançar às próximas equações, a primeira deve estar
				// resolvida obrigatoriamente. Se o usuário tentar acessar outra
				// equação do mesmo plano,
				// será redirecionado mesmo assim para a primeira equação.

				var equation;

				if (id !== 0)
					equation = new Equation(data.equation,
							scoresStages[planoAtual]);

				else
					equation = new Equation(data.equation, 20);

				equation.id = data.id;
				for (var j = 0; j < data.steps.length; j++) {
					equation.steps[j] = new Step(data.steps[j], 0);
				}

				idEquation = id;

				if (data.performed) {
					equation.isComplete = true;
				}

				newEquations[0] = equation;
			}
			reloadPaper(1);

			if (levelGamification !== "without") {
				var cookieName = "equationErrorScore" + idEquation;
				var cookieErrorPoints = getCookie(cookieName);

				if (cookieErrorPoints !== "") {
					var errorPoints = parseInt(cookieErrorPoints) * (-1);
					selectedEquation.addPoints(errorPoints);
				}

				calculatePoints(selectedEquation);
			}

			setCookieDays("currentEquation", idEquation, 1);
			
			  
		

			// cookieName = "numLines" + currentPos + idEquation;
			//			
			// if (getCookie (cookieName) === "")
			// setCookieDays (cookieName, "20", 1);

			// stop = true; //Essa variável recebe false em seguida se o usuário
			// clicou no botão de próxima equação

			// var eq = loadEquation(0);
			//			
			// if (currentPlan === -1 || currentEquation === -1)
			// for (var i = 0; i < planos.length; i++)
			// for (var j = 0; j < planos[i].length; j++)
			// if (planos[i][j] === eq) {
			// currentPlan = i;
			// currentEquation = eq;
			// }

		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("Ocorreu um erro inesperado");
		}
	});
	blockMenu = false;

	 $("#topics").fadeOut();
	   $("#topicsAux").show();
	loadingHide();

}

function getClassPlanCode(id, lastId) {
	$.ajax({
		type : "GET",
		url : appContext + "student/showTopic",
		data : {
			"idSet" : id
		},
		success : function(data) {
			classPlanCodes[id++] = data;
			
			if (id < lastId) {
				getClassPlanCode(id, lastId);
			}
			
			else {
				var firstId = firstStagePerLevel[currentLevel];

				if (currentLevel < 4) {
					lastId = firstStagePerLevel[currentLevel+1];
				}
				
				else if (currentLevel === 4) {
					lastId = 34;
				}
				
				else {
					lastId = 43;
				}
				
				getExercisesElements(firstId, lastId);
			}
		},
		error : function(XMLHttpRequest, textStatus,
				errorThrown) {
			alert("Perdão, obtivemos um erro ao processar esta ação.");
			return false;
		}
	});
}

function getExerciseElements(idExercise, idPlan, lastIdPlan) {
	$.ajax({
		type : "GET",
		url : appContext + "student/loadExercise",
		data : {
			"exerciseId" : idExercise
		},
		success : function(data) {
			exercisesElements[idExercise] = new Array();
			var elements = exercisesElements[idExercise++];
			elements[0] = data.equation;
			elements[1] = data.steps;
			elements[2] = data.performed;
			var verification = "loadExercise(" + idExercise + ")";
			
			if (classPlanCodes[idPlan].indexOf(verification) !== -1) {
				getExerciseElements(idExercise, idPlan, lastIdPlan);
			}
						
			else {
				idPlan++;
				
				if (idPlan < lastIdPlan) {
					getExerciseElements(idPlan * 100, idPlan, lastIdPlan);
				}
			}
		},
		error : function(XMLHttpRequest, textStatus,
				errorThrown) {
			alert("Perdão, obtivemos um erro ao processar esta ação.");
			return false;
		}
	});
}

function checkEquationExam(currentStep, isCorrect, isFinalAnswer, last, element) {
	var elements = exercisesElements[idEquation];
	var steps = elements[1];	
	steps.push(currentStep);
	
	var stepDB = new StepDataBase(currentStep, isCorrect, isFinalAnswer, idEquation);
	stepsDB[idEquation].push(stepDB);
	
	var step = new Step(currentStep, NORMAL_STEP);
    selectedEquation.lastStep = step;
    selectedEquation.steps.push(step);
    selectedEquation.currentStep = "";
    
    if (currentStep.indexOf ("/") === -1)
    	usedLines++;
    
    else
    	usedLines += 2;
    
    if (usedLines >= (numLines - 4))
    	insertLines(false, idEquation);

    if (currentStep.indexOf("a") === -1 && currentStep.indexOf("d") === -1 && selectedEquation.initialEquation === last && selectedEquation.twoAnswers === false) {
        selectedEquation.initialEquation = currentStep;
    }

    $(selectedSheet + " .trash").remove();

    // $("#hintBox").slideUp(); // .hide("clip", 1000);
    $("#hintText").hide('blind', 500);
    $(".verticalTape").hide('blind', 500);
    $("#hintText").html("");
    $("#feedbackError").hide('blind', 200);
    $("#feedbackError").html("");

    if (element.parent().html().indexOf("frac") !== -1) {
        nextLineServer = element.parent().next().next();
    } else {
        nextLineServer = element.parent().next();
    }
    
    $(selectedSheet + " .canCopy li").draggable("disable");
    $(selectedSheet + " .canCopy li").css("opacity", "0.5");
    $(selectedSheet + " .formula li").css("opacity", "0.5");
    $(selectedSheet + " .canCopy").removeClass("canCopy");
    $(selectedSheet + " .canMove ul").sortable("disable");
    $(selectedSheet + " .canMove li").css("opacity", "0.75");
    nextLineServer.addClass("canMove");
    element.parent().removeClass("canMove");
    element.parent().addClass("canCopy");

    
    if (usingHandwritten) {
    	document.querySelector("myscript-math-web").delete();
        $(".labelDefault input").focus();
        $(".labelDefault input").click(function(){
        	showHandwritten();
    	});
        replaceInputForHandWritten();
    }
    
    var idCool = steps.length;
    $(element).replaceWith("<div class='cool coolAlign' id='borracha" + idCool + "' onclick='deleteFinalStep()' title='Apagar passo'></div>");
    
    if (idCool > 1) {
    	var previousID = "borracha" + (idCool - 1);
    	var previousElement = document.getElementById(previousID);
    	previousElement.style.backgroundImage = "none";
    	previousElement.style.cursor = "default";
    	previousElement.onclick = "";
        var finalizaQuestao = document.getElementById("finalizaQuestao");
        finalizaQuestao.parentNode.removeChild(finalizaQuestao);
    }
    
    else {
    	var finalizaQuestao = document.getElementById("finalizaQuestao");
    	
    	if (finalizaQuestao !== null) {
    		finalizaQuestao.parentNode.removeChild(finalizaQuestao);
    	}
    }
    
    if (usingHandwritten) {
    	nextLineServer.html(
            "<ul>" +
            "<li class='labelDefault'><input type='text' style='visibility:hidden;'></li>" +
            "</ul>" +
            "<div class='trash'></div>" +
            "<button id='button'></button><div id='finalizaQuestao'><button type='button' onclick='finalizaQuestao()'>Enviar para avaliação</button></div>");
    } else{ 
    nextLineServer.html(
            "<ul>" +
            "<li class='labelDefault'><input type='text' id='inputMobile'></li>" +
            "</ul>" +
            "<div class='trash'></div>" +
            "<button id='button'></button><div id='feedbackError'></div><div id='finalizaQuestao'><button type='button' onclick='finalizaQuestao()'>Enviar para avaliação</button></div>" +
            "");
    
    }
    
    centralizeCanMoveAndButton();
    coolAlign();
    sortable();
    draggable();
    trashClick();
    buttonClick();
    focus();
}

function getExercisesElements(firstIdPlan, lastIdPlan) {
	getExerciseElements(firstIdPlan * 100, firstIdPlan, lastIdPlan);
}

function hideMenu() {
	$("#topics").hide();
}