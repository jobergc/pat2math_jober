<div id="fb-root"></div>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script src="/pat2math/js/pat2math.js"></script>
<script src="/pat2math/patexpression/index.js"></script>


	<button id="show_menu" type="button" class="btn btn-info" style="z-index:1000">Menu</button>

    <div id="loading">
  		<img id="loading-image" src="/pat2math/images/Pat2Math_Load.gif" alt="Loading..." />
	</div>
	<div id="loadingWE">
  		<img id="loading-image" src="/pat2math/images/Pat2Math_Load.gif" alt="Loading..." />
	</div>
    
    <!-- <img id="loadingImage" src="img/loading.gif"/> -->
    
    <div id="topics" style="overflow: auto">
    	<div id="bar-header" >
    		<img src="/pat2math/images/logo_horizontal_pat2math.png" style="width: 50%; margin-left: 42px">
    		<span id="idiomSelection" onclick="languageSelection()" title="htmlTXT-1"> 

	    	<img src="/pat2math/images/globe.png" style="width: 12%; margin-left:10px; margin-bottom:2px"/> 
			</span>
    		<span id="newPatequationCurrentFlag" onclick="languageSelection()" title="Flag"> 
				<img src="/pat2math/images/pt-BR.png" style="width: 6%; margin-left:2px; margin-top:20px"> 
			</span>
    		
    	</div>
    	
    	<div class="left">
			<!-- <p><a href="account" class="white-link">Perfil</a></p> -->
			<p><a href="/pat2math/j_spring_security_logout" onclick="logout_click();"><img src="/pat2math/images/logout.png" style="height: 15%; width: 15%; border-radius: 5px; margin-top: -96px; margin-left: 7px" /></a></p>
			<p><div class="generalScore" id="totalScore" style="margin-top: -26px;"></div>
			<p><div class="generalScore" id="levelScore"></div>
			<p><div class="generalScore" id="stageScore"></div>	
<!-- 			<p><a href="/pat2math/j_spring_security_logout"	><img src="/pat2math/images/logout.png" style="height: 15%; width: 15%; border-radius: 5px;" /><span id="exitText">&nbspSair</span></a></p> -->
<!--  			<p><button type="button" onclick="getResolution()">Teste Resolução</button></p> -->
<!-- 			<p><button type="button" onclick="getStep()">Teste Passo</button></p> -->
			
<!-- 			<p><span class="white-link" onclick="rel()">Reload</span></p> -->
		</div>
    	
		<br>
		<%-- <c:forEach items="${topics}" var="topic">
			<span class="topic" onclick="loadTasks(${topic.set.id})">
				${topic.set.name}
			</span>
			<div id="tasks${topic.set.id}" class="tasks"></div>
		</c:forEach> 
		<%@ include file="./topicList.jsp"%>
		--%>
		
		
		<div id="the_list">		
<%-- 		<%@ include file="./topicList.jsp"%> --%>
		</div>
		
		<br><br><br>
		
		<br>
	</div>
	
	<p><span class="hide-menu">
	 
	</span></p>
	
	<div id='feedbackError'></div>
	
	<div id="note">
        <span id="amountPoins"></span>
        <br><br>
        <span>Equações concluídas:</span>

        <div id="progressBar" class="progress">
            <div class="bar" role="progressbar" style="width: 0%;">
                <span class="label"></span>
            </div>
        </div>

        <button id="hint" class="btn">D i c a</button>
        <div id="freeHints" style="width: 212px; margin-left: -20px; margin-top: 2px;"></div>    
    </div>
     <div id='hintBox'><div id='hintText'></div></div>
    
     <div id="papers" style="text-align: center;">
   		<div id="paper-1"  style="display: inline-block;"> 		    
   			<div id="refresh_page" title="Atualizar a página" onclick="window.location.reload();"></div>
   			<div id="logo" title="PAT2Math =D"></div>
   			<div class="limitsHintsAndErrors" id="freeHintsOld"></div>
   			<div class="limitsHintsAndErrors" id="freeErrors">freeErrors</div>
			<div style="display: none;" id="date">
				<div id="dateDay" class="dates"></div>
				<div id="dateMonthAux">
					<div id="dateMonth" class="dates"></div> 
				</div>
				<div id="dateYear" class="dates"></div>
			</div>
			<div id="lines"></div>
			<div id="draft">
				<button id="draftButton" class="btn" onclick="draft()"> Rascunho </button>
			</div>			        	       
     	</div>
	</div>
    
    <div style="position: fixed; top: 0; left: 0">
        <div class="dropdown">
            <!-- Link or button to toggle dropdown -->
            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                <!--<li><a id="addLabel" tabindex="-1" onclick="addLabelDefault();" accesskey="a">Adicionar caixa</a></li>-->
                <li><a id="clearLine" tabindex="-1" onclick="clearLine('all');" accesskey="l">Limpar linha</a></li>
<!--                 <li class="divider"></li> -->
<!--                 <li><a id="abc" tabindex="-1" onclick="referenceToABC('', '', '', 'a');" accesskey="t">Encontrar a, b e c</a></li> -->
<!--                 <li><a id="delta" tabindex="-1" onclick="referenceToDelta();" accesskey="c">Calcular Delta</a></li> -->
<!--                 <li><a id="bhaskara" tabindex="-1" onclick="referenceToBhaskara();" accesskey="b">Calcular Bhaskara</a></li> -->
            </ul>
        </div>
    </div>

	<!-- <div id="noteBox">
        <div id="canvasDiv">
            <p id="clearCanvas">x</p>
        </div>
    </div> -->
	

	<div id="newPoints">+10</div>
	<div id="rewardWorkedExamples" title="htmlTXT-5" onclick="changeColor()"></div>
	<div id="help" title="htmlTXT-0" onclick="helpPage()"></div>

    <div id="mask" onclick="test56()"></div>
<!--     <div id="equationTour"></div> -->
    <!-- <div id="tour" title="Tour Interativo" onclick="tourTCC()"></div> -->
    <div id="reportBug" title="htmlTXT-4" onclick="reportBug()"></div>
    <div id="ranking" title="htmlTXT-2" onclick="ranking()"></div>
	<div id="video-box"></div>
	<div id="help-box"></div>
	<div id="reportBug-box"></div>
	<div id="uploadImage-box"></div>
	<div id="quest-box"></div>
	<div id="topicsAux"></div>
	<div id="tourAux"></div>
	<div id="imLegend" title="I'm Legend"><img src= "/pat2math/patequation/img/ImLegend.jpg" height="310px" width="232px"></img></div>
<!-- 	<div id="plansAux" style='visibility: hidden'></div> -->
<!-- 	<div id="easter-egg-loupe-box"></div> -->
	
	
	<div class="modal" id="msg-box"></div>
	<div id="noticeHint" style="display: none;"><div id="noticeHintModal" class="jGuider ui-draggable ui-draggable-disabled ui-state-disabled" style="display: block; position: absolute; width: 581px; top: 43.967px; left: 473.15px;" id="jGuider_gamification2" aria-disabled="true"><div class="jgContent" style="background: #82C785;"><div class="jgTitle">Nós otimizamos o sistema de ajuda do PAT2Math</div><div class="jgNoDrag"><div class="jgClose"></div><div class="jgDesc">Clique neste botão sempre que precisar de ajuda, em qualquer um dos passos da equação selecionada. E não se preocupe se você não souber como prosseguir após a ajuda recebida: a cada clique adicional no botão de dica em um mesmo passo da equação, o texto de ajuda será mais objetivo. </div><div class="jgButtons jgAlignCenter"><button class="primary" onclick="document.getElementById('noticeHint').style.display='none';">Entendi</button></div></div></div><span class="jgArrow jg-left" style="border-width: 30px 0px 30px 30px; margin-top: -30px;"><span style="border-width: 30px 0px 30px 30px; top: -30px;"></span></span></div></div>
<!-- 		<div id="calculatorIcon" title='Calculadora' onclick="showCalculator()"><img src=/pat2math/patequation/img/calculadora.png border=0></div> -->
<!-- 		<div id="calculator" title='Digite a expressão desejada e clique em "="'> -->
<!-- 	<form name="calculator" > <input type="textfield" name="ans" value=""> -->
<!-- <input type="button" value="=" onClick="document.calculator.ans.value=eval(document.calculator.ans.value)"> -->
<!-- </form> -->
<!-- 	</div> -->


<div id="handwritten">
	<!-- CORRETO - COBRA POR REQUEST -->
	<myscript-math-web apiversion="V4"
		applicationkey="45b6b72b-bef6-4a9d-8089-e8da9ee78298"
		hmackey="17c1c877-4002-4506-ab76-3c80fa67feca">
	</myscript-math-web>
	
	<!-- CHAVE ERRADA - PARA TESTE -->
	<!-- <myscript-math-web
		applicationkey="45b6b72b-bef6-4a9d-8089-e8da9ee78543"
		hmackey="17c1c877-4002-4506-ab76-3c80fa67trew">
	</myscript-math-web> -->


	<!-- <button id="send_button" type="button" class="btn btn-success"
		onclick="copy_result()">Enviar</button> -->
</div>

<div class="modal fade" id="handwrittenModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Tipo de entrada de dados</h4>
      </div>
      <div class="modal-body">
        <p>Escolha o método de entrada de dados:</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="useKeyboard();">Teclado</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="useHandwritting();">Escrita à mão</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.7&appId=669959713214349&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

windowBlur();

function compartilharFacebook(){
	FB.ui({
		method: 'feed',
		name: 'PAT2Math teste',
		app_id: 669959713214349,
		link: 'http://pat2math.unisinos.br/pat2math/ranking',
		picture: 'TESTE COM ESSE TEXTO',
		caption: 'LEGENDA DO PAT2MATH',
		description: 'DESCRIÇÃO DO PAT2MATH'
	}, function(response){});
}

function rankingGeral(){
	$.ajax({
		type: "GET",
		url: "newPatequation/top10",
		data: {"id" : idCurrentUser, "rankingGeral" : true},
		success:
			function(data) {
				$.guider({
					name: "top10",
					title: "RANKING GERAL",
					description: data,									
					alignButtons: "center",
					position: "center",
					buttons: {
						Fechar: {
							click: true,
							className: "primary"
						}
					}
				}).show();
			},
		error:
			 function(XMLHttpRequest, textStatus, errorThrown) {
		     	alert("Perdão, obtivemos um erro ao processar esta ação.");
		 	}
		});
}

</script>