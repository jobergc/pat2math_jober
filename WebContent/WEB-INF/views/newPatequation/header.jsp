<!-- PAT2HAND -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, minimum-scale=1.0, initial-scale=1.0, user-scalable=0">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="mobile-web-app-capable" content="yes">
<!-- <meta name="HandheldFriendly" content="true" /> -->

<link href='http://fonts.googleapis.com/css?family=Petit+Formal+Script' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Iceland' rel='stylesheet' type='text/css'> 
<link href='http://fonts.googleapis.com/css?family=Amatic+SC:400,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="/pat2math/patequation/css/index.css"/>
<link rel="stylesheet" href="/pat2math/patequation/css/fractions.css"/>
<link rel="stylesheet" href="/pat2math/patequation/css/paper.css"/>
<!-- <link rel="stylesheet" href="/pat2math/patequation/css/guider-2.1.0.min.css" type="text/css"/> -->

<!-- PAT2HAND --> 
<link rel="import" href="/pat2math/pat2hand/bower_components/myscript-math-web/myscript-math-web.html">
<script src="/pat2math/pat2hand/bower_components/webcomponentsjs/webcomponents-lite.js"></script>

<!-- 
<link rel="stylesheet" href="/pat2math/pat2hand/css/bootstrap.min.css">
<link rel="stylesheet" href="/pat2math/pat2hand/css/bootstrap-theme.min.css"> 
-->
<link rel="stylesheet" href="/pat2math/pat2hand/css/index.css">
<!-- PAT2HAND -->

<style>
	input[type="text"] {
		text-align: center;
		font-size: 18px;
		font-weight: bold;
		margin-top: -3px;
		height: 20px;
		background-color: transparent !important;
    	margin-top: 1px;
    	height: 19px;
    	width: 30px;
    	min-width: 30px;
    	max-width: 300px;
	}
</style>