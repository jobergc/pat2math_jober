<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>	

<form:form action="/pat2math/group/updatecontent" modelAttribute="exercise" accept-charset="utf-8" class="box block left" enctype="multipart/form-data">

	<form:errors path="*">
		<div class="error-global">
			<spring:message code="error.global" />
		</div>
	</form:errors>
	
	<label>Equação</label>
	<p>
		<form:input path="equation" />
	</p>
	<label>Nível da Questão</label>
	<p>
		<form:select path="nivelDificuldade">
			<form:option value="1"/>
			<form:option value="2"/>
			<form:option value="3"/>
			<form:option value="4"/>
			<form:option value="5"/>
			<form:option value="6"/>
			<form:option value="7"/>
			<form:option value="8"/>
			<form:option value="9"/>
			<form:option value="10"/>
		</form:select>
	</p>
	
	<label>Hash da Emoção</label>
	<div >
		<p>
			<label>Ação 1(Texto Qualquer):</label>
			<form:textarea path="acao1"/>
		</p>
		<p>
			<label>Ação 2(Figura Qualquer):</label>
			<form:input path="acao2" type="file"/>
		</p>
		<p>
			<label>Ação 3(Vídeo Qualquer):</label>
			<form:input path="url"/>
		</p>
	</div>
	
	<img width="400" height="500" src="/pat2math/content/imageDisplay?id=${id}"/>
	
	<p>
		<form:errors path="equation">
			<form:errors path="equation" htmlEscape="false" class="error" />
			<form:errors path="nivelDificuldade" htmlEscape="false" class="error" />
			<form:errors path="acao1" htmlEscape="false" class="error" />
			<form:errors path="acao2" htmlEscape="false" class="error" />
			<form:errors path="url" htmlEscape="false" class="error" />
		</form:errors>
	</p>
	
	<form:hidden path="description" />
	<form:hidden path="name" />
	<form:hidden path="id" />
	<br>
	<input type="submit" class="btn btn-large" value="Alterar Exercício" />
</form:form>