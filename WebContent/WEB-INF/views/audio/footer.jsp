
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script>
function verifyID() {
	var currentID = "" + ${student.id};
	var previousUser = getCookie("previousUser");
	
	if (previousUser !== "") {
		if (previousUser !== currentID) {
			deleteAllCookies();
			setCookieDays("previousUser", currentID, 1);
		}
	}
	
	else
		setCookieDays("previousUser", currentID, 1);
	
	
	var id = parseInt (currentID);
	
	//Usu�rio de teste da Patr�cia
	if (id === 1226) {
		setCookieDays("gift", "true", 1);
	}
	
	verifyIsAdminPAT2Exam();
}

function verifyIsAdminPAT2Exam() {
	$.ajax({
		type : "GET",
		url : "newPatequation/isAdminPAT2Exam",
		data : {
			
		},
		success : function(isAdmin) {
			if (isAdmin === "true") {
				redirectPage("configurepat2exam");
			}
			
			else {
				verifyNeedTakeExam();
			}
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("Ocorreu um erro inesperado");
		}
	});
}

function verifyNeedTakeExam() {
	$.ajax({
		type : "GET",
		url : "newPatequation/verifyNeedTakeExam",
		data : {
			
		},
		success : function(needTakeExam) {
			if (needTakeExam === "true") {
				verifyResolutionExam();
			}
			
			else {
				redirectPage("newpatequation");
			}
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("Ocorreu um erro inesperado");
		}
	});
}

function verifyResolutionExam() {
	$.ajax({
		type : "GET",
		url : "newPatequation/verifyResolutionExam",
		data : {
			
		},
		success : function(isResolved) {
			if (isResolved === "true") {
				redirectPage("newpatequation");
			}
			
			else {
				redirectPage("pat2exam");
			}
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("Ocorreu um erro inesperado");
		}
	});
	
}

function checkGroup(id) {
	
	
}

function redirectPage(url) {
	setTimeout ("location.href='/pat2math/" + url + "';", 2000); 
	//setTimeout ("document.getElementById('go').style.visibility = 'visible';", 5000);
}

verifyID();
</script>