<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<link rel="stylesheet" type="text/css" href="/pat2math/css/patt_calculator.css" id="pagesheet"/>
<link rel="stylesheet" type="text/css" href="/pat2math/pattranslation/css/tooltip.css" id="pagesheet"/>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<div class="box block">
	<h2 class="left">${patt_problem.name}</h2> 
	<c:if test="${not empty topic}">
		<p class="left">Problema associado ao tópico <b>${topic.name}</b></p>
	</c:if>
	
	<p class="left">${patt_problem.statement}</p>
	
	<br>
	<ul id="sortable" class="list">
		<!-- Dados -->	
	  	<c:forEach items="${data}" var="data">
	  		<c:if test = "${data.type == 'd'}">
				<li class="item left" style="padding: 10px 0px 0px 10px; width: 100%;">
		    		<!-- <a href="" tooltip="texto do tooltip"> -->
		    		<a href="" >
		    			Dado: ${data.value} | ${data.statementPart}
		    			<c:if test="${not empty data.statementPart2}">// ${data.statementPart2}</c:if>
		    			<c:if test="${not empty data.statementPart3}">// ${data.statementPart3}</c:if>
		    			<c:if test="${data.irrelevantData}"> <i>(DIS)</i></c:if>    
		    		</a>
		    		<form name="removeData" method="POST" action="/pat2math/patt_problem/removeData?id=${data.id}&problem=${patt_problem.id}">
					    <input type="submit" value="" style="float: right; padding: 0px 10px 0px 4px; margin-top: -25px; margin-right: 10px;" class="icon-remove">
					</form>
		    	</li>
	    	</c:if>
	    </c:forEach>
	   
	    <!-- Incógnitas -->
	    <c:forEach items="${data}" var="data">
	    	<c:if test = "${data.type == 'i'}">
				<li class="item left" style="padding: 10px 0px 0px 10px; width: 100%;">
		    		<a href="">Incógnita: ${data.statementPart}</a>
		    		<form name="removeData" method="POST" action="/pat2math/patt_problem/removeData?id=${data.id}&problem=${patt_problem.id}">
					    <input type="submit" value="" style="float: right; padding: 0px 10px 0px 4px; margin-top: -25px; margin-right: 10px;" class="icon-remove">
					</form>
		    	</li>
		    </c:if>
	    </c:forEach>
	    
	    <!-- Relações -->
	    <c:forEach items="${relations}" var="relation">
	    	<c:if test = "${relation.type == 'r'}">
				<li class="item left" style="padding: 10px 0px 0px 10px; width: 100%;">
		    		<a href="">Relação: ${relation.expression} | ${relation.description}</a>
		    		<form name="removeData" method="POST" action="/pat2math/patt_problem/removeRelation?id=${relation.id}&problem=${patt_problem.id}">
					    <input type="submit" value="" style="float: right; padding: 0px 10px 0px 4px; margin-top: -25px; margin-right: 10px;" class="icon-remove">
					</form>
		    	</li>
		    </c:if>
	    </c:forEach>
	    
	    <!-- Equações -->
	    <c:forEach items="${relations}" var="relation">
	    	<c:if test = "${relation.type == 'e'}">
				<li class="item left" style="padding: 10px 0px 0px 10px; width: 100%;">
		    		<a href="">Equação: ${relation.expression}</a>
		    		<form name="removeData" method="POST" action="/pat2math/patt_problem/removeRelation?id=${relation.id}&problem=${patt_problem.id}">
					    <input type="submit" value="" style="float: right; padding: 0px 10px 0px 4px; margin-top: -25px; margin-right: 10px;" class="icon-remove">
					</form>
		    	</li>
		    </c:if>
	    </c:forEach>
	</ul>
</div>

<c:if test="${not empty errorMessage}">
	<div class="error left">${errorMessage}</div>
</c:if>

<form:form action="/pat2math/patt_problem/addData" modelAttribute="patt_problem" accept-charset="utf-8" class="left">
	<label>Adicionar dados e incógnitas ao problema</label>
	<p><form:hidden path="id" value="${problem.id}" class="focus" /></p>
	<form:hidden path="id_patequation" value="${problem.id_patequation}" class="focus" />
	<p>
		<form:input path="value" id="value" placeholder="0.0" value="" class="focus" style="width: 100px;" required="true"/>
		<form:checkbox path="irrelevantData" value="Java"/><label> Dado Irrelevante para a solução</label>
	</p>
	<p><form:input path="statementPart" placeholder="Parte do Enunciado" value="" class="focus" style="width: 450px;" required="true"/></p>
	<p><form:input path="statementPart2" placeholder="Parte do Enunciado (Opção 2)" value="" class="focus" style="width: 450px;"/></p>
	<p><form:input path="statementPart3" placeholder="Parte do Enunciado (Opção 3)" value="" class="focus" style="width: 450px;"/></p>
	<p></p>
	<p><label>Dicas:</label></p>
	<p><form:input path="selectionHint1" placeholder="Seleção #1 (Point)" value="" class="focus" style="width: 450px;"/></p>
	<p><form:input path="selectionHint2" placeholder="Seleção #2 (Bottom-Out)" value="" class="focus" style="width: 450px;"/></p>
	<p><form:input path="wrongValueHint1" placeholder="Valor Incorreto #1 (Point)" value="" class="focus" style="width: 450px;"/></p>
	<p><form:input path="wrongValueHint2" placeholder="Valor Incorreto #2 (Teach)" value="" class="focus" style="width: 450px;"/></p>
	<p><form:input path="wrongValueHint3" placeholder="Valor Incorreto #3 (Bottom-Out)" value="" class="focus" style="width: 450px;"/></p>
	<p><input class="btn btn-large" id="submitAddData" type="submit" value="Adicionar"></p>
</form:form>

<form:form action="/pat2math/patt_problem/addRelation" modelAttribute="patt_problem" accept-charset="utf-8" class="left">
	<label>Adicionar relações e equações ao problema</label>
	<p><form:hidden path="id" value="${problem.id}" class="focus" /></p>
	<form:hidden path="id_patequation" value="${problem.id_patequation}" class="focus" />
	<p><form:input id="expression_field" path="expression" placeholder="Expressão" value="" class="focus" style="width: 450px;" required="true"/></p>
	<p><form:input path="description" placeholder="Descrição" value="" class="focus" style="width: 450px;"/></p>
	<p></p>
	<p><label>Dicas:</label></p>
	<p><form:input path="wrongValueHint1" placeholder="Valor Incorreto #1 (Point)" value="" class="focus" style="width: 450px;"/></p>
	<p><form:input path="wrongValueHint2" placeholder="Valor Incorreto #2 (Teach)" value="" class="focus" style="width: 450px;"/></p>
	<p><form:input path="wrongValueHint3" placeholder="Valor Incorreto #3 (Bottom-Out)" value="" class="focus" style="width: 450px;"/></p>
	<p><input class="btn btn-large" type="submit" value="Adicionar"></p>
</form:form>

<div id="calculator">
	<div class="keys">
		<!-- operators and other keys -->
		<input type="button" class="pattbtn" onClick="appendValue('7')" value="7">
		<input type="button" class="pattbtn" onClick="appendValue('8')" value="8">
		<input type="button" class="pattbtn" onClick="appendValue('9')" value="9">
		<input type="button" class="pattbtn operator" onClick="appendValue('+')" value="+">
		<input type="button" class="pattbtn operator" onClick="appendValue('(')" value="(">
		<br>
		<input type="button" class="pattbtn" onClick="appendValue('4')" value="4">
		<input type="button" class="pattbtn" onClick="appendValue('5')" value="5">
		<input type="button" class="pattbtn" onClick="appendValue('6')" value="6">
		<input type="button" class="pattbtn operator" onClick="appendValue('-')" value="-">
		<input type="button" class="pattbtn operator" onClick="appendValue(')')" value=")">
		<br>
		<input type="button" class="pattbtn" onClick="appendValue('1')" value="1">
		<input type="button" class="pattbtn" onClick="appendValue('2')" value="2">
		<input type="button" class="pattbtn" onClick="appendValue('3')" value="3">
		<input type="button" class="pattbtn operator" onClick="appendValue('/')" value="/">
		<input type="button" class="pattbtn operator" onClick="appendValue('x=')" value="x=">
		<br>
		<input type="button" class="pattbtn" onClick="appendValue('0')" value="0">
		<input type="button" class="pattbtn" onClick="appendValue('.')" value=".">
		<input type="button" class="pattbtn" onClick="appendValue('=')" value="=">
		<input type="button" class="pattbtn operator" onClick="appendValue('*')" value="*">
		<input type="button" class="pattbtn clear" onClick="clearField()" value="Limpar">
		<br>
		
		<c:forEach items="${data}" var="data">
	  		<c:if test = "${data.type == 'd'}">
	  			<input type="button" class="pattbtn data" onClick="appendValue(${data.value})" value="${data.statementPart}">
	    	</c:if>
	    </c:forEach>
		
		<c:forEach items="${relations}" var="relation">
	  		<c:if test = "${relation.type == 'r'}">
	  			<input type="button" class="pattbtn data" onClick="appendValue('(${relation.expression})')" value="[R] ${relation.description}">
	    	</c:if>
	    </c:forEach>
		
	</div>
</div>

<script>
document.getElementById("submitAddData").onclick = function() {
    document.getElementById("value").value = document.getElementById("value").value.replace(/,/g, '.'); 
    console.log('done');
};

function appendValue(value) {
	$('#expression_field').val($('#expression_field').val() + value);
}

function clearField(value) {
	$('#expression_field').val("");
}
</script>

<div id="sets" class="modal"></div>
<div id="mask" onclick="test56()"></div>