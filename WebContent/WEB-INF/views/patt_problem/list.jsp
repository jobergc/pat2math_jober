<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<p class="left">
	<a href="new" class="btn btn-large">Novo Problema</a>
</p>

<c:if test="${not empty errorMessage}">
	<br><div class="error left">${errorMessage}</div>
</c:if>

<table class="table table-bordered">
    <thead>
	    <tr>
	    	<th><p>Todos os Problemas</p></th>
    	</tr>
    </thead>
	<tbody>
		<c:forEach items="${problems}" var="problem">
    		<tr>
		    	<td>
		    		<a title="excluir" href="<c:url value="/patt_problem/delete/${problem.id}" />" onclick="return confirm('Você tem certeza?')"><i class="icon-remove-circle"></i></a>  
		    		<a title="editar" href="edit/${problem.id}"><i class="icon-edit"></i></a>
		    		<a href="${problem.id}">${problem.name}</a>
		    	</td>
		    </tr>
		</c:forEach>	
    </tbody>
</table>

<c:if test="${empty problems}"><p>Não foi encontrado nenhum problema para ser exibido.</p>
	<br><br><br><br><br>
</c:if>