<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<form:form action="/pat2math/patt_problem/update" modelAttribute="patt_problem" accept-charset="utf-8" class="box block left">
	<form:errors path="*">
		<div class="error-global">
			<spring:message code="error.global" />
		</div>
	</form:errors>
	
	<form:hidden path="id" value="${patt_problem.id}" class="focus" />
	<form:hidden path="id_patequation" value="${problem.id_patequation}" class="focus" />
	
	<br>
	<label>Nome</label>
	<p><form:input path="name" placeholder="Nome" value="" class="focus" /></p>
	<p><form:errors path="name" htmlEscape="false" class="error" /></p>
	
	<label>Tipo</label>
	<p><form:input path="type" placeholder="Tipo" value="" class="focus" /></p>
	<p><form:errors path="type" htmlEscape="false" class="error" /></p>
	
	<label>Problema</label>
	<p><form:textarea path="statement" placeholder="Texto do Problema" value="" class="focus" rows="8" cols="25" /></p>
	<p><form:errors path="statement" htmlEscape="false" class="error" /></p>

	<label>Nível de Dificuldade</label>
	<p><form:input path="level" placeholder="Nível de Dificuldade" value="" class="focus" /></p>
	<p><form:errors path="level" htmlEscape="false" class="error" /></p>
	
	<br>
	<input type="submit" class="btn btn-large" value="Atualizar Problema" />
</form:form>