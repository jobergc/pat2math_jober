<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<div id="plan-id" style="display: none">${plan.id}</div>

<div class="box block">
	<h2 class="left">${plan.name}</h2>
	<p class="left">Plano pertencente ao professor <b>${plan.teacher.firstName} ${plan.teacher.lastName}</b></p>
	
	<ul id="sortable" class="list">
	  	<c:forEach items="${plan.topics}" var="topic">
			<li class="item" style="padding: 10px 0px 0px 0px;">
	    		<a href="/pat2math/patt_topic/${topic.id}">${topic.name}</a>
	    		<form name="removeTopic" method="POST" action="/pat2math/patt_plan/removeTopic?id=${topic.id}&ck=${plan.id}">
				    <input type="submit" value="" style="float: right; padding: 0px 10px 0px 4px; margin-top: -25px; margin-right: 10px;" class="icon-remove">
				</form>
	    	</li>
	    </c:forEach>
	</ul>
	
	<c:if test="${empty plan.topics}">
		<p style="text-align: left;">Este plano ainda não possui nenhum tópico</p>
		<br>
	</c:if>
</div>

<c:choose>
	<c:when test="${empty topics}">
		<p style="text-align: left">Não existem tópicos disponíveis para adicionar a este plano</p>
  	</c:when>
  	<c:otherwise>
		<div class="sets" style="text-align: left">
			<p>Lista de tópicos disponíveis</p>
			<form:form action="/pat2math/patt_plan/addTopics" modelAttribute="patt_topic" accept-charset="utf-8">
				<input style="display: none" id="${plan.id}" type="checkbox" name="ck" value="${plan.id}" checked>
					<c:forEach items="${topics}" var="topic">
						<p>
							<input id="${topic.id}" type="checkbox" name="ck" value="${topic.id}"></input>
							<a href="/pat2math/patt_topic/${topic.id}">${topic.name}</a>
						</p>
					</c:forEach>
				<input class="btn btn-large" type="submit" value="Adicionar Tópico(s) ao Plano">
			</form:form>
		</div>    
  	</c:otherwise>
</c:choose>

<div id="sets" class="modal"></div>
<div id="mask" onclick="test56()"></div>