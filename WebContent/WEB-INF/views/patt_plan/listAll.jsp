<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<p class="left">
	<a href="list" class="btn btn-large">Listar meus planos</a>
	<a href="new" class="btn btn-large">Novo Plano</a>
</p>	

<table class="table table-bordered">
    <thead>
	    <tr>
	    	<th><p>Todos os Planos</p></th>
    	</tr>
    </thead>
	<tbody>
		<c:forEach items="${plans}" var="plan">
    		<tr>
		    	<td>
		    		<a title="excluir" href="<c:url value="/patt_plan/delete/${plan.id}" />" onclick="return confirm('Você tem certeza?')"><i class="icon-remove-circle"></i></a> 
		    		<a title="editar" href="edit/${plan.id}"><i class="icon-edit"></i></a> 
		    		<a href="${plan.id}">${plan.name}</a>
		    	</td>
		    </tr>
		</c:forEach>	
    </tbody>
</table>