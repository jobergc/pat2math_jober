<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!-- @RequestMapping = update -->
<form:form action="/pat2math/patt_plan/update" modelAttribute="patt_plan" accept-charset="utf-8" class="box block left">
	<form:errors path="*">
		<div class="error-global">
			<spring:message code="error.global" />
		</div>
	</form:errors>
	
	<form:hidden path="id" value="${plan.id}" class="focus" />
	
	<br>
	<label>Nome</label>
	<p><form:input path="name" placeholder="Nome do Plano" value="" class="focus" /></p>
	<p><form:errors path="name" htmlEscape="false" class="error" /></p>

	<br>
	<input type="submit" class="btn btn-large" value="Atualizar Plano de Aula" />
</form:form>