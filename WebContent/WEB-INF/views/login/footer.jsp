<script>

function helpPopups(title, description, width) {
	$.guider({
		name: "helpPopups",
		title: title,
		description: description,
		overlay : "dark",
		width : width,    
		alignButtons: "center",
		onShow : function() {document.getElementById("jGuider_helpPopups").style.top = "15px"},
		buttons: {
			OK: {
				click: true,
				className: "primary"
			}
	}
	}).show();
}

</script>


<script>
// 	atualiza os textos dos elementos HTML
if(document.getElementById("loginElem0") != null)
	document.getElementById("loginElem0").innerHTML = loginTXT[0];
if(document.getElementById("loginElem1") != null)
	document.getElementById("loginElem1").innerHTML = loginTXT[1];
document.getElementById("email").placeholder = loginTXT[3];
document.getElementById("password").placeholder = loginTXT[4];
if(document.getElementById("loginElem3") != null)
	document.getElementById("loginElem3").innerHTML = loginTXT[7];
document.getElementById("loginElem4").innerHTML = loginTXT[8];
document.getElementById("loginElem5").innerHTML = loginTXT[9];
document.getElementById("loginElem6").innerHTML = loginTXT[10];
document.getElementById("loginButton").value = loginTXT[14];

//	Atualiza a bandeira do bot�o de altera��o de idioma
document.getElementById("loginCurrentFlag").innerHTML = '<img src="/pat2math/images/' + idioma + '.png" onclick="languageSelection()" style="width:8%; margin-right:-330px"/>';
document.getElementById("loginCurrentFlag").title = idioma;
</script>

