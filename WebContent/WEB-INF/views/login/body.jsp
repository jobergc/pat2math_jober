
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${confirmed == true}">
	<p id="loginElem0" class="msg"></p>
</c:if>
<c:if test="${param.recovery == true}">
	<p id="loginElem1" class="msg"></p>
</c:if>

<form class="box" action="/pat2math/j_spring_security_check" method="post" accept-charset="utf-8">
	<p>
		<img src="/pat2math/images/Pat2MathBETA.png" />
	</p>
	<div id="loginCurrentFlag"></div>
	
	<br>
	
	<p>
		<input id="email" type='text' class="focus" name='j_username' />
	</p>
	
	<p>
		<input id="password" type='password' name='j_password'>			
	</p>
	
	<br>

	<!-- 	<input type="checkbox" name="_spring_security_remember_me" /> Lembrar-me -->
	<input class="btn btn-large" id="loginButton" value="Login" type="submit">
		
    <br>
    <br>
	<c:if test="${param.failed == true}">
		<p id="loginElem3" class="error" style="font-size: 16px;margin-top: 15px""></p>
	</c:if>
	
	<br>
	
	<p class="left"><a id="loginElem4" href="/pat2math/student/new"></a></p> 
 	<p class="left"><a id="loginElem5" href="user/forgotPassword"></a></p>
 	
 	<br>

</form>

<div id="mask" onclick="test56()"></div>

<div id="helpPopups-box">
	
</div>

<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
<script>
	
	function popupBlocked(){
		if(navigator.userAgent.indexOf("Edge")!=-1){
			helpPopups(loginTXT[11], "<img src=/pat2math/img/edge.png border=0 width=98% height=90%>", "45%");
		}
		else if(navigator.userAgent.indexOf("OPR")!=-1){
			helpPopups(loginTXT[11], "<img src=/pat2math/img/opera.png border=0 width=98% height=90%> <br><br> <img src=/pat2math/img/opera2.png border=0 width=98% height=90%>", "45%");
		}
		else if(navigator.userAgent.indexOf("Firefox")!=-1){
			helpPopups(loginTXT[11], "<img src=/pat2math/img/firefox.png border=0 width=98% height=90%> <br><br> <img src=/pat2math/img/firefox2.png border=0 width=98% height=90%>" ,"45%");
		}
		else if(navigator.userAgent.indexOf("Version")!=-1){
			helpPopups(loginTXT[11], "<img src=/pat2math/img/safari.png border=0 width=98% height=90%> <br><br> <img src=/pat2math/img/safari2.png border=0 width=98% height=90%>", "45%");
		}
		else if(navigator.userAgent.indexOf(".NET")!=-1){
			document.getElementById("helpPopups-box").style.height = "50%";
			helpPopups(loginTXT[11], "<img src=/pat2math/img/ie.png border=0 width=98% height=90%> <br><br> <img src=/pat2math/img/ie2.png border=0 width=98% height=90%>", "45%");
		}
		else{
			helpPopups(loginTXT[11], "<img src=/pat2math/img/chrome.png border=0 width=98% height=90%> <br><br> <img src=/pat2math/img/chrome2.png border=0 width=98% height=90%>", "45%");
		}
				
	}
	
	function fazLogin(response) {
		document.getElementById('email').style.visibility = "hidden";
		document.getElementById('password').style.visibility = "hidden";
		document.getElementById('loginButton').style.visibility = "hidden";
		document.getElementById('email').value = ""+response.email;
		document.getElementById('password').value = ""+response.id;
		document.getElementById('loginButton').click(); 
	}
	function statusChangeCallback(response){
		console.log('statusChangeCallback');
		console.log(response);
		if(response.status === 'connected'){
			usuarioConectado();
			login();
		}else if (response.status === 'not_authorized'){
			document.getElementById('status').innerHTML = loginTXT[12];
		}else{
			document.getElementById('status').innerHTML = loginTXT[13];
		}
	}
</script>

