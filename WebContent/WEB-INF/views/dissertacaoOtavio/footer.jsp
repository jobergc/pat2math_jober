

<script>

//	atualiza os textos dos elementos HTML
document.getElementById("help").title = htmlTXT[0];
document.getElementById("idiomSelection").title = htmlTXT[1];
document.getElementById("ranking").title = htmlTXT[2];
document.getElementById("refresh_page").title = htmlTXT[3];
document.getElementById("reportBug").title = htmlTXT[4];
document.getElementById("rewardWorkedExamples").title = htmlTXT[5];
// if(document.getElementById("tour") !== null)
// 	document.getElementById("tour").title = htmlTXT[6];
document.getElementById("hint").innerHTML = htmlTXT[7];

//	Atualiza a bandeira do bot�o de altera��o de idioma
document.getElementById("newPatequationCurrentFlag").innerHTML = '<img src="/pat2math/images/' + idioma + '.png" style="width: 6%; margin-top:16px">';
document.getElementById("newPatequationCurrentFlag").title = idioma;

</script>
<script src="/pat2math/patequation/js/string.js"></script>
<script src="/pat2math/dissertacaoOtavio/object.js"></script>
<script src="/pat2math/patequation/js/conversion.js"></script>
<script src="/pat2math/dissertacaoOtavio/server.js"></script>
<script src="/pat2math/dissertacaoOtavio/helpSystem/hintsAndErrorsFeedbacks/hintsBank.js"></script>
<script src="/pat2math/dissertacaoOtavio/helpSystem/hintsAndErrorsFeedbacks/errorFeedbacksBank.js"></script>
<script src="/pat2math/dissertacaoOtavio/helpSystem/hintsAndErrorsFeedbacks/hintsSelector.js"></script>
<script src="/pat2math/dissertacaoOtavio/helpSystem/hintsAndErrorsFeedbacks/errorFeedbacksSelector.js"></script>
<script src="/pat2math/dissertacaoOtavio/helpSystem/utilities/fraction.js"></script>
<script src="/pat2math/dissertacaoOtavio/helpSystem/utilities/formatsEquations.js"></script>
<script src="/pat2math/dissertacaoOtavio/helpSystem/utilities/adjust.js"></script>
<script src="/pat2math/dissertacaoOtavio/helpSystem/utilities/general.js"></script>
<script src="/pat2math/dissertacaoOtavio/helpSystem/utilities/math.js"></script>
<script src="/pat2math/dissertacaoOtavio/helpSystem/utilities/findTerms.js"></script>
<script src="/pat2math/dissertacaoOtavio/helpSystem/expertSystem/corrector.js"></script>
<script src="/pat2math/dissertacaoOtavio/helpSystem/expertSystem/resolver.js"></script>
<script src="/pat2math/patequation/js/tour.js"></script>
<script src="/pat2math/patequation/js/workedExamplesUtil.js"></script>
<script src="/pat2math/dissertacaoOtavio/workedExamples/workedExamples.js"></script>
<script src="/pat2math/dissertacaoOtavio/workedExamples/workedExamplesController.js"></script>
<script src="/pat2math/dissertacaoOtavio/gamification/difficultLevelsAndClassPlans.js"></script>
<script src="/pat2math/dissertacaoOtavio/gamification/specialRewards.js"></script>
<script src="/pat2math/dissertacaoOtavio/gamification/score.js"></script>
<script src="/pat2math/dissertacaoOtavio/index.js"></script>
<script src="/pat2math/patequation/js/decimalNumbers.js"></script>
<script src="/pat2math/patequation/js/paper.js"></script>
<!-- <script src="/pat2math/patequation/js/guider-2.1.0.min.js"></script> -->

<!-- PAT2HAND -->
<!-- 
<script src="/pat2math/pat2hand/js/jquery.min.js"></script>
<script src="/pat2math/pat2hand/js/jquery-ui.custom.min.js"></script> 
<script src="/pat2math/pat2hand/js/bootstrap.min.js"></script>
-->
<script src="/pat2math/pat2hand/js/index.js"></script>
<!-- PAT2HAND -->