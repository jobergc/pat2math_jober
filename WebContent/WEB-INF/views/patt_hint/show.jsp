<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<div id="topic-id" style="display: none">${hint.id}</div>

<div class="box block">
	<h2 class="left">${hint.name}</h2> 
	<p class="left">Dica genérica associada à 
		<c:if test = "${hint.reference == 'd'}"><b>Dados</b></c:if>
		<c:if test = "${hint.reference == 'i'}"><b>Incógnitas</b></c:if>
		<c:if test = "${hint.reference == 'r'}"><b>Relações</b></c:if>
		<c:if test = "${hint.reference == 'e'}"><b>Equações</b></c:if>
	</p>
	
	<p class="left">Dica de nível 
		<c:if test = "${hint.level == '1'}"><b>1: Teach</b></c:if>
		<c:if test = "${hint.level == '2'}"><b>2: Point</b></c:if>
		<c:if test = "${hint.level == '3'}"><b>3: Bottom-Out</b></c:if>
	</p>
	
	<p class="left">${hint.hint}</p>
</div>

<div id="sets" class="modal"></div>
<div id="mask" onclick="test56()"></div>