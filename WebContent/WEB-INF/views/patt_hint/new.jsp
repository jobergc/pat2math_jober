<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<form:form action="." modelAttribute="patt_hint" accept-charset="utf-8" class="box block left">
	<form:errors path="*">
		<div class="error-global">
			<spring:message code="error.global" />
		</div>
	</form:errors>
	
	<form:hidden path="type" value="generic" class="focus" />
	<form:hidden path="id_problem" value="0" class="focus" />
	
	<br>
	<label>Nome</label>
	<p><form:input path="name" placeholder="Nome da Dica" value="" class="focus" /></p>
	<p><form:errors path="name" htmlEscape="false" class="error" /></p>
	
	<label>Dica</label>
	<p><form:textarea path="hint" placeholder="Texto da Dica" value="" class="focus" rows="3" cols="25" /></p>
	<p><form:errors path="hint" htmlEscape="false" class="error" /></p>
	
	<label>Referente a [d]ado, [i]ncognita, [r]elação ou [e]quação</label>
	<p><form:input path="reference" placeholder="" value="d" class="focus" /></p>
	<p><form:errors path="reference" htmlEscape="false" class="error" /></p>
	
	<label>Ordem (1: Point, 2: Teach, 3: Bottom-Out)</label>
	<p><form:input path="level" placeholder="Ordem da Dica" value="1" class="focus" /></p>
	<p><form:errors path="level" htmlEscape="false" class="error" /></p>

	<br>
	<input type="submit" class="btn btn-large" value="Criar Dica" />
</form:form>