<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<p class="left">
	<a href="new" class="btn btn-large">Nova Dica</a>
</p>

<table class="table table-bordered">
    <thead>
	    <tr>
	    	<th><p>Todas as Dicas</p></th>
    	</tr>
    </thead>
	<tbody>
		<c:forEach items="${hints}" var="hint">
    		<tr>
		    	<td>
		    		<a title="excluir" href="<c:url value="/patt_hint/delete/${hint.id}" />" onclick="return confirm('Você tem certeza?')"><i class="icon-remove-circle"></i></a>  
		    		<a title="editar" href="edit/${hint.id}"><i class="icon-edit"></i></a> 
		    		<a href="${hint.id}">${hint.name}</a>
		    	</td>
		    </tr>
		</c:forEach>	
    </tbody>
</table>

<c:if test="${empty hints}"><p>Não foi encontrada nenhuma dica para ser exibida.</p>
	<br><br><br><br><br>
</c:if>