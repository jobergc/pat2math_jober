<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<body>
	<script src="/pat2math/js/cookies.js"></script>
	<script src="/pat2math/js/pat2math.js"></script>
	<script src="/pat2math/patexpression/index.js"></script>
	<div class="boxpersonality">
				
		<table class="table table-bordered">
		<thead>
		    <tr>
		    	<th><p align="center"><b>Questionário de Personalidade</b><br></p>

<p align="center">
INSTRUÇÕES. A seguir encontram-se algumas características (afirmações) que podem ou não lhe dizer respeito. <br>
Por favor, escolha uma das opções que melhor expresse sua opinião em relação a você mesmo. <br>
Vales ressaltar que não existem respostas certas ou erradas.</p></th>
			
	    	</tr>
		</thead>
		
		<tbody>
			<c:forEach items="${contentsPersonality}" var="content">
			    <tr >
			    	<td>
			    		<b>${content.id} ) ${content.description} </b>
			    	</td>
			    </tr> 
			    <tr >
			    	<td>
			    	<form>
			    	
			    		<input type="radio" value=1 name="answer${content.id}" onclick="setAnswer( ${content.id}, 1 )" > Discordo Totalmente   
			    	
			    		<input type="radio" value=2 name="answer${content.id}" onclick="setAnswer( ${content.id}, 2 )"> Discordo  
			    		
			    		<input type="radio" value=3 name="answer${content.id}" onclick="setAnswer( ${content.id}, 3 )" > Nem concordo nem discordo       
			    		
			    		<input type="radio" value=4 name="answer${content.id}" onclick="setAnswer( ${content.id}, 4 )"> Concordo      
			    		
			    		<input type="radio" value=5 name="answer${content.id}" onclick="setAnswer( ${content.id}, 5 )"> Concordo Totalmente   
			    	</form>
			    	</td>
			    </tr>
			 </c:forEach>
			
	    	</tbody>
		</table>
		<div>
			<p id="msg" style="color: red"></p>
		</div>
		<input type="button" value="Continuar" onclick="finalizaQuestionario()">
	</div>
</body>