<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<p>Selecione o número de questões para o teste:</p>
<select id="numberQuestions">
  <option value="1">1</option>
  <option value="2">2</option>
  <option value="3">3</option>
  <option value="4">4</option>
  <option value="5">5</option>
  <option value="6">6</option>
  <option value="7">7</option>
  <option value="8">8</option>
  <option value="9">9</option>
  <option value="10">10</option>
  <option value="11">11</option>
  <option value="12">12</option>
  <option value="13">13</option>
  <option value="14">14</option>
  <option value="15">15</option>
  <option value="16">16</option>
  <option value="17">17</option>
  <option value="18">18</option>
  <option value="19">19</option>
  <option value="20">20</option>
  <option value="21">21</option>
  <option value="22">22</option>
  <option value="23">23</option>
  <option value="24">24</option>
  <option value="25">25</option>
  <option value="26">26</option>
  <option value="27">27</option>
  <option value="28">28</option>
  <option value="29">29</option>
  <option value="30">30</option>
</select>

<p>Selecione os conteúdos do teste (se não desejar especificar o número de equações por conteúdo, deixe as caixas de texto vazias):</p>
<table style="text-align: left; margin-left: 374px">
<tbody>
<tr>
<td><input class="testContent" type="checkbox" value="0" name="content0"> x±b=±c </td><td><input type="text" class="numberEquationsPerContent" id="0" name="content0"></td>
<td>&nbsp <input class="testContent" type="checkbox" value="1" name="content1"> -x±b=±c </td><td><input type="text" class="numberEquationsPerContent" id="1" name="content1"></td>
</tr>
<tr>
<td><input class="testContent" type="checkbox" value="2" name="content2"> ax=±c </td><td><input type="text" class="numberEquationsPerContent" id="2" name="content2"></td>
<td>&nbsp <input class="testContent" type="checkbox" value="3" name="content3"> -ax=±c </td><td><input type="text" class="numberEquationsPerContent" id="3" name="content3"></td>
</tr>
<tr>
<td><input class="testContent" type="checkbox" value="4" name="content4"> x/b=±c </td><td><input type="text" class="numberEquationsPerContent" id="4" name="content4"></td>
<td>&nbsp <input class="testContent" type="checkbox" value="5" name="content5"> -x/b=±c </td><td><input type="text" class="numberEquationsPerContent" id="5" name="content5"></td>
</tr>
<tr>
<td><input class="testContent" type="checkbox" value="6" name="content6"> ax±b=±c </td><td><input type="text" class="numberEquationsPerContent" id="6" name="content6"></td>
<td>&nbsp <input class="testContent" type="checkbox" value="7" name="content7"> -ax±b=±c </td><td><input type="text" class="numberEquationsPerContent" id="7" name="content7"></td>
</tr>
<tr>
<td><input class="testContent" type="checkbox" value="8" name="content8"> Vários Termos </td><td><input type="text" class="numberEquationsPerContent" id="8" name="content8"></td>
<td>&nbsp <input class="testContent" type="checkbox" value="9" name="content9"> Parênteses </td><td><input type="text" class="numberEquationsPerContent" id="9" name="content9"></td>
</tr>
<tr>
<td><input class="testContent" type="checkbox" value="10" name="content10"> Propriedade Distributiva </td><td><input type="text" class="numberEquationsPerContent" id="10" name="content10"></td>
<td>&nbsp <input class="testContent" type="checkbox" value="11" name="content11"> Razão e Proporção </td><td><input type="text" class="numberEquationsPerContent" id="11" name="content11"></td>
</tr>
<tr>
<td><input class="testContent" type="checkbox" value="12" name="content12"> Frações Simples </td><td><input type="text" class="numberEquationsPerContent" id="12" name="content12"></td>
<td>&nbsp <input class="testContent" type="checkbox" value="13" name="content13"> Frações Compostas &nbsp </td><td><input type="text" class="numberEquationsPerContent" id="13" name="content13"></td>
</tr>
<tr>
<td><input class="testContent" type="checkbox" value="14" name="content14"> Frações com Propriedade Distributiva &nbsp </td><td><input type="text" class="numberEquationsPerContent" id="14" name="content14"></td>
<td>&nbsp <button onclick="checkAll(true)">Marcar todos</button></td><td><button onclick="checkAll(false)">Desmarcar todos</button></td>
</tr>
</tbody>
</table>
<!-- DivTable.com -->
<br>
<button onclick="saveData()">Salvar</button>