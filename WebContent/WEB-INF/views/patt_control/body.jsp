<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div id="loading">
	<img id="loading-image" src="/pat2math/images/Pat2Math_Load.gif"
		alt="Loading..." />
</div>

<div id="loadingWE">
	<img id="loading-image" src="/pat2math/images/Pat2Math_Load.gif"
		alt="Loading..." />
</div>

<div id="topics" style="overflow: auto">
	<div id="bar-header">
		<a href="student/home"><img
			src="/pat2math/images/logo_horizontal_pat2math.png"
			style="width: 50%;"></a>
	</div>

	<!-- Logout button -->
	<div class="left">
		<p>
			<a onclick href="/pat2math/login"><img
				src="/pat2math/images/logout.png"
				style="height: 15%; width: 15%; border-radius: 5px;" /></a>
		</p>
	</div>

	<!-- Menu com os planos de aula. -->
	<br>
	<div id="the_list">
		<c:forEach items="${topics}" var="topic">

			<!-- Mostra o cadeado e tranca os tópicos ainda não liberados -->
			<c:if test="${unlockedTopic lt topic.sequence}">
				<div class="locked" id="lplan${topic.sequence}"
					onclick="padlockClick()">
					<img src="/pat2math/patequation/img/cadeado_fechado.png"></img>
				</div>
			</c:if>
			<span class="topic"
				onclick="openClose(${topic.id},${topic.sequence}, ${unlockedTopic})">${topic.name}</span>

			<!-- Esconde os problemas dos tópicos ainda não liberados -->
			<c:choose>
				<c:when test="${unlockedTopic lt topic.sequence}">
					<span class="tasks" id="problems${topic.sequence}"
						style="display: none"> <c:forEach items="${problems}"
							var="problem">
							<c:if test="${(problem.topic.id eq topic.id)}">
								<span class="task"
									onclick="loadProblem(${problem.id}, '${problem.statement}')"
									id="task${problem.id}">${problem.name}</span>
								<c:choose>
									<c:when test="${problem.completed == true}">
										<i style="margin-right: 6px" class="icon-pencil icon-white"></i>
										<i class="icon-ok icon-white"></i>
									</c:when>
									<c:otherwise>
										<i style="margin-right: 24px" class="icon-pencil icon-white"
											id="icon-pencil-${problem.id}"></i>
										<i class="icon-ok icon-white hide" id="icon-ok-${problem.id}"></i>
									</c:otherwise>
								</c:choose>
							</c:if>
						</c:forEach>
					</span>
				</c:when>
				<c:otherwise>
					<span class="tasks" id="problems${topic.id}" style="display: block">
						<c:forEach items="${problems}" var="problem">
							<c:if test="${(problem.topic.id eq topic.id)}">
								<span class="task"
									onclick="loadProblem(${problem.id}, '${problem.statement}')"
									id="task${problem.id}">${problem.name}</span>
								<c:choose>
									<c:when test="${problem.completed == true}">
										<i style="margin-right: 6px" class="icon-pencil icon-white"></i>
										<i class="icon-ok icon-white"></i>
									</c:when>
									<c:otherwise>
										<i style="margin-right: 24px" class="icon-pencil icon-white"
											id="icon-pencil-${problem.id}"></i>
										<i class="icon-ok icon-white hide" id="icon-ok-${problem.id}"></i>
									</c:otherwise>
								</c:choose>
							</c:if>
						</c:forEach>
					</span>
				</c:otherwise>
			</c:choose>
		</c:forEach>
	</div>
	<br>
	<br>
	<br>
</div>

<p>
	<span class="hide-menu"></span>
</p>

<div id="note">
	<span id="amountPoins">${score} pontos</span><br> <br>
	<span>Problemas resolvidos:</span>

	<div id="progressBar" class="progress">
		<div class="bar" role="progressbar" style="width: ${widthBar}%;">
			<span class="label">${problemsSolved}/${totalProblems}</span>
		</div>
	</div>

	<!-- <button id="hint" class="btn" onclick="defineHint()">D i c a</button> -->
</div>

<div id="hintBox">
	<div id="hintText"></div>
</div>

<div id="papers" style="text-align: center; margin-top: -119px">
	<div id="paper-1" style="display: inline-block;">
		<div id="refresh_page" title="Atualizar a página"></div>
		<div id="logo" title="PAT2Math =D"></div>

		<div style="display: none;" id="date">
			<div id="dateDay" class="dates"></div>
			<div id="dateMonthAux">
				<div id="dateMonth" class="dates"></div>
			</div>
			<div id="dateYear" class="dates"></div>
		</div>
		<div id="lines"></div>
	</div>
</div>

<div id="marker-selector" title="Selecionar um marcador" style="visibility: hidden;"
	onclick="selectMarker()">
	<div id="color-picker-yellow"></div>
	<div id="color-picker-green"></div>
</div>

<div style="position: fixed; top: 0; left: 0">
	<div class="dropdown">
		<!-- Link or button to toggle dropdown -->
		<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
			<li><a id="clearLine" tabindex="-1" onclick="clearLine('all');"
				accesskey="l">Limpar linha</a></li>
		</ul>
	</div>
</div>

<div id="newPoints">+10</div>
<div id="help" title="Páginas de Ajuda" onclick="helpPage()"></div>
<div id="mask" onclick="test56()"></div>
<div id="reportBug" title="Reportar um problema no PAT2Math"
	onclick="reportBug()"></div>
<div id="video-box"></div>
<div id="help-box"></div>
<div id="reportBug-box"></div>
<div id="uploadImage-box"></div>
<div id="quest-box"></div>
<div id="topicsAux"></div>
<div id="tourAux"></div>

<div class="modal" id="msg-box"></div>

<div id="calculator">
	<div class="calculator_keys" id="calculator_keys">
		<!-- operators and other keys -->
		<input type="button" class="pattbtn" onClick="appendValue('7')"
			value="7"> <input type="button" class="pattbtn"
			onClick="appendValue('8')" value="8"> <input type="button"
			class="pattbtn" onClick="appendValue('9')" value="9"> <input
			type="button" class="pattbtn operator" onClick="appendValue('+')"
			value="+"> <input type="button" class="pattbtn operator"
			onClick="appendValue('(')" value="("> <br> <input
			type="button" class="pattbtn" onClick="appendValue('4')" value="4">
		<input type="button" class="pattbtn" onClick="appendValue('5')"
			value="5"> <input type="button" class="pattbtn"
			onClick="appendValue('6')" value="6"> <input type="button"
			class="pattbtn operator" onClick="appendValue('-')" value="-">
		<input type="button" class="pattbtn operator"
			onClick="appendValue(')')" value=")"> <br> <input
			type="button" class="pattbtn" onClick="appendValue('1')" value="1">
		<input type="button" class="pattbtn" onClick="appendValue('2')"
			value="2"> <input type="button" class="pattbtn"
			onClick="appendValue('3')" value="3"> <input type="button"
			class="pattbtn operator" onClick="appendValue('/')" value="/">
		<input type="button" class="pattbtn operator"
			onClick="appendValue('x')" value="x"> <br> <input
			type="button" class="pattbtn" onClick="appendValue('0')" value="0">
		<input type="button" class="pattbtn" onClick="appendValue('.')"
			value=".">
		<!-- <input type="button" class="pattbtn" onClick="confirm()" value="="> -->
		<input type="button" class="pattbtn" onClick="appendValue('=')"
			value="="> <input type="button" class="pattbtn operator"
			onClick="appendValue('*')" value="*"> <input type="button"
			class="pattbtn clear" onClick="clearField()" value="Limpar">
		<br>
		<div class="calculator_extra_keys" id="calculator_extra_keys"></div>
	</div>
</div>

<script>
	function trim(str) {
	    return str.replace(/^\s+|\s+$/gm,'');
	}
	
	function setMouseFocus(element) {
		element.focus();
	}
</script>