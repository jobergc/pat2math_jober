<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<form:form action="." modelAttribute="patt_topic" accept-charset="utf-8" class="box block left">
	<form:errors path="*">
		<div class="error-global">
			<spring:message code="error.global" />
		</div>
	</form:errors>
	
	<br>
	<label>Nome</label>
	<p><form:input path="name" placeholder="Nome do Tópico" value="" class="focus" /></p>
	<p><form:errors path="name" htmlEscape="false" class="error" /></p>
	
	<label>Ordem</label>
	<p><form:input path="sequence" placeholder="Ordem do tópico na lista" value="" class="focus" /></p>
	<p><form:errors path="sequence" htmlEscape="false" class="error" /></p>

	<label>Nível de Dificuldade</label>
	<p><form:input path="level" placeholder="Nível de Dificuldade" value="" class="focus" /></p>
	<p><form:errors path="level" htmlEscape="false" class="error" /></p>

	<br>
	<input type="submit" class="btn btn-large" value="Criar Tópico" />
</form:form>