<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<p class="left">
	<a href="new" class="btn btn-large">Novo Tópico</a>
</p>

<table class="table table-bordered">
    <thead>
	    <tr>
	    	<th><p>Todos os Tópicos</p></th>
    	</tr>
    </thead>
	<tbody>
		<c:forEach items="${topics}" var="topic">
    		<tr>
		    	<td>
		    		<a title="excluir" href="<c:url value="/patt_topic/delete/${topic.id}" />" onclick="return confirm('Você tem certeza?')"><i class="icon-remove-circle"></i></a>  
		    		<a title="editar" href="edit/${topic.id}"><i class="icon-edit"></i></a> 
		    		<a href="${topic.id}">${topic.name}</a>
		    	</td>
		    </tr>
		</c:forEach>	
    </tbody>
</table>

<c:if test="${empty topics}"><p>Não foi encontrado nenhum tópico para ser exibido.</p>
	<br><br><br><br><br>
</c:if>