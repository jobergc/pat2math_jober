<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<div id="topic-id" style="display: none">${topic.id}</div>

<div class="box block">
	<h2 class="left">${topic.name}</h2> 
	<c:if test="${not empty plan}">
		<p class="left">Tópico associado ao plano <b>${plan.name}</b></p>
	</c:if>
	
	<ul id="sortable" class="list">
	  	<c:forEach items="${topic.problems}" var="problem">
			<li class="item" style="padding: 10px 0px 0px 0px;">
	    		<a href="/pat2math/patt_problem/${problem.id}">${problem.name}</a>
	    		<form name="removeProblem" method="POST" action="/pat2math/patt_topic/removeProblem?id=${problem.id}&ck=${topic.id}">
				    <input type="submit" value="" style="float: right; padding: 0px 10px 0px 4px; margin-top: -25px; margin-right: 10px;" class="icon-remove">
				</form>
	    	</li>
	    </c:forEach>
	</ul>
	
	<c:if test="${empty topic.problems}">
		<p style="text-align: left;">Este tópico ainda não possui nenhum problema</p>
		<br>
	</c:if>
</div>

<c:choose>
	<c:when test="${empty problems}">
		<p style="text-align: left">Não existem problemas disponíveis para adicionar a este tópico</p>
	</c:when>
	<c:otherwise>
		<div class="sets" style="text-align: left">
			<p>Lista de problemas disponíveis</p>
			<form:form action="/pat2math/patt_topic/addProblems" modelAttribute="patt_problem" accept-charset="utf-8">
				<input style="display: none" id="${topic.id}" type="checkbox" name="ck" value="${topic.id}" checked>
				<c:forEach items="${problems}" var="problem">
					<p>
						<input id="${problem.id}" type="checkbox" name="ck" value="${problem.id}"></input> 
						<a href="/pat2math/patt_problem/${problem.id}">${problem.name}</a>
					</p>
				</c:forEach>
				<input class="btn btn-large" type="submit" value="Adicionar Problema(s) ao Tópico">
			</form:form>
		</div>
	</c:otherwise>
</c:choose>

<div id="sets" class="modal"></div>
<div id="mask" onclick="test56()"></div>