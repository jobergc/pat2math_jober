<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>

<style>
section {
	margin-top: 110px;
}
</style>

<header>
	<div class="row-fluid">
		<div class="span12">

			<a href="/pat2math/patt_plan/list" class="menu">Planos</a> 
			<a href="/pat2math/patt_topic/list" class="menu">T�picos</a>
			<a href="/pat2math/patt_problem/list" class="menu">Problemas</a>
			<a href="/pat2math/patt_hint/list" class="menu">Dicas</a>

			<!-- Redireciona para os menus do PAT2Math -->
			<a href="/pat2math/plan/list?page=0" class="menu">Pat2Math</a> 
			<a href="/pat2math/login" class="menu">Logout</a>
		</div>
	</div>
</header>