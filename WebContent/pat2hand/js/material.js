
var file_counter = 10;

function add_file(title, date, description, path) {
    file_counter++;
    
    var html_content = $("#file_template").clone()[0];
    $(html_content).attr("id","file_" + file_counter);
    $(html_content).find(".file_path").attr("href", path);
    $(html_content).find(".file_title").text(title);
    $(html_content).find(".file_date").text(date);
    $(html_content).find(".file_description").text(description);
    $(html_content).show();
    
    $("#material_files").append($(html_content));
}