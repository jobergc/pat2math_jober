# README

O PAT2Math é um Sitema Tutor Inteligente voltado para equações algébricas.

## Como rodar

Você vai precisar:

- De um servlet container(tomcat, jetty, etc.). Você pode usar um embedded server na sua IDE de preferência
- Mysql(crie o database 'pat2math')
- Copiar o divalite dentro da pasta WebContent/patequation
- Copiar as dependências(pasta lib) dentro do diretório WebContent/WEB-INF

Instruções mais detalhadas de instalação [aqui](https://www.dropbox.com/s/bguip423r5rv5ur/Instru%C3%A7%C3%B5es%20de%20instala%C3%A7%C3%A3o%20dos%20arquivos%20e%20programas%20necess%C3%A1rios.pdf?dl=0)
```
#!


```
: